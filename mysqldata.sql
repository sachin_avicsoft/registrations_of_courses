CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
--   `password` varchar(255) DEFAULT NULL,
--   `contact_number` varchar(45) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `org_unit_number` varchar(255) DEFAULT NULL,
  `role` varchar(45) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
--   UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `status` varchar(45) DEFAULT NULL,
  `start_date` varchar(45) DEFAULT NULL,
  `end_date` varchar(45) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `course_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) DEFAULT NULL,
  `lecture_id` int(11) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_type` varchar(45) DEFAULT NULL,
  `fileName` varchar(255) DEFAULT NULL,
  `contentType` varchar(255) DEFAULT NULL,
  `ext` varchar(45) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `exams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `start_date` varchar(45) DEFAULT NULL,
  `end_date` varchar(45) DEFAULT NULL,
  `time_limit` varchar(45) DEFAULT NULL,
  `active` int(1) DEFAULT '1',
  `status` varchar(45) DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text,
  `exam_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `question_type` varchar(255) DEFAULT NULL,
  `active` int(1) DEFAULT '1',
  `marks` varchar(45) DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `question_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `option` text,
  `status` varchar(45) DEFAULT NULL,
  `active` int(1) DEFAULT '1',
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `lectures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `active` int(1) DEFAULT '1',
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text,
  `course_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT '1',
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `properties` (
  `id` int(11) NOT NULL,
  `no_of_courses` int(11) DEFAULT NULL,
  `no_of_exams_per_course` int(11) DEFAULT NULL,
  `no_of_questions_per_exam` int(11) DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `student_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `registered` int(1) DEFAULT '1',
  `createdAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `student_solutions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `student_course_id` int(11) DEFAULT NULL,
  `fileName` varchar(255) DEFAULT NULL,
  `contentType` varchar(255) DEFAULT NULL,
  `ext` varchar(45) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `lecture_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `student_solution_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_solution_id` int(11) DEFAULT NULL,
  `comment` text,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `student_exams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `submitted` int(1) DEFAULT '0',
  `student_course_id` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `grade` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `student_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `student_exam_id` int(11) DEFAULT NULL,
  `solution` text,
  `marks_obtained` varchar(45) DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `student_course_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `courses` (`id`, `user_id`, `name`, `description`, `status`, `start_date`, `end_date`, `active`, `createdAt`, `updatedAt`) VALUES
(1, NULL, 'Datenbanken Grundlagen', 'Datenbanken Grundlagen is the name of current course.', 'enable', '05/10/2018', '05/10/2019', 1, '2018-09-03 13:02:58', NULL),
(2, NULL, 'Datenbanken und Webtechniken', 'Datenbanken und Webtechniken is the name of current course.', 'enable', '05/10/2018', '05/10/2019', 1, '2018-09-03 13:03:44', NULL);

INSERT INTO `exams` (`id`, `name`, `course_id`, `start_date`, `end_date`, `time_limit`, `active`, `createdAt`, `updatedAt`, `status`) VALUES
(1, 'Exam 1', 1, '03/09/2018', '31/08/2019', '15', 1, '2018-09-03 13:05:01', '2018-09-03 13:05:01', 'enable'),
(2, 'Exam 1', 2, '03/09/2018', '31/08/2019', '15', 1, '2018-09-03 14:41:22', '2018-09-03 14:41:22', 'enable');

INSERT INTO `questions` (`id`, `question`, `exam_id`, `course_id`, `status`, `question_type`, `active`, `createdAt`, `updatedAt`, `marks`) VALUES
(1, 'What are the different types of Normalization?', 1, 1, 'enable', 'shortdescription', 1, '2018-09-03 13:05:23', '2018-09-03 13:05:23', '2'),
(2, 'Which of the following gives a logical structure of the database graphically ?', 1, 1, 'enable', 'multichoice', 1, '2018-09-03 13:07:29', '2018-09-03 13:07:29', '2'),
(3, 'If you were collecting and storing information about your music collection, an album would be considered a(n) _____', 1, 1, 'enable', 'fillintheblank', 1, '2018-09-03 13:11:36', '2018-09-03 14:38:13', '2'),
(4, 'A database is called \'self-describing\' because it contains a description of itself.', 1, 1, 'enable', 'truefalse', 1, '2018-09-03 14:38:57', '2018-09-03 14:39:24', '2'),
(5, 'Words used in PL/SQL block are called.', 1, 1, 'enable', 'fillintheblank', 1, '2018-09-03 14:40:39', '2018-09-03 14:40:39', '2'),
(6, 'What is a Trigger?', 2, 2, 'enable', 'shortdescription', 1, '2018-09-03 14:42:23', '2018-09-03 14:42:23', '2'),
(7, 'In data model schemas, constraints that are can not be expressed directly are classified as', 2, 2, 'enable', 'multichoice', 1, '2018-09-03 14:43:28', '2018-09-03 14:43:28', '2'),
(8, 'In relational database schemas, state constraints are also known as', 2, 2, 'enable', 'multichoice', 1, '2018-09-03 14:44:41', '2018-09-03 14:44:41', '2'),
(9, 'SQL does not allow you to use the same table in a subquery as is being used in the outer query (the query that includes the subquery)', 2, 2, 'enable', 'truefalse', 1, '2018-09-03 14:45:20', '2018-09-03 14:45:20', '2'),
(10, 'The process that stores copies of a table and stores it in several different locations at the same time is called  ______ ?', 2, 2, 'enable', 'fillintheblank', 1, '2018-09-03 14:46:18', '2018-09-03 14:46:18', '2'),
(11, 'The type of form that displays data from two tables that have a one to many relationship is called a __________', 2, 2, 'enable', 'fillintheblank', 1, '2018-09-03 14:47:14', '2018-09-03 14:47:14', '2'),
(12, 'Words used in PL/SQL block are called.', 2, 2, 'enable', 'multichoice', 1, '2018-09-03 14:48:17', '2018-09-03 14:48:17', '2'),
(13, 'A simple class diagram for a university database has only three classes  :Teachers, Students and Courses.How many binary associations will be sufficient to establish whether Teacher X is teaching Course Y to student Z?', 2, 2, 'enable', 'multichoice', 1, '2018-09-03 14:50:12', '2018-09-03 14:50:12', '2'),
(14, 'The Unified Modeling Language(UML)is :', 2, 2, 'enable', 'multichoice', 1, '2018-09-03 14:51:27', '2018-09-03 14:51:27', '2');

INSERT INTO `question_options` (`id`, `question_id`, `option`, `status`, `active`, `createdAt`, `updatedAt`) VALUES
(6, 2, 'Entity-relationship diagram', NULL, 1, '2018-09-03 13:07:29', '2018-09-03 13:07:29'),
(7, 2, 'Entity diagram', NULL, 1, '2018-09-03 13:07:29', '2018-09-03 13:07:29'),
(8, 2, 'Database diagram', NULL, 1, '2018-09-03 13:07:29', '2018-09-03 13:07:29'),
(9, 2, 'Architectural representation', NULL, 1, '2018-09-03 13:07:29', '2018-09-03 13:07:29'),
(10, 3, 'Relation', NULL, 1, '2018-09-03 14:38:13', '2018-09-03 14:38:13'),
(11, 3, 'Entity', NULL, 1, '2018-09-03 14:38:13', '2018-09-03 14:38:13'),
(12, 3, 'Instance', NULL, 1, '2018-09-03 14:38:13', '2018-09-03 14:38:13'),
(13, 3, 'Attribute', NULL, 1, '2018-09-03 14:38:13', '2018-09-03 14:38:13'),
(14, 5, 'Grammar', NULL, 1, '2018-09-03 14:40:39', '2018-09-03 14:40:39'),
(15, 5, 'Lexical Units', NULL, 1, '2018-09-03 14:40:39', '2018-09-03 14:40:39'),
(16, 5, 'Block Units', NULL, 1, '2018-09-03 14:40:39', '2018-09-03 14:40:39'),
(17, 5, 'Lexical', NULL, 1, '2018-09-03 14:40:39', '2018-09-03 14:40:39'),
(18, 7, 'application based constraints', NULL, 1, '2018-09-03 14:43:28', '2018-09-03 14:43:28'),
(19, 7, 'semantic constraints', NULL, 1, '2018-09-03 14:43:28', '2018-09-03 14:43:28'),
(20, 7, 'business rules', NULL, 1, '2018-09-03 14:43:28', '2018-09-03 14:43:28'),
(21, 7, 'all of above', NULL, 1, '2018-09-03 14:43:28', '2018-09-03 14:43:28'),
(22, 8, 'static constraints', NULL, 1, '2018-09-03 14:44:41', '2018-09-03 14:44:41'),
(23, 8, 'dynamic constraints', NULL, 1, '2018-09-03 14:44:41', '2018-09-03 14:44:41'),
(24, 8, 'implicit constraints', NULL, 1, '2018-09-03 14:44:41', '2018-09-03 14:44:41'),
(25, 8, 'explicit constraints', NULL, 1, '2018-09-03 14:44:41', '2018-09-03 14:44:41'),
(26, 10, 'Partitioning', NULL, 1, '2018-09-03 14:46:18', '2018-09-03 14:46:18'),
(27, 10, 'Segmentation', NULL, 1, '2018-09-03 14:46:18', '2018-09-03 14:46:18'),
(28, 10, 'Data mirroring', NULL, 1, '2018-09-03 14:46:18', '2018-09-03 14:46:18'),
(29, 10, 'Replication', NULL, 1, '2018-09-03 14:46:18', '2018-09-03 14:46:18'),
(30, 11, 'Relational form', NULL, 1, '2018-09-03 14:47:14', '2018-09-03 14:47:14'),
(31, 11, 'Crosstab form', NULL, 1, '2018-09-03 14:47:14', '2018-09-03 14:47:14'),
(32, 11, 'Subform', NULL, 1, '2018-09-03 14:47:14', '2018-09-03 14:47:14'),
(33, 11, 'Referential form', NULL, 1, '2018-09-03 14:47:14', '2018-09-03 14:47:14'),
(34, 12, 'Grammar', NULL, 1, '2018-09-03 14:48:17', '2018-09-03 14:48:17'),
(35, 12, 'Lexical Units', NULL, 1, '2018-09-03 14:48:17', '2018-09-03 14:48:17'),
(36, 12, 'Block Units', NULL, 1, '2018-09-03 14:48:17', '2018-09-03 14:48:17'),
(37, 12, 'Lexical', NULL, 1, '2018-09-03 14:48:17', '2018-09-03 14:48:17'),
(38, 13, '3', NULL, 1, '2018-09-03 14:50:12', '2018-09-03 14:50:12'),
(39, 13, '1', NULL, 1, '2018-09-03 14:50:12', '2018-09-03 14:50:12'),
(40, 13, '2', NULL, 1, '2018-09-03 14:50:12', '2018-09-03 14:50:12'),
(41, 13, 'None of the above', NULL, 1, '2018-09-03 14:50:12', '2018-09-03 14:50:12'),
(42, 14, 'An international standard language used to write database application programs.', NULL, 1, '2018-09-03 14:51:27', '2018-09-03 14:51:27'),
(43, 14, 'An important markup language used for developing web sites.', NULL, 1, '2018-09-03 14:51:27', '2018-09-03 14:51:27'),
(44, 14, 'A general purpose set of standards to visually model various aspects of software systems and business processes.', NULL, 1, '2018-09-03 14:51:27', '2018-09-03 14:51:27'),
(45, 14, 'None of the above', NULL, 1, '2018-09-03 14:51:27', '2018-09-03 14:51:27');

INSERT INTO `users` (`first_name`, `last_name`, `email`, `password`, `contact_number`, `role`, `active`, `createdAt`, `updatedAt`) VALUES
('Teacher', 'User', 'teacher@test.com', '25f9e794323b453885f5181f1b624d0b', '9999999999', 'TEACHER', 1, '2018-08-22 15:10:59', '2018-08-22 15:10:59'),
('Student', 'User', 'student@test.com', '25f9e794323b453885f5181f1b624d0b', '9999999999', 'STUDENT', 1, '2018-08-22 15:10:59', '2018-08-22 15:10:59'),
('Student1', 'User', 'student1@test.com', '25f9e794323b453885f5181f1b624d0b', '9999999999', 'STUDENT', 1, '2018-08-22 15:10:59', '2018-08-22 15:10:59');

INSERT INTO `properties` (`id`, `no_of_courses`, `no_of_exams_per_course`, `no_of_questions_per_exam`, `createdAt`, `updatedAt`) VALUES
(1, '3', '3', '3', '2018-09-03 14:51:27', '2018-09-03 14:51:27');