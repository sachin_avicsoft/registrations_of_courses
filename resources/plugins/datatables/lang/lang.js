var lang_de =
{
	"sEmptyTable":   	"कोणतीही माहिती उपलब्ध नाही",
	"sInfo":         	"दर्शवित आहे _TOTAL_ पैकी _START_ ते _END_ नोंदी",
	"sInfoEmpty":    	"दर्शवित आहे 0 ते 0 च्या 0 नोंदी",
	"sInfoFiltered": 	"(_MAX_ मधील नोंदी निवडले आहे )",
	"sInfoPostFix":  	"",
	"sInfoThousands":  	".",
	"sLengthMenu":   	"_MENU_ नोंदी दाखवा ",
	"sLoadingRecords": 	"प्रक्रिया करत आहे...",
	"sProcessing":   	"प्रक्रिया करत आहे...",
	"sSearch":       	"शोधा ",
	"sZeroRecords":  	"कोणतेही जुळणाऱ्या नोंदी सापडल्या नाही",
	"oPaginate": {
		"sFirst":    	"पहिले",
		"sPrevious": 	"मागील",
		"sNext":     	"पुढील",
		"sLast":     	"शेवट"
	},
	"oAria": {
		"sSortAscending":  ": चढत्या क्रमाने क्रमवारी करणे सक्रिय आहे",
		"sSortDescending": ": उतरत्या क्रमाने क्रमवारी करणे सक्रिय आहे"
	}
};

var lang_en =
{
	"sEmptyTable":     "No data available in table",
	"sInfo":           "Showing _START_ to _END_ of _TOTAL_ entries",
	"sInfoEmpty":      "Showing 0 to 0 of 0 entries",
	"sInfoFiltered":   "(filtered from _MAX_ total entries)",
	"sInfoPostFix":    "",
	"sInfoThousands":  ",",
	"sLengthMenu":     "Show _MENU_ entries",
	"sLoadingRecords": "Loading...",
	"sProcessing":     "Processing...",
	"sSearch":         "Search:",
	"sZeroRecords":    "No matching records found",
	"oPaginate": {
		"sFirst":    "First",
		"sLast":     "Last",
		"sNext":     "Next",
		"sPrevious": "Previous"
	},
	"oAria": {
		"sSortAscending":  ": activate to sort column ascending",
		"sSortDescending": ": activate to sort column descending"
	}
};