<?php

class File extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    /**
     * Get file row by id from course_files
     */
    public function get_file($id){
        $this->db->select()->from('course_files');
        $this->db->where('course_id',$id);
//        $this->db->where('file_type',1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get student_solutions table row by id
     */
    public function get_student_solution_file_by_id($fileId){
        $this->db->select('student_solutions.*,course_files.file_name')->from('student_solutions');
        $this->db->join('course_files','student_solutions.file_id = course_files.id','left outer');
        $this->db->where('student_solutions.id',$fileId);
//        $this->db->where('file_type',1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    /**
     * Get student_solutions row by id and user_id 
     */
    public function get_student_solution_file($fileId, $userId){
        $this->db->select('student_solutions.*,course_files.file_name')->from('student_solutions');
        $this->db->join('course_files','student_solutions.file_id = course_files.id','left outer');
        $this->db->where('student_solutions.id',$fileId);
        $this->db->where('student_solutions.user_id',$userId);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    /**
     * Get row by id from course_files
     */
    public function get_course_file($fileId){
        $this->db->select()->from('course_files');
        $this->db->where('id', $fileId);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    /**
     * Add file to course_files table
     */
    public function insert_file($fileName, $file_name, $contentType, $ext, $id, $file_type, $lectureId){
        $data = array (
            'fileName' => $fileName,
            'contentType' => $contentType,
            'ext' => $ext,
            'course_id' => $id,
            'lecture_id' => $lectureId,
            'file_name' => $file_name,
            'file_type' => $file_type,
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('course_files',$data);
        return $this->db->insert_id();
    }
    /**
     * Update location field by id from course_files table
     */
    public function updateLocation($id, $location){
        $data = array (
            'location' => $location,
        );
        $this->db->where('id',$id);
        $this->db->update('course_files',$data);
    }
    /**
     * Delete file by id from course_files table
     */
    public function delete_file(){
        $this->db->where('id',$this->input->get('id'));
        $this->db->delete('course_files');
    }
    /**
     * Delete file by id from course_files table
     */
    public function delete_file_by_id($file_id){
        $this->db->where('id',$file_id);
        $this->db->delete('course_files');
    }
}
