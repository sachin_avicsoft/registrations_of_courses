<?php

class Comment extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    /**
     * Add comments to table comments
     */
    public function add($userId){
        $data = array(
            'comment' => $this->input->post('comment'),
            'course_id' => $this->input->post('id'),
            'user_id' => $userId,
            'createdAt' => date('Y-m-d H:i:s'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('comments',$data);
        return $this->db->insert_id();
    }
    /**
     * Update comment row from comments table
     */
    public function update(){
        $data = array(
            'comment' => $this->input->post('comment'),
            'course_id' => $this->input->post('course_id'),
            'user_id' => $this->input->post('user_id'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('comments',$data);
    }
    /**
     * Delete row from comments table
     */
    public function delete(){
        $data = array(
            'active' => 0,
        );
        $this->db->where('id',$this->input->get('id'));
        $this->db->update('comments',$data);
    }
    /**
     * Get row from comments table with specific id
     */
    public function get($id) {
        $this->db->select()->from('comments');
        $this->db->where('id', $id);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    /**
     * Get rows from comments table according to particular course_id
     */
    public function get_comment_list($courseId){
        $this->db->select('comments.*,users.first_name,users.last_name')->from('comments');
        $this->db->join('users','comments.user_id = users.id','left outer');
        $this->db->where('comments.course_id', $courseId);
        $this->db->where('comments.active', 1);
        $this->db->order_by('comments.id','desc');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
}
