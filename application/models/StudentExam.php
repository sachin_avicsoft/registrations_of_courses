<?php

class StudentExam extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    /**
     * Add row to student_exams table
     */
    public function add($userId, $examId, $studentCourseId){
        $data = array(
            'user_id' => $userId,
            'exam_id' => $examId,
            'student_course_id' => $studentCourseId,
            'start_time' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('student_exams',$data);
        return $this->db->insert_id();
    }
    /**
     * Update row with status and grade fields from student_exams table by id
     */
    public function update_result($studentExamId, $status, $grade){
        $data = array(
            'status' => $status,
            'grade' => $grade,
        );
        $this->db->where('id',$studentExamId);
        $this->db->update('student_exams',$data);
    }
    /**
     * Update row from student_exams table by id
     */
    public function update($userId, $examId){
            $data = array(
                'user_id' => $userId,
                'exam_id' => $examId,
            );
            $this->db->where('id',$this->input->post('id'));
            $this->db->update('student_exams',$data);
    }
    /**
     * Update row with submitted fields from student_exams table by id
     */
    public function update_submitted($id, $value){
        $data = array(
            'submitted' => $value,
        );
        $this->db->where('id',$id);
        $this->db->update('student_exams',$data);
    }
    /**
     * Get row by id with time_limit field of exams table from student_exams table
     */
    public function get($id) {
            $this->db->select('student_exams.*,exams.time_limit')->from('student_exams');
            $this->db->join('exams','student_exams.exam_id = exams.id','left outer');
            $this->db->where('student_exams.id', $id);
    //        $this->db->where('active', 1);
            $query = $this->db->get();
            if($query->num_rows() > 0){
                return $query->row_array();
            }
            return FALSE;
    }
    /**
     * Get row by user_id and exam_id with time_limit field of exams table from student_exams table
     */
    public function get_exam_by_user($userId, $examId) {
        $this->db->select('student_exams.*,exams.time_limit')->from('student_exams');
        $this->db->join('exams','student_exams.exam_id = exams.id','left outer');
        $this->db->where('student_exams.user_id', $userId);
        $this->db->where('student_exams.exam_id', $examId);
//        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    /**
     * Get row by user_id and exam_id and student_course_id with time_limit field of exams table from student_exams table
     */
    public function get_exam_by_user_and_student_course_id($userId, $examId, $studentCourseId) {
        $this->db->select('student_exams.*,exams.time_limit')->from('student_exams');
        $this->db->join('exams','student_exams.exam_id = exams.id','left outer');
        $this->db->where('student_exams.user_id', $userId);
        $this->db->where('student_exams.exam_id', $examId);
        $this->db->where('student_exams.student_course_id', $studentCourseId);
//        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
}
