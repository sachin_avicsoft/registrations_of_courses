<?php

class Question extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    /**
     * Add row to questions table
     */
    public function add(){
        $data = array(
            'question' => $this->input->post('question'),
            'course_id' => $this->input->post('course_id'),
            'exam_id' => $this->input->post('exam_id'),
            'status' => $this->input->post('status'),
            'marks' => $this->input->post('marks'),
            'question_type' => $this->input->post('question_type'),
            'createdAt' => date('Y-m-d H:i:s'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('questions',$data);
        return $this->db->insert_id();
    }
    /**
     * Update data from questions table by id
     */
    public function update(){
        $data = array(
            'question' => $this->input->post('question'),
            'course_id' => $this->input->post('course_id'),
            'exam_id' => $this->input->post('exam_id'),
            'status' => $this->input->post('status'),
            'marks' => $this->input->post('marks'),
            'question_type' => $this->input->post('question_type'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('questions',$data);
    }
    /**
     * Update active to 0 by id from questions table
     */
    public function delete(){
        $data = array(
            'active' => 0,
        );
        $this->db->where('id',$this->input->get('id'));
        $this->db->update('questions',$data);
    }
    /**
     * Change status field by id from questions table
     */
    public function change_status($id, $status){
        $data = array(
            'status' => $status,
        );
        $this->db->where('id',$this->input->get('id'));
        $this->db->update('questions',$data);
    }
    /**
     * Get row by id and active 1 from questions table
     */
    public function get($id){
        $this->db->select()->from('questions');
        $this->db->where('id', $id);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    /**
     * Get rows by exam_id from questions table
     */
    public function get_question_list_by_examId($examId){
        $this->db->select('questions.*')->from('questions');
        $this->db->where('exam_id', $examId);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get rows by exam_id and status with enable from questions table
     */
    public function get_question_list_by_examId_by_enable_status($examId){
        $this->db->select('questions.*')->from('questions');
        $this->db->where('exam_id', $examId);
        $this->db->where('status', 'enable');
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Add row with question_id to question_options table
     */
    public function add_option($questionId, $option){
        $data = array(
            'question_id' => $questionId,
            'option' => $option,
            'createdAt' => date('Y-m-d H:i:s'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('question_options',$data);
        return $this->db->insert_id();
    }
    /**
     * Get rows by question_id and active 1 from question_options table
     */
    public function get_question_option_list($questionId){
        $this->db->select()->from('question_options');
        $this->db->where('active', 1);
        $this->db->where('question_id', $questionId);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Update active to 0 by id from question_options table
     */
    public function delete_option($id){
        $data = array(
            'active' => 0,
        );
        $this->db->where('id', $id);
        $this->db->update('question_options', $data);
    }
}
