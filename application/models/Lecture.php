<?php

class Lecture extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    /**
     * Add row to lectures table
     */
    public function add(){
        $data = array(
            'name' => $this->input->post('name'),
            'course_id' => $this->input->post('course_id'),
            'date' => $this->input->post('date'),
            'createdAt' => date('Y-m-d H:i:s'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('lectures',$data);
        return $this->db->insert_id();
    }
    /**
     * Update data from lectures table by id
     */
    public function update(){
        $data = array(
            'name' => $this->input->post('name'),
            'course_id' => $this->input->post('course_id'),
            'date' => $this->input->post('date'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('lectures',$data);
    }
    /**
     * Update active to 0 by id from lectures table
     */
    public function delete(){
        $data = array(
            'active' => 0,
        );
        $this->db->where('id',$this->input->get('id'));
        $this->db->update('lectures',$data);
    }
    /**
     * Get row by id and active 1 from lectures table
     */
    public function get($id) {
        $this->db->select()->from('lectures');
        $this->db->where('id', $id);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    /**
     * Get rows by course_id from lectures table
     */
    public function get_lecture_list_by_courseId($courseId){
        $this->db->select()->from('lectures');
        $this->db->where('course_id', $courseId);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get rows by lecture_id and file_type 1 from course_files table
     */
    public function get_lecture_homework_file_list($lectureId){
        $this->db->select()->from('course_files');
        $this->db->where('lecture_id', $lectureId);
        $this->db->where('file_type', '1');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get rows by lecture_id from course_files table
     */
    public function get_lecture_file_list($id){
        $this->db->select()->from('course_files');
        $this->db->where('lecture_id', $id);
//        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get rows by lecture_id and file_type from course_files table
     */
    public function get_file_list_by_lecture_id_and_type($id, $type = null){
        $this->db->select()->from('course_files');
        $this->db->where('lecture_id',$id);
        if($type != null){
            $this->db->where('file_type',$type);
        }
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get rows by lecture_id and file_id from course_files table
     */
    public function get_file_list_by_lecture_id_and_file_id($id, $fileId){
        $this->db->select()->from('course_files');
        $this->db->where('lecture_id',$id);
        $this->db->where('file_id',$fileId);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Delete file by id from course_files table
     */
    public function delete_file(){
        $this->db->where('id',$this->input->get('id'));
        $this->db->delete('course_files');
    }
    /**
     * Delete file by id from course_files table
     */
    public function delete_file_by_id($file_id){
        $this->db->where('id',$file_id);
        $this->db->delete('course_files');
    }
    /**
     * Update location field by id from course_files table
     */
    public function updateLocation($id, $location){
        $data = array (
            'location' => $location,
        );
        $this->db->where('id',$id);
        $this->db->update('course_files',$data);
    }
    /**
     * Add file to course_files table
     */
    public function insert_file($fileName, $file_name, $contentType, $ext, $id, $file_type, $lectureId){
        $data = array (
            'fileName' => $fileName,
            'contentType' => $contentType,
            'ext' => $ext,
            'course_id' => $id,
            'lecture_id' => $lectureId,
            'file_name' => $file_name,
            'file_type' => $file_type,
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('course_files',$data);
        return $this->db->insert_id();
    }
    /**
     * Add file to course_files table
     */
    public function insert_solution_file($fileName, $file_name, $contentType, $ext, $id, $file_type, $lectureId, $file_id){
        $data = array (
            'fileName' => $fileName,
            'contentType' => $contentType,
            'ext' => $ext,
            'course_id' => $id,
            'lecture_id' => $lectureId,
            'file_name' => $file_name,
            'file_type' => $file_type,
            'file_id' => $file_id,
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('course_files',$data);
        return $this->db->insert_id();
    }
    
}