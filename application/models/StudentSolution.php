<?php

class StudentSolution extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    /**
     * Get row with file_name field of course_files table by id from student_solutions table
     */
    public function get($id){
        $this->db->select('student_solutions.*,course_files.file_name')->from('student_solutions');
        $this->db->join('course_files','student_solutions.file_id = course_files.id','left outer');
        $this->db->where('student_solutions.id',$id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    /**
     * Get rows by course_id and user_id from student_solutions table
     */
    public function get_solution_list_by_course_id_and_user_id($courseId, $userId){
        $this->db->select('student_solutions.*,course_files.file_name, lectures.name as lectureName')->from('student_solutions');
        $this->db->join('course_files','student_solutions.file_id = course_files.id','left outer');
        $this->db->join('lectures','student_solutions.lecture_id = lectures.id','left outer');
        $this->db->where('student_solutions.course_id',$courseId);
        $this->db->where('student_solutions.user_id',$userId);
        $this->db->order_by('lectures.id');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get rows by course_id and user_id and student_course_id from student_solutions table
     */
    public function get_solution_list_by_course_id_and_user_id_and_course_id($courseId, $userId, $studentCourseId){
        $this->db->select('student_solutions.*,course_files.file_name, lectures.name as lectureName')->from('student_solutions');
        $this->db->join('course_files','student_solutions.file_id = course_files.id','left outer');
        $this->db->join('lectures','student_solutions.lecture_id = lectures.id','left outer');
        $this->db->where('student_solutions.course_id',$courseId);
        $this->db->where('student_solutions.user_id',$userId);
        $this->db->where('student_solutions.student_course_id',$studentCourseId);
        $this->db->order_by('lectures.id');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get rows by lecture_id and user_id from student_solutions table
     */
    public function get_lecture_solution_file_list_by_user_id($lectureId, $userId){
        $this->db->select('student_solutions.*,course_files.file_name')->from('student_solutions');
        $this->db->join('course_files','student_solutions.file_id = course_files.id','left outer');
        $this->db->where('student_solutions.lecture_id',$lectureId);
        $this->db->where('student_solutions.user_id',$userId);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get rows by lecture_id and user_id and student_course_id from student_solutions table
     */
    public function get_lecture_solution_file_list_by_user_id_and_student_course_id($lectureId, $userId, $studentCourseId){
        $this->db->select('student_solutions.*,course_files.file_name')->from('student_solutions');
        $this->db->join('course_files','student_solutions.file_id = course_files.id','left outer');
        $this->db->where('student_solutions.lecture_id',$lectureId);
        $this->db->where('student_solutions.user_id',$userId);
        $this->db->where('student_solutions.student_course_id',$studentCourseId);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get rows by lecture_id and user_id and student_course_id from student_solutions table
     */
    public function get_solution_list_by_course_id_and_user_id_by_student_course_id($lectureId, $userId, $studentCourseId){
        $this->db->select('student_solutions.*,course_files.file_name')->from('student_solutions');
        $this->db->join('course_files','student_solutions.file_id = course_files.id','left outer');
        $this->db->where('student_solutions.lecture_id',$lectureId);
        $this->db->where('student_solutions.user_id',$userId);
        $this->db->where('student_solutions.student_course_id',$studentCourseId);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Add file to student_solutions table
     */
    public function insert_file($fileName, $fileId, $contentType, $ext, $courseId, $userId, $lectureId, $studentCourseId){
        $data = array (
            'fileName' => $fileName,
            'contentType' => $contentType,
            'ext' => $ext,
            'course_id' => $courseId,
            'lecture_id' => $lectureId,
            'student_course_id' => $studentCourseId,
            'file_id' => $fileId,
            'user_id' => $userId,
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('student_solutions',$data);
        return $this->db->insert_id();
    }
    /**
     * Update location field by id from student_solutions table
     */
    public function updateLocation($id, $location){
        $data = array (
            'location' => $location,
        );
        $this->db->where('id',$id);
        $this->db->update('student_solutions',$data);
    }
    /**
     * Delete file by id from student_solutions table
     */
    public function delete_file(){
        $this->db->where('id',$this->input->get('id'));
        $this->db->delete('student_solutions');
    }
    /**
     * Delete file by id from student_solutions table
     */
    public function delete_file_by_id($file_id){
        $this->db->where('id',$file_id);
        $this->db->delete('student_solutions');
    }
    
    //student solution comment
    /**
     * Add row to table student_solution_comments
     */
    public function addCommentforStudentSolutionByTeacher($teacherId){
        $data = array (
            'student_solution_id' => $this->input->post('student_solution_id'),
            'comment' => $this->input->post('comment'),
            'user_id' => $teacherId,
            'createdAt' => date('Y-m-d H:i:s'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('student_solution_comments',$data);
        return $this->db->insert_id();
    }
    /**
     * Add row to table student_solution_comments with student user
     */
    public function addCommentforStudentSolutionByStudent($userId, $solutionId){
        $data = array (
            'student_solution_id' => $solutionId,
            'comment' => $this->input->post('comment'),
            'user_id' => $userId,
            'createdAt' => date('Y-m-d H:i:s'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('student_solution_comments',$data);
        return $this->db->insert_id();
    }
    /**
     * Get rows from student_solution_comments table according to particular solution_id
     */
    public function get_student_solution_comment_by_solution_id($solutionId){
        $this->db->select('student_solution_comments.*,users.first_name,users.last_name');
        $this->db->from('student_solution_comments');
        $this->db->join('users','student_solution_comments.user_id = users.id','left outer');
        $this->db->where('student_solution_comments.student_solution_id',$solutionId);
        $this->db->order_by('student_solution_comments.id','desc');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
}
