<?php

class StudentCourse extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    /**
     * Get row by id from student_courses table
     */
    public function get($id) {
        $this->db->select()->from('student_courses');
        $this->db->where('student_courses.id', $id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    /**
     * Delete row by id from student_courses table
     */
    public function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('student_courses');
    }
    /**
     * Get rows by course_id from student_courses table
     */
    public function get_student_list_by_course_id($id){
        $this->db->select('student_courses.*')->from('student_courses');
        $this->db->where('student_courses.registered', 1);
        $this->db->where('student_courses.course_id',$id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get rows by course_id from student_courses table
     */
    public function get_student_list_by_course($id){
        $this->db->select('users.first_name,users.last_name,users.id,users.email')->from('student_courses');
        $this->db->join('users','student_courses.user_id = users.id','left outer');
        $this->db->where('users.active', 1);
        $this->db->where('student_courses.registered', 1);
        $this->db->where('student_courses.course_id',$id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get list of courses by user_id from courses table
     */
    public function get_course_list_with_student_courses($userId){
        $this->db->select('courses.*');
//        $this->db->join('student_courses','courses.id = student_courses.course_id','left outer');
        $this->db->from('courses');
        $this->db->where('courses.active', 1);
        $this->db->where('courses.status', 'enable');
//        $where = "(student_courses.user_id = $userId OR student_courses.user_id is null)";
//        $this->db->where($where);
        $this->db->order_by('courses.id');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get student_courses row by user_id and course_id
     */
    public function get_student_course_by_user_id_and_course_id($userId, $courseId){
        $this->db->select()->from('student_courses');
        $this->db->where('course_id',$courseId);
        $this->db->where('user_id',$userId);
        $this->db->where('registered', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    /**
     * Add row to student_courses table
     */
    public function add_student_course($courseId, $status, $userId){
        $data = array(
            'course_id' => $courseId,
            'user_id' => $userId,
            'registered' => $status,
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('student_courses',$data);
        return $this->db->insert_id();
    }
    /**
     * Delete student course id 
     */
    public function change_registration_status($id, $status){
        $this->db->where('id',$id);
        $this->db->delete('student_courses');
    }
}
