<?php

class Properties extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    /**
     * Add row to properties table
     */
    public function add(){
        $data = array(
            'no_of_courses' => $this->input->post('no_of_courses'),
            'no_of_exams_per_course' => $this->input->post('no_of_exams_per_course'),
            'no_of_questions_per_exam' => $this->input->post('no_of_questions_per_exam'),
            'updatedAt' => date('Y-m-d H:i:s'),
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('properties',$data);
    }
    /**
     * Update data from properties table by id
     */
    public function update(){
        $data = array(
            'no_of_courses' => $this->input->post('no_of_courses'),
            'no_of_exams_per_course' => $this->input->post('no_of_exams_per_course'),
            'no_of_questions_per_exam' => $this->input->post('no_of_questions_per_exam'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->update('properties',$data);
    }
    /**
     * Get row from properties table
     */
    public function get() {
        $this->db->select()->from('properties');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    
}
