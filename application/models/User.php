<?php

class User extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    /**
     * Add admin user to database
     */
    public function add() {
        $data = array(
            'role' => "ADMIN",
            'first_name' => $this->input->post("first_name"),
            'last_name' => $this->input->post("last_name"),
            'email' => $this->input->post("email"),
            'contact_number' => $this->input->post("contact_number"),
            'password' => md5($this->input->post("password")),
            'createdAt' => date('Y-m-d H:i:s'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }
    /**
     * Update user role to admin
     */
    public function update_user_to_admin($id){
        $data = array(
            'role' => "ADMIN",
            'first_name' => $this->input->post("first_name"),
            'last_name' => $this->input->post("last_name"),
            'email' => $this->input->post("email"),
            'contact_number' => $this->input->post("contact_number"),
            'password' => md5($this->input->post("password")),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id',$id);
        $this->db->update('users',$data);
    }
    /**
     * Get user by email and password
     */
    public function get_entry($email, $password) {
        $this->db->select()->from('users');
        $this->db->where('email', $email);
        $this->db->where('password', md5($password));
        $this->db->where('active', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }
    /**
     * Get particular user by id
     */
    public function get($id) {
        $this->db->select()->from('users');
        $this->db->where('id', $id);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }
    /**
     * Get the list of all users
     */
    public function get_user_list(){
        $this->db->select('users.*')->from('users');
        $this->db->where('active',1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    /**
     * Get the list of users by specific role
     */
    public function get_user_list_by_role($role){
        $this->db->select('users.*')->from('users');
        $this->db->where('role',$role);
        $this->db->where('active',1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    /**
     * Get user by email 
     */
    public function get_user_by_email($email) {
        $this->db->select('users.*');
        $this->db->from('users');
        $this->db->where('email', $email);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }
    /**
     * Check email is exists in users table or not
     */
    function mail_exists($email) {
        $this->db->where('email', $email);
        $this->db->where('active', 1);
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Check email is exists in users table or not
     */
    public function email_exists($userId,$email){
        $this->db->where('email', $email);
        $this->db->where('userId !='.$userId);
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function get_user_by_username($username){
        $this->db->select('users.*');
        $this->db->from('users');
        $this->db->where('username', $username);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }
    
    public function add_chemnitz_user($username, $first_name, $last_name, $email, $org_unit_number, $role){
        $data = array(
            'role' => $role,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'username' => $username,
            'org_unit_number' => $org_unit_number,
            'active' => 1,
            'createdAt' => date('Y-m-d H:i:s'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }

}
