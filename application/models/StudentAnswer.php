<?php

class StudentAnswer extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    /**
     * Add row to student_answers table
     */
    public function add($userId, $studentCourseId, $questionId, $studentExamId){
        $data = array(
            'user_id' => $userId,
            'question_id' => $questionId,
            'student_course_id' => $studentCourseId,
            'student_exam_id' => $studentExamId,
            'createdAt' => date('Y-m-d H:i:s'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('student_answers',$data);
        return $this->db->insert_id();
    }
    /**
     * Update data from student_answers table by id
     */
    public function update($id, $solution){
            $data = array(
                'solution' => $solution,
                'updatedAt' => date('Y-m-d H:i:s'),
            );
            $this->db->where('id',$id);
            $this->db->update('student_answers',$data);
    }
    /**
     * Add solution to student_answers table by id
     */
    public function submit_answer($id, $value){
        $data = array(
            'solution' => $value,
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id',$id);
        $this->db->update('student_answers',$data);
    }
    
    public function update_marks($id, $marks){
        $data = array(
            'marks_obtained' => $marks,
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id',$id);
        $this->db->update('student_answers',$data);
    }
    /**
     * Get row by id from student_answers table
     */
    public function get($id) {
            $this->db->select('student_answers.*')->from('student_answers');
            $this->db->where('id', $id);
            $query = $this->db->get();
            if($query->num_rows() > 0){
                return $query->row_array();
            }
            return FALSE;
    }
    /**
     * Get rows by user_id,student_exam_id and student_course_id from student_answers table
     */
    public function get_student_questions_and_answers_by_user_course_list($userId, $studentCourseId, $studentExamId){
        $this->db->select('student_answers.*')->from('student_answers');
        $this->db->where('student_answers.user_id', $userId);
        $this->db->where('student_answers.student_course_id', $studentCourseId);
        $this->db->where('student_answers.student_exam_id', $studentExamId);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get rows by user_id,student_exam_id from student_answers table
     */
    public function get_student_questions_and_answers_by_user_exam_id($userId, $studentExamId){
        $this->db->select('student_answers.*')->from('student_answers');
        $this->db->where('student_answers.user_id', $userId);
        $this->db->where('student_answers.student_exam_id', $studentExamId);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
}
