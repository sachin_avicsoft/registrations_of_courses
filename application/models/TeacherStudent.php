<?php

class TeacherStudent extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    /**
     * Get list of students user by course_id from student_courses table
     */
    public function get_student_list_by_course_id($courseId){
        $this->db->select('users.*')->from('student_courses');
        $this->db->join('users','student_courses.user_id = users.id','left outer');
        $this->db->where('student_courses.course_id', $courseId);
        $this->db->where('users.active', 1);
        $this->db->where('student_courses.registered', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
}
