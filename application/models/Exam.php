<?php

class Exam extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    /**
     * Add exam to exams table
     */
    public function add(){
        $data = array(
            'name' => $this->input->post('name'),
            'course_id' => $this->input->post('course_id'),
            'start_date' => $this->input->post('start_date'),
            'end_date' => $this->input->post('end_date'),
            'time_limit' => $this->input->post('time_limit'),
            'status' => $this->input->post('status'),
            'createdAt' => date('Y-m-d H:i:s'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('exams',$data);
        return $this->db->insert_id();
    }
    /**
     * Update data from exams table by id
     */
    public function update(){
        $data = array(
            'name' => $this->input->post('name'),
            'course_id' => $this->input->post('course_id'),
            'start_date' => $this->input->post('start_date'),
            'end_date' => $this->input->post('end_date'),
            'status' => $this->input->post('status'),
            'time_limit' => $this->input->post('time_limit'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('exams',$data);
    }
    /**
     * Update active to 0 by id from exams table
     */
    public function delete(){
        $data = array(
            'active' => 0,
        );
        $this->db->where('id',$this->input->get('id'));
        $this->db->update('exams',$data);
    }
    /**
     * Get exam row by id and active 1 from exams table
     */
    public function get($id) {
        $this->db->select()->from('exams');
        $this->db->where('id', $id);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    /**
     * Get exam rows by course_id from exams table
     */
    public function get_exam_list_by_courseId($courseId){
        $this->db->select()->from('exams');
        $this->db->where('course_id', $courseId);
//        $this->db->where('status', 'enable');
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get exam rows by course_id with status enable from exams table
     */
    public function get_exam_list_by_courseId_by_enable_status($courseId){
        $this->db->select()->from('exams');
        $this->db->where('course_id', $courseId);
        $this->db->where('status', 'enable');
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Change status field by id from exams table
     */
    public function change_status($id, $status){
        $data = array(
            'status' => $status,
        );
        $this->db->where('id',$this->input->get('id'));
        $this->db->update('exams',$data);
    }
}
