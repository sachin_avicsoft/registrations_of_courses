<?php

class Course extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    /**
     * Add course to courses table
     */
    public function add(){
        $data = array(
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'start_date' => $this->input->post('start_date'),
            'end_date' => $this->input->post('end_date'),
            'status' => $this->input->post('status'),
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('courses',$data);
        return $this->db->insert_id();
    }
    /**
     * Update course from courses table by id
     */
    public function update(){
        $data = array(
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'status' => $this->input->post('status'),
            'start_date' => $this->input->post('start_date'),
            'end_date' => $this->input->post('end_date'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('courses',$data);
    }
    /**
     * change status from courses table by id
     */
    public function change_status($id, $status){
        $data = array(
            'status' => $status,
        );
        $this->db->where('id',$this->input->get('id'));
        $this->db->update('courses',$data);
    }
    /**
     * update active field to 0 by id from courses table
     */
    public function delete(){
        $data = array(
            'active' => 0,
        );
        $this->db->where('id',$this->input->get('id'));
        $this->db->update('courses',$data);
    }
    /**
     * Get row by id from courses table
     */
    public function get($id) {
        $this->db->select()->from('courses');
        $this->db->where('id', $id);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    /**
     * Get all course list by active 1
     */
    public function get_course_list(){
        $this->db->select()->from('courses');
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get files by course_id from course_files table
     */
    public function get_file_list_by_course_id($id){
        $this->db->select()->from('course_files');
        $this->db->where('course_id',$id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get files by course_id with file_type is 1 from course_files table
     */
    public function get_file_list_by_course_id_and_type($id){
        $this->db->select()->from('course_files');
        $this->db->where('course_id',$id);
        $this->db->where('file_type','1');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Add file to course_files table
     */
    public function insert_file($fileName, $file_name, $contentType, $ext, $id, $file_type, $lectureId){
        $data = array (
            'fileName' => $fileName,
            'contentType' => $contentType,
            'ext' => $ext,
            'course_id' => $id,
            'lecture_id' => $lectureId,
            'file_name' => $file_name,
            'file_type' => $file_type,
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('course_files',$data);
        return $this->db->insert_id();
    }
    /**
     * Update location field by id from course_files table
     */
    public function updateLocation($id, $location){
        $data = array (
            'location' => $location,
        );
        $this->db->where('id',$id);
        $this->db->update('course_files',$data);
    }
    /**
     * Delete file by id from course_files table
     */
    public function delete_file(){
        $this->db->where('id',$this->input->get('id'));
        $this->db->delete('course_files');
    }
    /**
     * Delete file by id from course_files table
     */
    public function delete_file_by_id($file_id){
        $this->db->where('id',$file_id);
        $this->db->delete('course_files');
    }
    /**
     * Get list of courses with registered field of student_courses table by user_id from courses table
     */
    public function get_course_list_with_student_courses($userId){
        $this->db->select('courses.*,student_courses.registered');
        $this->db->join('student_courses','courses.id = student_courses.course_id','left outer');
        $this->db->from('courses');
        $this->db->where('active', 1);
        $where = "(student_courses.user_id = $userId OR student_courses.user_id is null)";
        $this->db->where($where);
        $this->db->order_by('courses.id');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    /**
     * Get student_courses row by user_id and course_id
     */
    public function get_student_course_by_user_id_and_course_id($userId, $courseId){
        $this->db->select()->from('student_courses');
        $this->db->where('course_id',$courseId);
        $this->db->where('user_id',$userId);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    /**
     * Add row to student_courses table
     */
    public function add_student_course($courseId, $status, $userId){
        $data = array(
            'course_id' => $courseId,
            'user_id' => $userId,
            'registered' => $status,
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('student_courses',$data);
        return $this->db->insert_id();
    }
}