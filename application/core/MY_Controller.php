<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property User $User
 * @property Course $Course
 * @property StudentCourse $StudentCourse
 * @property StudentSolution $StudentSolution
 * @property Exam $Exam
 * @property Question $Question
 * @property Comment $Comment
 * @property Lecture $Lecture
 * @property StudentExam $StudentExam
 * @property StudentAnswer $StudentAnswer
 * @property DeliveryBoyFeedback $DeliveryBoyFeedback
 * @property Banner $Banner
 */

class MY_Controller extends CI_Controller {

    public $site_data; 
    
    public function __construct() {       
        parent::__construct();    
        $this->load->database();
        $this->load->model('Properties');
        // load accordingly language
//        $this->lang->load('message','marathi');
        
//        $this->load->model('Category');
//        $this->site_data = $this->Category->get_category_list();
    }
    
    var $SESSION_UESR_ID;
    var $SESSION_UESR_ROLE;
    var $MAX_NO_OF_COURSES;
    var $MAX_NO_OF_EXAMS_PER_COURSE;
    var $MAX_NO_OF_QUESTIONS_PER_EXAM;
    
    public function getConfigProperties(){
        $row = $this->Properties->get();
        if($row){
            $this->MAX_NO_OF_COURSES = $row['no_of_courses'];
            $this->MAX_NO_OF_EXAMS_PER_COURSE = $row['no_of_exams_per_course'];
            $this->MAX_NO_OF_QUESTIONS_PER_EXAM = $row['no_of_questions_per_exam'];
        }
    }
    
    public function getMaxCoursesCount(){
        return $this->MAX_NO_OF_COURSES;
    }
    
    public function getMaxExamsCount(){
        return $this->MAX_NO_OF_EXAMS_PER_COURSE;
    }
    
    public function getMaxQuestionsCount(){
        return $this->MAX_NO_OF_QUESTIONS_PER_EXAM;
    }
    
    public function checkLogin(){        
        if(!$this->session->userdata('logged_in')){              
            redirect("/login");
        }
    }
    
    public function checkRole($role){
        $userdata = $this->session->userdata('logged_in');    
        if($userdata['role'] != $role){              
            show_404();
        }
        
       $this->SESSION_UESR_ID = $userdata['user_id']; 
       $this->SESSION_UESR_ROLE = $userdata['role'];     
    }
	
    public function checkRoles($roles = array()){
        $userdata = $this->session->userdata('logged_in');  
        $roleFound = false;
        for($i =0; $i < count($roles); $i++){  
        if($userdata['role'] == $roles[$i]){
                $roleFound = true;              	            
        }
        }

        if(!$roleFound){
                show_404();
        }
        
       $this->SESSION_UESR_ID = $userdata['user_id']; 
       $this->SESSION_UESR_ROLE = $userdata['role'];     
    }
    
    public function getSessionUserId(){
        return $this->SESSION_UESR_ID;
    }
    
    public function getSessionUserRole(){
        return $this->SESSION_UESR_ROLE;
    }   
}