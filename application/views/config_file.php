<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- This should be from i18n file -->    
    <title>Registration of Courses</title>   
    
    <!-- Navigation -->
    <?php $this->load->view('include/css', 'refresh'); ?> 
    <!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">-->
  </head>
  <body>
    
    <!-- Navigation -->
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <!------ Include the above in your HEAD tag ---------->
    <div class="container" style="margin-top: 10%;">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
                            <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                            <div class="panel panel-login" style="border: 1px solid #e6e6e6">
					<div class="panel-heading">
                                            <h1>Configuration of Database</h1>    
					</div>
					<div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <form id="configForm" action="<?php echo site_url('update_database'); ?>" method="post" role="form" style="display: block;">
                                                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                        <input name="action" type="hidden" value="db_config" class="form-control">
                                                        <div class="form-group">
                                                            <label>DB Username : </label>
                                                            <input type="text" name="username" id="username" class="form-control" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label>DB Password : </label>
                                                            <input type="password" name="password" id="password" class="form-control" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label>DB Name : </label>
                                                            <input type="text" name="database" id="database" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>DB Type : </label>
                                                            <select class="form-control" name="dbdriver" id="dbdriver">
                                                                <option value="mysqli">MySQL</option>
                                                                <option value="postgre">postgreSQL</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-sm-6 col-sm-offset-3">
                                                                    <input type="button" class="form-control btn btn-login btn-info" value="Next" onclick="checkConnection()">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
					</div>
				</div>
			</div>
		</div>
	</div>
    
    <!--/ Contact-->
    
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
<!--    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>-->
    <script type="text/javascript">

        $(function(){
            $("#configForm").validate({
                rules : {
                    username : {required : true},
                    password : {required :true},
                    database : {required : true},
                    dbdriver : {required : true},
                }
            });
            

//            $("#forgetPasswordForm").validate({
//                rules : {
//                    email : {required : true, email : true},
//                }
//            });
//
//            $('#forget_password_form_link').on('click',function(){
//                $('#forgetPasswordForm').show();
//                $('#loginForm').hide();
//                $('#forget_password_form_link').hide();
//                $('#login_form_link').show();
//            });
//
//            $('#login_form_link').on('click',function(){
//                $('#forgetPasswordForm').hide();
//                $('#loginForm').show();
//                $('#forget_password_form_link').show();
//                $('#login_form_link').hide();
//            });

        });
        
        function checkConnection(){
            var username = $('#username').val();
            var password = $('#password').val();
            var database = $('#database').val();
            var dbdriver = $('#dbdriver').val();
            $.ajax({
                url: "<?php echo site_url('connection-check?username='); ?>"+username+'&password='+password+'&database='+database+'&dbdriver='+dbdriver,
                cache: false
            }).done(function( result ) {
                if(result=="1") {
                    $('#configForm').submit();
                  /*jquery code to show a green light on a section in my html page*/
                }
                else
                {
                    alert('Connection not successful');
                  /*jquery code to show a red light*/}
            });
        }

    </script>
  </body>
</html>






