<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- This should be from i18n file -->    
    <title>Registration of Courses</title>   
    
    <!-- Navigation -->
    <?php $this->load->view('include/css', 'refresh'); ?> 
    <!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">-->
  </head>
  <body>
    
    <!-- Navigation -->
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <!------ Include the above in your HEAD tag ---------->
    <div class="container" style="margin-top: 10%;">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
                            <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                            <?php
                                if(array_key_exists('REMOTE_USER', $_SERVER) && strlen($_SERVER['REMOTE_USER'])){
                                    
                                } else {
                                    echo '<div class="panel-heading" style="text-align: center"><h3><a href="/tu/wtc/?prev=https://www.tu-chemnitz.de/urz/www/auth/wtc.html"> Login </a></h3></div>';
                                }
                            ?>
			</div>
		</div>
	</div>
    
    <!--/ Contact-->
    
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
<!--    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>-->
    <script type="text/javascript">

        $(function(){
            $("#loginForm").validate({
                rules : {
                    email : {required : true, email : true},
                    password : {required :true},
                }
            });

//            $("#forgetPasswordForm").validate({
//                rules : {
//                    email : {required : true, email : true},
//                }
//            });
//
//            $('#forget_password_form_link').on('click',function(){
//                $('#forgetPasswordForm').show();
//                $('#loginForm').hide();
//                $('#forget_password_form_link').hide();
//                $('#login_form_link').show();
//            });
//
//            $('#login_form_link').on('click',function(){
//                $('#forgetPasswordForm').hide();
//                $('#loginForm').show();
//                $('#forget_password_form_link').show();
//                $('#login_form_link').hide();
//            });

        });

    </script>
  </body>
</html>






