<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
    <style type="text/css">
        .nav li a{
            background-color: darkgrey;
        }
        
        .nav li a:hover{
            background-color: lightskyblue;
        }
    </style>
  </head>
  <body>
    
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            Courses
            <p class="pull-right">
                <a href="<?php echo site_url('student/course/exam-list?id='.$courseId); ?>" class="btn btn-info">Exams</a>
                <a href="<?php echo site_url('student/course/comments?id='.$courseId); ?>" class="btn btn-info">Forum</a>
                <a href="<?php echo site_url('student/courses'); ?>" class="btn btn-info">Back</a>
            </p>
        </h1>
        
        <div class="clearfix"></div>
    	<div class="row">
            
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Course Information : <?php echo $name; ?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <label>Course Description : </label>

                            <p><?php echo $description; ?></p>
                            <hr>
                        </div>
                        <br>
                        <div class="col-md-12">
                            <label>Course Content :</label>
                            <div class="table-responsive">
                                <table class="table table-bordered table-responsive table-striped" style="background-color: #eee">
                                    <thead>
                                        <tr>
                                            <th style="width: 10%">Name</th>
                                            <th style="width: 10%">Date</th>
                                            <th style="width: 10%">Content</th>
                                            <th style="width: 40%">Homework</th>
                                            <!--<th style="width: 20%">Teacher Solution</th>-->
                                            <th style="width: 20%">Student Solution</th>
                                            <th style="width: 10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            if($lectureList){
                                                foreach ($lectureList as $key => $value) {
                                        ?>
                                            <tr>
                                                <td><?php echo $value['name']?></td>
                                                <td><?php echo $value['date']?></td>
                                                <td>
                                                    <?php 
                                                    if($value['contentList']){
                                                        foreach ($value['contentList'] as $key => $file) {
                                                            if($file['file_type'] == 0){
//                                                                echo '<a href="'.base_url().'uploads/files/courses/'.$file['id'].'.'.$file['ext'].'">'.$file['file_name'].'</a><br>';
                                                                echo '<a target="_blank" href="'.site_url('get/files?id='.$file['id'].'&type=course').'">'.$file['file_name'].'</a><br>';
                                                            }
                                                            
                                                        }
                                                    } else {
                                                        echo '<p>No content available</p>'; 
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    if($value['homeworkList']){
                                                        foreach ($value['homeworkList'] as $key => $file) {
                                                            echo '<div class="col-md-12" style="border-bottom: 2px solid #ddd;">';
                                                            echo '<div class="col-md-6">';
                                                            echo '<a target="_blank" href="'.site_url('get/files?id='.$file['id'].'&type=course').'">'.$file['file_name'].'</a><br>';
                                                            echo '</div>';
                                                            echo '<div class="col-md-6" style="border-left: 2px solid #ddd;">';
                                                            if($file['teacherSolutionList']){
                                                                echo 'Teacher Solutions<br>';
                                                                echo '<div class="col-md-12"  style="border-bottom: 2px solid #ddd;"></div>';
                                                                foreach ($file['teacherSolutionList'] as $key => $solution) {
                                                                    echo '<a target="_blank" href="'.site_url('get/files?id='.$solution['id'].'&type=course').'">'.$solution['file_name'].'</a><br>';
                                                                    echo '<div class="col-md-12"  style="border-bottom: 1px solid #ddd;"></div>';
                                                                }
                                                            }  else {
                                                                echo 'No solution available';
                                                            }
                                                            echo '</div>';
                                                            echo '</div>';
                                                            
                                                            
                                                        }
                                                    } else {
                                                        echo '<p>No content available</p>'; 
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    if($value['solutionList']){
                                                        foreach ($value['solutionList'] as $key => $file) {
                                                            echo '<div class="col-md-12">';
                                                            echo '<a target="_blank" href="'.site_url('get/files?id='.$file['id'].'&type=solution').'">'.$file['file_name'].'</a>';
                                                            echo '<button style="margin-bottom : 1px;" type="button" class="btn btn-info btn-xs pull-right" title="Delete" onclick="delete_file('.$file['id'].')"><i class="fas fa-trash-alt"></i></button>';
                                                            echo '<a style="margin-right : 5px;" href="'. site_url('student/solutions/comments?id='.$file['id']).'" class="btn btn-info btn-xs pull-right" title="Comment" ><i class="fas fa-comment"></i></a>';
                                                            echo '</div>';
                                                            echo '<div class="col-md-12"  style="border-bottom: 2px solid #ddd;"></div>';
                                                        }
                                                    } else {
                                                        echo '<p>No solution available</p>'; 
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <a class="btn btn-info btn-sm" onclick="openModal(<?php echo $value['id']; ?>)">Upload Solution</a>
                                                    <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Upload Solution</button>-->
                                                    <!--<a class="btn btn-info btn-sm" onclick="deleteInfo(<?php echo $value['id'];?>)">Delete</a>-->
                                                </td>
                                            </tr>
                                        <?php
                                                }
                                            } else {
                                        ?>
                                            <tr><td colspan="6">No record found!</td></tr>
                                        <?php
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p><a href="<?php echo site_url('student/courses'); ?>" class="btn btn-info">Back</a></p>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <form enctype="multipart/form-data" id="solutionFileForm" class="form-horizontal" method="post" action="<?php echo site_url('student/course/lectures/save/file'); ?>" >
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                <input type="hidden" name="lecture_id" id="lecture_id" value="" class="form-control"/>
                <input type="hidden" name="course_id" id="course_id" value="<?php echo $id; ?>" class="form-control"/>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Upload File</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group"> 
                            <div class="col-md-6">
                                <label class="control-label">File Name : </label>
                                <select class="form-control" name="file_id" id="file_id">
                                    <option value="">--Select--</option>
                                </select>
                                <input style="margin-top: 10px;" type="file" name="userfile[]" required="required" id="file" onchange="return ValidateFileUpload(this)" accept=".pdf, .doc, .docx" size="20"/>
                                <small>(max upload file size is 2MB and allowed file types are PDF,DOC,DOCX)</small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info" ><i class="fas fa-upload"></i> Upload</button>
                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
//            $("#courseForm").validate({
//                rules : {
//                    name : {required : true},
//                    description : {required :true},
//                    
//                }
//            });

            $('#commentForm').validate({
                rules : {
                    comment : {required : true, minlength : 10},
                }
            });
            
            $("#solutionFileForm").validate({
                rules : {
                    file_id : {required : true},
                    userfile : {required : true},
                }
            });
            
            var tab = "<?php echo $this->input->get('tab'); ?>";
            if(tab == 3){
                openTab3();
            } else if(tab == 2){
                openTab2();
            } else {
//                openTab1();
                openTab3();
            }
            
        });
        
        function openModal(lectureId){
            
            $('#lecture_id').val("");
            $('#file_id').find('option').remove();
            $('#file_id').append('<option value="">--Select--</option>');
            $('#lecture_id').val(lectureId);
            $.ajax({
                url: '<?php echo site_url("student/course/lectures/get-file?id="); ?>'+lectureId,
                type: 'GET',
                dataType :'json',
                success: function (data) {
                    
                    if(data){
                        for( var i = 0; i < data.length; i++ ){
                            $('#file_id').append('<option value="'+data[i].id+'">'+data[i].file_name+'</option>');
                        }
                    }
                }
            });
            $('#myModal').modal('show');
        }
        
        function ValidateFileUpload(file){
            var fuData = document.getElementById('file');
            var FileUploadPath = fuData.value;
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

            //The file uploaded is an image
            if (Extension == "pdf" || Extension == "doc" || Extension == "docx") {
                var fileName = $(file).val();
                var fileSize = file.files[0].size;
                if(fileName){
                    if( fileSize > 2097152){//5242880
                        $(file).val("");
                        alert('Please upload less than 2MB file');
                        return false;
                    } 
                }
                return;
            } else {
                $('#file').val('');
                alert("Only allows file types of DOC, DOCX, PDF");
            }
        }
        
        function delete_file(id){
            BootstrapDialog.show({      
                    message: 'Are you sure you want to delete this File?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'Delete',
                        cssClass:'btn-danger',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('student/course/lectures/delete/file?id='); ?>"+id+'&course_id='+<?php echo $id; ?>;
                            window.location = url;
                        }
                    }]
                });
        }
        
        function openTab1(){
            $('#tab1').show();
            $('#tab2').hide();
            $('#tab3').hide();
            
            $('#liTab1').addClass('active');
            $('#liTab2').removeClass('active');
            $('#liTab3').removeClass('active');
        }
        
        function openTab2(){
            $('#tab1').hide();
            $('#tab2').show();
            $('#tab3').hide();
            
            $('#liTab1').removeClass('active');
            $('#liTab2').addClass('active');
            $('#liTab3').removeClass('active');
        }
        
        function openTab3(){
            $('#tab1').hide();
            $('#tab2').hide();
            $('#tab3').show();
            
            $('#liTab1').removeClass('active');
            $('#liTab2').removeClass('active');
            $('#liTab3').addClass('active');
        }
        
        function takeExam(id){
            BootstrapDialog.show({      
                    message: 'Are you sure you want start exam?',
                    title : "Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'Start',
                        cssClass:'btn-success',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('student/course/exams/start?id='); ?>"+id+'&course_id='+<?php echo $id; ?>;
                            window.location = url;
                        }
                    }]
                });
        }

    </script>
  </body>
</html>