<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
  </head>
  <body>
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            <?php echo $course['name'];?> - Comments
            <a class="btn btn-info pull-right" href="<?php echo site_url('student/courses/edit?id='.$courseId); ?>">Back</a>
        </h1>
        
        <div class="clearfix"></div>
    	<div class="row">
            
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <form id="commentForm" class="form" action="<?php echo site_url('student/course/comments/save'); ?>" method="post">
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                <input type="hidden" name="id" value="<?php echo $courseId; ?>" class="form-control"/>
                                <div class="col-md-12">
                                    <textarea name="comment" class="form-control" rows="3"></textarea>
                                    <button style="margin-top: 5px;" type="submit" class="btn btn-info">Submit</button>
                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>

                        <hr>
                        <div class="col-md-12">
                            <?php 
                                if($forumList){
                                    foreach ($forumList as $key => $value) {
                            ?>
                            <div style="padding: 10px;border: 1px solid #eee;margin-bottom: 20px;">
                                <p><?php echo $value['createdAt'].' - '.$value['first_name'].' '.$value['last_name']; ?></p>
                                <p><?php echo $value['comment']; ?></p>
                            </div>
                            <div class="clearfix"></div>
                            <?php
                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-info" href="<?php echo site_url('student/courses/edit?id='.$courseId); ?>">Back</a>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            $("#commentForm").validate({
                rules : {
                    comment : {required :true},
                }
            });
        });
    </script>
  </body>
</html>