<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
  </head>
  <body>
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            Courses
            <a class="btn btn-info pull-right" href="<?php echo site_url('student/dashboard'); ?>">Back</a>
        </h1>
        
        <div class="clearfix"></div>
    	<div class="row">
            
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>
                            Course List
                            
                        </h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-responsive table-striped" style="background-color : #eee">
                                <thead>
                                    <tr>
                                        <th style="width: 40%">Name</th>
                                        <th style="width: 20%">Course Registration Deadline</th>
                                        <th style="width: 20%">Registration Status</th>
                                        <th style="width: 20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if($courseList){
                                            foreach ($courseList as $key => $value) {
                                    ?>
                                    <tr>
                                        <td><?php echo (($value['registered'] == 1)?'<a href="'. site_url('student/courses/edit?id='.$value['id']).'">'.$value['name'].'</a>':$value['name']);?></td>
                                        <?php 
//                                        $start_date = $value['start_date'];
//                                        $sdate = DateTime::createFromFormat('d/m/Y', $start_date);
//                                        $startDate = $sdate->format('d M Y');

                                        $end_date = $value['end_date'];
                                        $edate = DateTime::createFromFormat('d/m/Y', $end_date);
                                        $endDate = $edate->format('d M Y');

                                        echo '<td>'.$endDate.'</td>';
                                        ?>
                                        <td><?php echo (($value['registered'] == 1)?'Registered':'Not Register'); ?></td>
                                        <td>
                                            <?php if($value['is_expired'] == 'active') { ?>
                                            <a class="btn <?php echo (($value['registered']== 1)?'btn-success':'btn-warning'); ?> btn-sm" onclick="changeRegistration(<?php echo (($value['registered']== 1)?1:0); ?>,<?php echo $value['id']; ?>)" ><?php echo (($value['registered'] == 1)?'Deregistered':'Register'); ?></a>
                                            <?php } else {
                                                echo 'Course is expired!';
                                            }?>
                                        </td>
                                    </tr>

                                    <?php
                                            }
                                        } else {
                                    ?>
                                    <tr><td colspan="4">No record found!</td></tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-info" href="<?php echo site_url('student/dashboard'); ?>">Back</a>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            $("#loginForm").validate({
                rules : {
                    email : {required : true, email : true},
                    password : {required :true},
                }
            });
        });
        
        function changeRegistration(status,id){
            if(status == 1){
                var data = 'deregister';
            } else {
                var data = 'register';
            }
            BootstrapDialog.show({      
                    message: 'Are you sure you want to '+data+' for this course?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'OK',
                        cssClass:'btn-warning',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('student/courses/change-registration-status?status='); ?>"+status+'&id='+id;
                            window.location = url;
                        }
                    }]
                });
        }
    </script>
  </body>
</html>