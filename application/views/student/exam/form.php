<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
    <style>
        * {
          box-sizing: border-box;
        }

        body {
          background-color: #f1f1f1;
        }

        #examQuestionForm {
          background-color: #ffffff;
          /*margin: 100px auto;*/
          font-family: Raleway;
          padding: 20px;
          width: 100%;
          /*min-width: 300px;*/
        }

        h1 {
          text-align: center;  
        }

        input {
          padding: 10px;
          width: 100%;
          font-size: 17px;
          font-family: Raleway;
          border: 1px solid #aaaaaa;
        }

        /* Mark input boxes that gets an error on validation: */
        input.invalid {
          background-color: #ffdddd;
        }

        /* Hide all steps by default: */
        .tab {
          display: none;
        }

        button {
          background-color: #4CAF50;
          color: #ffffff;
          border: none;
          padding: 10px 20px;
          font-size: 17px;
          font-family: Raleway;
          cursor: pointer;
        }

        button:hover {
          opacity: 0.8;
        }

        #prevBtn {
          /*background-color: #bbbbbb;*/
        }

        /* Make circles that indicate the steps of the form: */
        .step {
          height: 15px;
          width: 15px;
          margin: 0 2px;
          background-color: #bbbbbb;
          border: none;  
          border-radius: 50%;
          display: inline-block;
          opacity: 0.5;
        }

        .step.active {
          opacity: 1;
        }

        /* Mark the steps that are finished and valid: */
        .step.finish {
          background-color: #4CAF50;
        }
    </style>
  </head>
  <body>
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <div class="container" style="margin-top: 5%;width: 80% !important;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            <?php echo $course['name'];?> - Exams
            <a class="btn btn-info pull-right" href="<?php echo site_url('student/courses/edit?id='.$courseId.'&tab=2'); ?>">Back</a>
        </h1>
        
        <div class="clearfix"></div>
    	<div class="row">
            <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?php if($studentQuestionList){ ?>
                        <div class="box">
                            <div class="col-md-12" style="font-size: 20px;">
                                <div class="col-md-4">
                                    Total Questions - <?php echo count($studentQuestionList); ?>
                                </div>
                                <div class="col-md-4" style="text-align: center">
                                    <p id="demo" style="background-color: #ddd"></p>
                                </div>
                                <div class="col-md-4">
                                    <span class="pull-right">
                                        Max Time - <?php echo $exam['time_limit']; ?> Min
                                    </span>
                                </div>
                            </div>
                        </div>
                        <form id="examQuestionForm" method="post" action="<?php echo site_url('student/course/exams/submit'); ?>" class="form-horizontal" >
                            <input type="hidden" name="examId" value="<?php echo $examId; ?>" >
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <div class="col-md-12">
                            <?php
                            if($studentQuestionList){
                                $number = 1;
                                foreach ($studentQuestionList as $key => $value) {
                            ?>
                            <div class="tab">
                                <h3><?php echo $number.'. '.$value['question']['question']; ?><p class="pull-right">Marks : <?php echo $value['question']['marks'];?></p></h3>
                                <input type="hidden" name="questionId[]" value="<?php echo $value['id']; ?>" >
                                <input type="hidden" name="questionType[]" value="<?php echo $value['question']['question_type']; ?>" >
                                    <?php
                                    $question_type = $value['question']['question_type'];
                                    if($question_type == 'multichoice' || $question_type == 'fillintheblank'){
                                        $questionOptions = $value['question']['questionOptions'];
                                        for($i = 0; $i < count($questionOptions); $i++){
                                            echo '<div class="form-group">';
                                            echo '<div class="col-md-1">';
                                            echo '<input type="checkbox" name="solution_'.$value['id'].'[]" value="'.$questionOptions[$i]['id'].'">';
                                            echo '</div>';
                                            echo '<div class="col-md-6"><label>'.$questionOptions[$i]['option'].'</label></div>';
                                            echo '</div>';
                                        }
                                    } else if($question_type == 'truefalse'){
                                        echo '<div class="form-group">';
                                        echo '<div class="col-md-1">';
                                        echo '<input type="radio" name="solution_'.$value['id'].'" value="True">';
                                        echo '</div>';
                                        echo '<div class="col-md-6"><label>True</label></div>';
                                        echo '</div>';
                                        echo '<div class="form-group">';
                                        echo '<div class="col-md-1">';
                                        echo '<input type="radio" name="solution_'.$value['id'].'" value="False">';
                                        echo '</div>';
                                        echo '<div class="col-md-6"><label>False</label></div>';
                                        echo '</div>';
                                    } else {
                                        echo '<textarea name="solution_'.$value['id'].'" rows="2" class="form-control"></textarea>';
                                    }
                                    ?>
                            </div>
                            <?php
                            $number++;
                                }
                            }
                            ?>
                                 <!--Circles which indicates the steps of the form:--> 
                            <div style="text-align:center;margin-top:40px;">
                                <?php
                            if($studentQuestionList){
                                foreach ($studentQuestionList as $key => $value) {
                            ?>
                                <span style="opacity : 0" class="step"></span>
                            <?php
                                }
                            }
                            ?>
                            </div>
                            </div>
                            <div class="col-md-12">
                                <div style="overflow:auto;">
                                  <div >
                                      <button style="float:left;" type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                                      <button style="float:right;" type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
                                  </div>
                                </div>
                            </div>
                        </form>
                        <?php } else {
                            echo 'No questions available.';
                        }?>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-info" href="<?php echo site_url('student/courses/edit?id='.$courseId.'&tab=2'); ?>">Back</a>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            $("#loginForm").validate({
                rules : {
                    email : {required : true, email : true},
                    password : {required :true},
                }
            });
            
//            $('.btnNext').click(function(){
//                $('.nav-tabs > .active').next('li').find('a').trigger('click');
//            });

//            $('.btnPrevious').click(function(){
//                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
//            });

            setTimeout(function(){ exexute_timer(); }, 1000);
        });
        
        function changeRegistration(status,id){
            if(status == 1){
                var data = 'deregister';
            } else {
                var data = 'register';
            }
            BootstrapDialog.show({      
                    message: 'Are you sure you want to '+data+' for this course?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'Yes',
                        cssClass:'btn-warning',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('student/courses/change-registration-status?status='); ?>"+status+'&id='+id;
                            window.location = url;
                        }
                    }]
                });
        }
    </script>
    <script type="text/javascript">
        // Set the date we're counting down to
            var someVarName = localStorage.getItem("someVarKey");
//            console.log(someVarName);
            if(someVarName != null){
                var countDownDate = someVarName;
            } else {
//                console.log('if null');
                var twentyMinutesLater = new Date();
                var countDownDate = twentyMinutesLater.setMinutes(twentyMinutesLater.getMinutes() + <?php echo $exam['time_limit']; ?>);
                localStorage.setItem("someVarKey", countDownDate);
            }
            function exexute_timer(){
                if (!(localStorage.getItem("someVarKey") === null)) {
                    // Update the count down every 1 second
                    var x = setInterval(function() {
                        // Get todays date and time
                        var now = new Date().getTime();

                        // Find the distance between now and the count down date
                        var distance = countDownDate - now;

                        // Time calculations for days, hours, minutes and seconds
                //            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                //            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                        // Output the result in an element with id="demo"
                        document.getElementById("demo").innerHTML = minutes + ": " + seconds + "";

                        // If the count down is over, write some text 
                        if(localStorage.getItem("someVarKey") != null){
                            if (distance < 0) {
                                clearInterval(x);
                                localStorage.removeItem("someVarKey");
                                document.getElementById("demo").innerHTML = "FINISHED";
                                document.getElementById("examQuestionForm").submit();
                            }
                        }
                    }, 1000);
                } 
            }
        
    </script>
        <script>
            var currentTab = 0; // Current tab is set to be the first tab (0)
            showTab(currentTab); // Display the crurrent tab

            function showTab(n) {
              // This function will display the specified tab of the form...
              var x = document.getElementsByClassName("tab");
              x[n].style.display = "block";
              //... and fix the Previous/Next buttons:
              if (n == 0) {
                document.getElementById("prevBtn").style.display = "none";
              } else {
                document.getElementById("prevBtn").style.display = "inline";
              }
              if (n == (x.length - 1)) {
                document.getElementById("nextBtn").innerHTML = "Submit";
              } else {
                document.getElementById("nextBtn").innerHTML = "Next";
              }
              //... and run a function that will display the correct step indicator:
              fixStepIndicator(n)
            }

            function nextPrev(n) {
              // This function will figure out which tab to display
              var x = document.getElementsByClassName("tab");
              // Exit the function if any field in the current tab is invalid:
              if (n == 1 && !validateForm()) return false;
              // Hide the current tab:
              
              x[currentTab].style.display = "none";
              // Increase or decrease the current tab by 1:
              currentTab = currentTab + n;
              // if you have reached the end of the form...
              if (currentTab >= x.length) {
                // ... the form gets submitted:
                localStorage.removeItem("someVarKey");
                document.getElementById("examQuestionForm").submit();
                return false;
              }
              // Otherwise, display the correct tab:
              showTab(currentTab);
            }

            function validateForm() {
              // This function deals with validation of the form fields
//              var x, y, i, valid = true;
//              x = document.getElementsByClassName("tab");
//              y = x[currentTab].getElementsByTagName("input");
//              // A loop that checks every input field in the current tab:
//              for (i = 0; i < y.length; i++) {
//                // If a field is empty...
//                if (y[i].value == "") {
//                  // add an "invalid" class to the field:
//                  y[i].className += " invalid";
//                  // and set the current valid status to false
//                  valid = false;
//                }
//              }
//              // If the valid status is true, mark the step as finished and valid:
//              if (valid) {
//                document.getElementsByClassName("step")[currentTab].className += " finish";
//              }
                var valid = true;
                return valid; // return the valid status
            }

            function fixStepIndicator(n) {
              // This function removes the "active" class of all steps...
              var i, x = document.getElementsByClassName("step");
              for (i = 0; i < x.length; i++) {
                x[i].className = x[i].className.replace(" active", "");
              }
              //... and adds the "active" class on the current step:
              x[n].className += " active";
            }
        </script>
  </body>
</html>