<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
  </head>
  <body>
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <div class="container" style="margin-top: 5%;width: 80% !important;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            <?php echo $course['name'];?> - Exams
            <a class="btn btn-info pull-right" href="<?php echo site_url('student/course/exam-list?id='.$courseId); ?>">Back</a>
        </h1>
        
        <div class="clearfix"></div>
    	<div class="row">
            <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2><?php echo $exam['name']; ?></h2>
                        <hr>
                        <p>The exam contains <?php echo $examCount; ?> questions and there is time limit of <?php echo $exam['time_limit']; ?> mins.</p>
                        <hr>
                        <h2>Start the Exam</h2>
                        <p>Good luck!</p>
                        <?php if($examCount > 0) {?>
                        <a href="<?php echo site_url('student/course/exams/start?examId='.$exam['id']); ?>" class="btn btn-warning">Start the Exam</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-info" href="<?php echo site_url('student/course/exam-list?id='.$courseId); ?>">Back</a>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            $("#loginForm").validate({
                rules : {
                    email : {required : true, email : true},
                    password : {required :true},
                }
            });
        });
        
        function changeRegistration(status,id){
            if(status == 1){
                var data = 'deregister';
            } else {
                var data = 'register';
            }
            BootstrapDialog.show({      
                    message: 'Are you sure you want to '+data+' for this course?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'Yes',
                        cssClass:'btn-warning',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('student/courses/change-registration-status?status='); ?>"+status+'&id='+id;
                            window.location = url;
                        }
                    }]
                });
        }
        
        function deleteInfo(id){
            BootstrapDialog.show({      
                    message: 'Are you sure you want to delete this Course?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'Delete',
                        cssClass:'btn-danger',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('teacher/courses/delete?id='); ?>"+id;
                            window.location = url;
                        }
                    }]
                });
        }
    </script>
  </body>
</html>