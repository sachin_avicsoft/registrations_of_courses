<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
  </head>
  <body>
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            <?php echo $course['name'];?> - Exams
            <a class="btn btn-info pull-right" href="<?php echo site_url('student/courses/edit?id='.$courseId); ?>">Back</a>
        </h1>
        
        <div class="clearfix"></div>
    	<div class="row">
            
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <th>Exam</th>
                                        <th>Exam Registration Deadline</th>
                                        <th>Time Limit</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        if($examList){
                                            foreach ($examList as $key => $value) {
//                                                if($value['is_expired'] == 'active'){
                                                    echo '<tr>';
                                                    echo '<td>'.$value['name'].'</td>';
//                                                    $start_date = $value['start_date'];
//                                                    $sdate = DateTime::createFromFormat('d/m/Y', $start_date);
//                                                    $startDate = $sdate->format('d M Y');

                                                    $end_date = $value['end_date'];
                                                    $edate = DateTime::createFromFormat('d/m/Y', $end_date);
                                                    $endDate = $edate->format('d M Y');

                                                    echo '<td>'.$endDate.'</td>';
                                                    echo '<td>'.$value['time_limit'].'</td>';
                                                    if($value['resultStatus'] != '' && $value['resultGrade'] != '' && $value['resultStatus'] != NULL && $value['resultGrade'] != NULL){
                                                        echo '<td>Result : '.$value['resultStatus'].', Grade : '.$value['resultGrade'].'</td>';
                                                    } else {
                                                        if($value['submitStatus'] == 'Appeared'){
                                                            echo '<td>Exam is Submitted</td>';
                                                        } else if($value['is_expired'] == 'active'){
                                                            echo (($value['questionCount'] > 0)?'<td><a href="'. site_url('student/course/exams?id='.$value['id'].'&courseId='.$courseId).'" class="btn btn-info" >Take Exam</a></td>':'<td>-</td>');
                                                        } else {
                                                            echo '<td>Exam is expired!</td>';
                                                        }
                                                    }

//                                                    } 
                                                    echo '</tr>';
//                                                }
                                            }
                                        } else {
                                            echo '<tr><td colspan="4">No records found!</tr>';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-info" href="<?php echo site_url('student/courses/edit?id='.$courseId); ?>">Back</a>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            $("#loginForm").validate({
                rules : {
                    email : {required : true, email : true},
                    password : {required :true},
                }
            });
        });
        
        function changeRegistration(status,id){
            if(status == 1){
                var data = 'deregister';
            } else {
                var data = 'register';
            }
            BootstrapDialog.show({      
                    message: 'Are you sure you want to '+data+' for this course?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'Yes',
                        cssClass:'btn-warning',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('student/courses/change-registration-status?status='); ?>"+status+'&id='+id;
                            window.location = url;
                        }
                    }]
                });
        }
        
        function deleteInfo(id){
            BootstrapDialog.show({      
                    message: 'Are you sure you want to delete this Course?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'Delete',
                        cssClass:'btn-danger',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('teacher/courses/delete?id='); ?>"+id;
                            window.location = url;
                        }
                    }]
                });
        }
    </script>
  </body>
</html>