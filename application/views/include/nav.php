<p>

    <?php 
        $userdata = $this->session->userdata('logged_in'); 
        $role = $userdata['role'];
    ?>
    <?php 
        if($role == 'TEACHER'){
            echo '<a title="Home" href="'.site_url('teacher/dashboard').'"><i class="fas fa-home" style="font-size: 2.5em"></i></a>';
        } else if($role == 'STUDENT'){
            echo '<a title="Home" href="'.site_url('student/dashboard').'"><i class="fas fa-home" style="font-size: 2.5em"></i></a>';
        } else if($role == 'ADMIN'){
            echo '<a title="Home" href="'.site_url('admin/dashboard').'"><i class="fas fa-home" style="font-size: 2.5em"></i></a>';
        }
    ?>
    
    <span class="pull-right">
        <?php 
//            echo '<span style="margin-right : 10px;">Welcome '. $userdata['username'].'</span>';
        ?>
        <?php 
        if($role == 'TEACHER'){
            echo '<span style="margin-right : 10px;">Teacher Portal</span>';
        } else if($role == 'STUDENT'){
            echo '<span style="margin-right : 10px;">Student Portal</span>';
        } else if($role == 'ADMIN'){
            echo '<span style="margin-right : 10px;">Admin Portal</span>';
        }
    ?>
        <a title="Sign Out" onclick="logoutUser()"><i class="fas fa-power-off" style="font-size: 2.5em"></i></a>
        <!--href="<?php echo site_url('logout'); ?>"-->
    </span>
</p>
<div class="clearfix" style="margin-bottom: 2%"></div>