<script src="<?php print_resource_url("js/jquery-1.11.1.min.js"); ?>"></script>
<script src="<?php print_resource_url("bootstrap/js/bootstrap.min.js"); ?>"></script>
<script src="<?php print_resource_url("js/jquery.validate.min.js"); ?>" ></script>
<script src="<?php print_resource_url("js/bootstrap-dialog.js")?>" ></script>
<script src="<?php print_resource_url("plugins/daterangepicker/moment.min.js")?>" ></script>
<script src="<?php print_resource_url("plugins/daterangepicker/daterangepicker.js")?>" ></script>
<script type="text/javascript">
function alphanumeric(inputtxt)
{ 
var letters = /^[0-9a-zA-Z\s]+$/;
    if(inputtxt.value.match(letters)){
        return true;
    } else {
        inputtxt.value = '';
        alert('Please input alphanumeric value only');
        return false;
    }
}

function logoutUser(){
            BootstrapDialog.show({      
                    message: 'Are you sure you want to Sign out?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'OK',
                        cssClass:'btn-warning',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('logout'); ?>";
                            window.location = url;
                        }
                    }]
                });
        }
</script>