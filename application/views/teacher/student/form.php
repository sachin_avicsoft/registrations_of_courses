<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
  </head>
  <body>
    
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            Courses
            <a class="btn btn-info pull-right" href="<?php echo site_url('teacher/courses'); ?>">Back</a>
        </h1>
        <div class="clearfix"></div>
    	<div class="row">
            
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>
                            
                        <?php 
                            if($id == ''){
                                echo 'Add Course';
                            } else {
                                echo 'Edit Course';
                            }
                        ?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        
                        <form id="courseForm" class="form-horizontal" method="post" action="<?php echo site_url('teacher/courses/save'); ?>">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control"/>
                            <input type="hidden" value="<?php echo $status ?>" name="status" class="form-control" />
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label">Name <span class="mandetory_field">*</span></label>
                                    <input type="text" name="name" value="<?php echo $name; ?>" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label">Description (Maximum 240 characters) <span class="mandetory_field">*</span></label>
                                    <textarea class="form-control" name="description" rows="5" id="textarea" maxlength="240"><?php echo $description; ?></textarea>
                                    <span id="rchars"><?php echo $length; ?></span> Character(s) Remaining
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-info" ><i class="fa fa-check"></i> Save</button>
                                    <!--<a type="button" class="btn btn-info" href="<?php echo site_url('teacher/courses'); ?>"><i class="fa fa-reply"></i> Back</a>-->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <?php 
                    if($id != ''){
                ?>
                <div class="panel panel-default">
                    <ul class="nav nav-pills tab-section" style="border-bottom: 1px solid #eee">
                        <!--<li style="width: 20%;padding: 0px;" class="active" id="liTab1"><a style="cursor: pointer" id="upload_solution" onclick="openTab1()">Upload Solution</a></li>-->
                        <!--<li style="width: 20%;padding: 0px;" id="liTab2"><a style="cursor: pointer" id="exams" onclick="openTab2()">Exams</a></li>-->
                        <li style="width: 20%;padding: 0px;" id="liTab3"><a style="cursor: pointer" onclick="openTab3()">Forum</a></li>
                    </ul>
                    <div class="panel-body">
                        <div id="tab1">
                        </div>
<!--                        <div id="tab2">
                            Comming Soon
                        </div>-->
                        <div id="tab3">
                            <div class="col-md-12">
                                <form id="commentForm" class="form" action="<?php echo site_url('teacher/course/comments/save'); ?>" method="post">
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control"/>
                                    <div class="col-md-12">
                                        <textarea name="comment" class="form-control" rows="3"></textarea>
                                        <button style="margin-top: 5px;" type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                            
                            <hr>
                            <div class="col-md-12">
                                <?php 
                                    if($forumList){
                                        foreach ($forumList as $key => $value) {
                                ?>
                                <div style="padding: 10px;border: 1px solid #eee;margin-bottom: 20px;">
                                    <p><?php echo $value['createdAt'].' - '.$value['first_name'].' '.$value['last_name']; ?></p>
                                    <p><?php echo $value['comment']; ?></p>
                                </div>
                                <div class="clearfix"></div>
                                <?php
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            </div>
        <a class="btn btn-info" href="<?php echo site_url('teacher/courses'); ?>">Back</a>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            $("#courseForm").validate({
                rules : {
                    name : {required : true},
                    description : {required :true},
                    
                }
            });
            
            $('#commentForm').validate({
                rules : {
                    comment : {required : true, minlength : 10},
                }
            });
            
            $("#courseFileForm").validate({
                rules : {
                    file_name : {required : true},
                    userfile : {required : true},
                }
            });
            
            var maxLength = 240;
            
            $('#textarea').keyup(function() {
              var textlen = maxLength - $(this).val().length;
              $('#rchars').text(textlen);
            });
            
            var tab = "<?php echo $this->input->get('tab'); ?>";
            if(tab == 3){
                openTab3();
            } else if(tab == 2){
                openTab2();
            } else {
//                openTab1();
                openTab3();
            }
        });
        
        function delete_file(id){
            BootstrapDialog.show({      
                    message: 'Are you sure you want to delete this File?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'Delete',
                        cssClass:'btn-danger',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('teacher/courses/delete/file?id='); ?>"+id+'&course_id='+<?php echo $id; ?>;
                            window.location = url;
                        }
                    }]
                });
        }
        
        function ValidateFileUpload(file){
            
                var fuData = document.getElementById('file');
                var FileUploadPath = fuData.value;
                var Extension = FileUploadPath.substring(
                        FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

                //The file uploaded is an image
                if (Extension == "pdf" || Extension == "doc" || Extension == "docx") {
                    var id = '<?php echo $id; ?>';
                    var fileName = $(file).val();
                    var fileSize = file.files[0].size;
                    if(fileName){
                        if( fileSize > 2097152){//5242880
                              $(file).val("");
                             alert('Please upload less than 2mb file');
                             return false;
                          } 
                    }
                    return;
                } else {
                    $('#file').val('');
                    alert("Only allows file types of DOC, DOCX, PDF");
                }
        }
        
        function openTab1(){
            $('#tab1').show();
            $('#tab2').hide();
            $('#tab3').hide();
            
            $('#liTab1').addClass('active');
            $('#liTab2').removeClass('active');
            $('#liTab3').removeClass('active');
        }
        
        function openTab2(){
            $('#tab1').hide();
            $('#tab2').show();
            $('#tab3').hide();
            
            $('#liTab1').removeClass('active');
            $('#liTab2').addClass('active');
            $('#liTab3').removeClass('active');
        }
        
        function openTab3(){
            $('#tab1').hide();
            $('#tab2').hide();
            $('#tab3').show();
            
            $('#liTab1').removeClass('active');
            $('#liTab2').removeClass('active');
            $('#liTab3').addClass('active');
        }

    </script>
  </body>
</html>