<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
  </head>
  <body>
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            <?php echo $course['name'].' - '.$student['first_name'].' '.$student['last_name'].' - '; ?>Exams
            <a class="btn btn-info pull-right" href="<?php echo site_url('teacher/students?course_id='.$course['id']); ?>">Back</a>
        </h1>
        
        <div class="clearfix"></div>
    	<div class="row">
            
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>
                            Exams
                            <!--<a class="btn btn-info pull-right" href="<?php echo site_url('teacher/course/lectures/add?courseId='.$course['id']); ?>">Add Lecture</a>-->
                        </h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-responsive table-striped" style="background-color: #eee">
                                <thead>
                                    <tr>
                                        <th style="width: 20%">Name</th>
                                        <th style="width: 20%">Status</th>
                                        <th style="width: 20%">Result</th>
                                        <th style="width: 40%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if($examList){
                                            foreach ($examList as $key => $value) {
                                    ?>
                                        <tr>
                                            <td><?php echo $value['name']; ?></td>
                                            <td><?php echo $value['submitStatus']; ?></td>
                                            <td><?php 
                                            if($value['resultStatus'] != '' && $value['resultGrade'] != '' && $value['resultStatus'] != NULL && $value['resultGrade'] != NULL){
                                                echo 'Result : '.$value['resultStatus'].', Grade : '.$value['resultGrade'];
                                            } else {
                                                echo '-';
                                            } 
                                            ?></td>
                                            <td>
                                                <!--<a class="btn btn-info btn-sm" onclick="addComment(<?php echo $value['id'];?>)">Add Comments</a>-->
                                                <a class="btn btn-info btn-sm <?php echo (($value['submitStatus'] == 'Appeared')?'':'disabled'); ?>" href="<?php echo site_url('teacher/students/exams/answers?id='.$value['studentExamId']); ?>">View Answers</a>
                                            </td>
                                        </tr>
                                    <?php
                                            }
                                        } else {
                                    ?>
                                        <tr><td colspan="4">No record found!</td></tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-info" href="<?php echo site_url('teacher/students?course_id='.$course['id']); ?>">Back</a>
    </div>
      <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <form enctype="multipart/form-data" id="solutionFileCommentForm" class="form-horizontal" method="post" action="<?php echo site_url('teacher/students/solutions/comments/save'); ?>" >
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                <input type="hidden" name="student_solution_id" id="studentSolutionId" value="" class="form-control"/>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Comment on Solution</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group"> 
                            <div class="col-md-12">
                                <label class="control-label">Comment (Maximum 240 characters) <span class="mandetory_field">*</span></label>
                                <textarea class="form-control" name="comment" rows="5" id="textarea" maxlength="240"></textarea>
                                <span id="rchars">240</span> Character(s) Remaining
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info" >Save</button>
                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            $("#solutionFileCommentForm").validate({
                rules : {
                    student_solution_id : {required : true, number : true},
                    comment : {required : true, minlength : 10},
                }
            });
            
            var maxLength = 240;
            
            $('#textarea').keyup(function() {
              var textlen = maxLength - $(this).val().length;
              $('#rchars').text(textlen);
            });
        });
        
        function addComment(id){
            $('#myModal').modal('show');
            $('#studentSolutionId').val(id);
        }
        
    </script>
  </body>
</html>