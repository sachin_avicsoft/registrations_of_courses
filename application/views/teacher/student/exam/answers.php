<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
  </head>
  <body>
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            <?php echo $course['name'].' - '.$student['first_name'].' '.$student['last_name'].' - '.$exam['name'].' - '; ?>Answers
            <a class="btn btn-info pull-right" href="<?php echo site_url('teacher/students/exams?course_id='.$course['id'].'&user_id='.$student['id']); ?>">Back</a>
        </h1>
        
        <div class="clearfix"></div>
    	<div class="row">
            
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>
                            Answers
                            <!--<a class="btn btn-info pull-right" href="<?php echo site_url('teacher/course/lectures/add?courseId='.$course['id']); ?>">Add Lecture</a>-->
                        </h3>
                        <?php
                        $outOff = 0;
                        if($examQuestions) {
                            $number = 1;
                            
                            foreach ($examQuestions as $key => $value) {
                        ?>
                        
                        <div class="bodywrapper">
                            <div class="col-md-12">
                                <h3><?php echo $number.'. '.$value['question']['question']; ?></h3>
                                <?php
                                $question_type = $value['question']['question_type'];
                                    if($question_type == 'multichoice' || $question_type == 'fillintheblank'){
                                        $answers = explode(",",$value['solution']);
                                        $questionOptions = $value['question']['questionOptions'];
                                        for($i = 0; $i < count($questionOptions); $i++){
                                            echo '<div class="col-md-1">';
                                            echo '<input type="checkbox" disabled=disabled name="solution_'.$value['id'].'[]" value="'.$questionOptions[$i]['id'].'" '.((in_array($questionOptions[$i]['id'], $answers))?'checked="checked"':'').'>';
                                            echo '</div>';
                                            echo '<div class="col-md-11" '.((in_array($questionOptions[$i]['id'], $answers))?'style="color : blue"':'').'><label>'.$questionOptions[$i]['option'].'</label></div>';
                                            echo '<div class="clearfix"></div>';
                                        }
                                    } else if($question_type == 'truefalse'){

                                        echo '<div class="col-md-1">';
                                        echo '<input type="radio" name="solution_'.$value['id'].'" value="True" disabled=disabled '.(($value['solution'] == 'True')?'checked="checked"':'').'>';
                                        echo '</div>';
                                        echo '<div class="col-md-6"><label '.(($value['solution'] == 'True')?'style="color : blue"':'').'>True</label></div>';
                                        echo '<div class="clearfix"></div>';
                                        echo '<div class="col-md-1">';
                                        echo '<input type="radio" name="solution_'.$value['id'].'" value="False" disabled=disabled '.(($value['solution'] == 'False')?'checked="checked"':'').'>';
                                        echo '</div>';
                                        echo '<div class="col-md-6"><label '.(($value['solution'] == 'False')?'style="color : blue"':'').'>False</label></div>';
                                        echo '<div class="clearfix"></div>';

                                    } else {
                                        echo '<h4> Answer : '.$value['solution'].'</h4>';
                                    }
                                    ?>
                                <div class="col-md-6">
                                    <input onkeyup="calculateMarks(this,<?php echo $value['question']['marks']; ?>,<?php echo $value['id']; ?>)" type="text" name="marks_obtained" class="marks_obtained" style="width: 10%;" value="<?php echo $value['marks_obtained']; ?>" > / <?php echo $value['question']['marks']; ?>
                                </div>
                            </div>
                            
                        </div>
                        <?php
                        $outOff = $outOff + $value['question']['marks'];
                        $number++;
                            }
                        }
                        ?>
                        <div class="col-md-12">
                            <h3>Total Marks : <input type="text" disabled="disabled" id="total_marks" value="0"> / <?php echo $outOff;?></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>
                            Result
                        </h3>
                        <form class="form" id="resultForm" method="post" action="<?php echo site_url('result/save'); ?>">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" name="id" value="<?php echo $studentExamId; ?>" class="form-control"/>
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label>Result Status</label>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control" name="status">
                                        <option value="">--Select--</option>
                                        <option value="Pass" <?php echo (($status == 'Pass')?'selected="selected"':''); ?>>Pass</option>
                                        <option value="Fail" <?php echo (($status == 'Fail')?'selected="selected"':''); ?>>Fail</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label>Grade</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="grade" class="form-control" value="<?php echo $grade; ?>"/>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <button type="submit" class="btn btn-info">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-info" href="<?php echo site_url('teacher/students/exams?course_id='.$course['id'].'&user_id='.$student['id']); ?>">Back</a>
    </div>
      
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            $("#resultForm").validate({
                rules : {
                    status : {required : true},
                    grade : {required : true},
                }
            });
            
            var maxLength = 240;
            
            $('#textarea').keyup(function() {
              var textlen = maxLength - $(this).val().length;
              $('#rchars').text(textlen);
            });
            
            calculateTotalMarks();
        });
        
        function addComment(id){
            $('#myModal').modal('show');
            $('#studentSolutionId').val(id);
        }
        
        function calculateMarks(input, outof, id){
            var value = $(input).val();
            if(isNaN(value) || value < 0 || value > outof){
                alert('Marks must be in range from 0 to'+outof);
                $(input).val('');
            } else {
                $.ajax({
                    url: '<?php echo site_url("teacher/students/marks?id="); ?>' + id + '&value='+value,
                    type: 'GET',
                    dataType: 'json',
                    error: function () {
                        console.log("error getting list");
                    },
                    success: function (res) {
                        calculateTotalMarks();
                    }
                });
            }
        }
        
        function calculateTotalMarks(){
            var marks = $('.marks_obtained');
            var total = 0;
            for(var i = 0; i < marks.length; i++ ){
                var mark = $(marks[i]).val();
                total = Number(total) + Number(mark);
            }
            $('#total_marks').val(total);
        }
        
    </script>
  </body>
</html>