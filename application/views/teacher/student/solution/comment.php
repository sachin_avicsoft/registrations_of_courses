<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
  </head>
  <body>
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            <?php echo $course['name'].' - '.$student['first_name'].' '.$student['last_name'].' - Solution of '.$file_name; ?>
            <?php if($type == 'comment'){
                echo '<a class="btn btn-info pull-right" href="'.site_url('teacher/students/solutions?course_id='.$course['id'].'&user_id='.$student['id']).'">Back</a>';
            } else if($type == 'summary'){
                echo '<a class="btn btn-info pull-right" href="'.site_url('teacher/courses/students/solutions?id='.$course['id']).'">Back</a>';
            }?>
            
        </h1>
        
        <div class="clearfix"></div>
    	<div class="row">
            
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>
                            Comments
                        </h3>
                        <?php 
                        if($studentSolutionCommentList){
                            echo '<div class="col-md-12">';
                            echo '<ul>';
                            foreach ($studentSolutionCommentList as $key => $value) {
                                echo '<li>'.$value['comment'].'</li>';
                            }
                            echo '</ul>';
                            echo '</div>';
                        } else {
                            echo '<p>No comments given yet!</p>';
                        }
                        ?>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <?php if($type == 'comment'){
                echo '<a class="btn btn-info" href="'.site_url('teacher/students/solutions?course_id='.$course['id'].'&user_id='.$student['id']).'">Back</a>';
            } else if($type == 'summary'){
                echo '<a class="btn btn-info" href="'.site_url('teacher/courses/students/solutions?id='.$course['id']).'">Back</a>';
            }?>
    </div>
      
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            $("#solutionFileCommentForm").validate({
                rules : {
                    student_solution_id : {required : true, number : true},
                    comment : {required : true, minlength : 10},
                }
            });
            
            var maxLength = 240;
            
            $('#textarea').keyup(function() {
              var textlen = maxLength - $(this).val().length;
              $('#rchars').text(textlen);
            });
        });
        
        function addComment(id){
            $('#myModal').modal('show');
            $('#studentSolutionId').val(id);
        }
        
    </script>
  </body>
</html>