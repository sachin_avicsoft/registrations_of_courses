<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
  </head>
  <body>
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <div class="container" style="margin-top: 5%;">
        <h1 style="text-align: center">Welcome to Online Communication and Examination Platform, TU Chemnitz</h1>
        <?php $this->load->view('include/home-nav', 'refresh'); ?> 
        
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
            <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default" style="border: 2px solid;">
                    <div class="panel-body">
                        <p class="pull-right"><i class="far fa-address-book" style="font-size: 2.5em;"></i></p><div class="clearfix"></div>
                        <p><a style="font-size: 2em;" href="<?php echo site_url('teacher/properties'); ?>">Master Data</a></p>
                        <!--<p class="pull-right"><a href="#">Create Master Data</a></p>-->
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default" style="border: 2px solid;">
                    <div class="panel-body">
                        <p class="pull-right"><i class="fas fa-user-plus" style="font-size: 2.5em"></i></p><div class="clearfix"></div>
                        <p><a style="font-size: 2em;" href="<?php echo site_url('teacher/students'); ?>">Student Data</a></p>
                        <!--<p class="pull-right"><a href="#">Maintain Student Data</a></p>-->
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default" style="border: 2px solid;">
                    <div class="panel-body">
                        <p class="pull-right"><i class="fas fa-book" style="font-size: 2.5em"></i></p><div class="clearfix"></div>
                        <p><a style="font-size: 2em;" href="<?php echo site_url('teacher/courses'); ?>">Course Data</a></p>
                        <!--<p class="pull-right"><a href="">Maintain Course Data</a></p>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            $("#loginForm").validate({
                rules : {
                    email : {required : true, email : true},
                    password : {required :true},
                }
            });
        });

    </script>
  </body>
</html>