<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
  </head>
  <body>
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            <?php echo $course['name'].' - '; ?>Lecture Contents
            <a class="btn btn-info pull-right" href="<?php echo site_url('teacher/courses'); ?>">Back</a>
        </h1>
        
        <div class="clearfix"></div>
    	<div class="row">
            
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>
                            Lecture Contents
                            <a class="btn btn-info pull-right" href="<?php echo site_url('teacher/course/lectures/add?courseId='.$courseId); ?>">Add Lecture</a>
                        </h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-responsive table-striped" style="background-color: #eee">
                                <thead>
                                    <tr>
                                        <th style="width: 20%">Name</th>
                                        <th style="width: 10%">Date</th>
                                        <th style="width: 20%">Content</th>
                                        <th style="width: 30%">Homework and Solution</th>
                                        <th style="width: 20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if($lectureList){
                                            foreach ($lectureList as $key => $value) {
                                    ?>
                                        <tr>
                                            <td><?php echo $value['name']?></td>
                                            <td><?php echo $value['date']?></td>
                                            <td>
                                                <?php 
                                                if($value['contentList']){
                                                    foreach ($value['contentList'] as $key => $file) {
                                                        if($file['file_type'] == 0){
//                                                            echo '<a href="'.base_url().'uploads/files/courses/'.$file['id'].'.'.$file['ext'].'">'.$file['file_name'].'</a><br>';
                                                            echo '<a target="_blank" href="'.site_url('get/files?id='.$file['id'].'&type=course').'">'.$file['file_name'].'</a><br>';
                                                        }
                                                    }
                                                } else {
                                                    echo '<p>No content available</p>'; 
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php 
                                                if($value['homeworkList']){
                                                    foreach ($value['homeworkList'] as $key => $file) {
                                                        echo '<div class="col-md-12" style="border-bottom: 2px solid #ddd;">';
                                                        echo '<div class="col-md-6">';
//                                                        echo '<a href="'.base_url().'uploads/files/courses/'.$file['id'].'.'.$file['ext'].'">'.$file['file_name'].'</a><br>';
                                                        echo '<a target="_blank" href="'.site_url('get/files?id='.$file['id'].'&type=course').'">'.$file['file_name'].'</a><br>';
                                                        echo '</div>';
                                                        echo '<div class="col-md-6" style="border-left: 2px solid #ddd;">';
                                                        if($file['teacherSolutionList']){
                                                            echo 'Teacher Solutions<br>';
                                                            echo '<div class="col-md-12"  style="border-bottom: 2px solid #ddd;"></div>';
                                                            foreach ($file['teacherSolutionList'] as $key => $solution) {
//                                                                    echo '<a href="'.base_url().'uploads/files/courses/'.$solution['id'].'.'.$solution['ext'].'">'.$solution['file_name'].'</a><br>';
                                                                echo '<a target="_blank" href="'.site_url('get/files?id='.$solution['id'].'&type=course').'">'.$solution['file_name'].'</a><br>';
                                                                echo '<div class="col-md-12"  style="border-bottom: 1px solid #ddd;"></div>';
                                                            }
                                                        }  else {
                                                            echo 'No solution available';
                                                        }
                                                        echo '</div>';
//                                                        echo '<hr>';
                                                        echo '</div>';
                                                    }
                                                } else {
                                                    echo '<p>No content available</p>'; 
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a class="btn btn-info btn-sm" href="<?php echo site_url('teacher/course/lectures/edit?id='.$value['id'].'&courseId='.$courseId); ?>">Edit</a>
                                                <a class="btn btn-info btn-sm" onclick="deleteInfo(<?php echo $value['id'];?>)">Delete</a>
                                            </td>
                                        </tr>
                                    <?php
                                            }
                                        } else {
                                    ?>
                                        <tr><td colspan="5">No record found!</td></tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-info" href="<?php echo site_url('teacher/courses'); ?>">Back</a>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            $("#loginForm").validate({
                rules : {
                    email : {required : true, email : true},
                    password : {required :true},
                }
            });
        });
        
        function deleteInfo(id){
            BootstrapDialog.show({      
                    message: 'Are you sure you want to delete this Lecture?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'Delete',
                        cssClass:'btn-danger',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('teacher/course/lectures/delete?id='); ?>"+id+'&courseId='+<?php echo $courseId; ?>;
                            window.location = url;
                        }
                    }]
                });
        }
    </script>
  </body>
</html>