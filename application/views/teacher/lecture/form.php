<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
  </head>
  <body>
    
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            <?php echo $course['name'].' - '; ?>Lecture Contents
            <a class="btn btn-info pull-right" href="<?php echo site_url('teacher/course/lectures?courseId='.$courseId); ?>">Back</a>
        </h1>
        <div class="clearfix"></div>
    	<div class="row">
            
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>
                            <?php 
                            if($id == ''){
                                echo 'Add Lecture';
                            } else {
                                echo $name.' - Edit Lecture';
                            }
                            ?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form name="lectureForm" enctype="multipart/form-data" id="lectureForm" class="form-horizontal" method="post" action="<?php echo site_url('teacher/course/lectures/save?courseId='.$courseId); ?>" >
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control">
                            <input type="hidden" name="course_id" value="<?php echo $courseId; ?>" class="form-control">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label">Name <span class="mandetory_field">*</span></label>
                                    <input type="text" name="name" value="<?php echo $name; ?>" class="form-control" />
                                     <!--onkeyup="alphanumeric(document.lectureForm.name)"-->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label">Date <span class="mandetory_field">*</span></label>
                                    <input type="text" id="datepicker" onkeypress="return false;" name="date" value="<?php echo $date; ?>" class="form-control" />
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-info" ><i class="fa fa-check"></i> Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php
                if($id != ''){
            ?>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>
                            Study Content
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form name="lectureFileForm" enctype="multipart/form-data" id="lectureFileForm" class="form-horizontal" method="post" action="<?php echo site_url('teacher/course/lectures/save/file'); ?>" >
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control"/>
                            <input type="hidden" name="course_id" value="<?php echo $courseId; ?>" class="form-control">
                            <div class="form-group"> 
                                <div class="col-md-6">
                                    <label class="control-label">File Name <span class="mandetory_field">*</span></label>
                                    <input type="text" id="filename" name="file_name" value="" class="form-control" onkeyup="alphanumeric(document.lectureFileForm.file_name)"/>
                                    <label>Homework? </label> <input style="margin-top: 10px;" type="checkbox"  name="file_type" value="1">
                                    <input style="margin-top: 10px;" type="file" name="userfile[]" required="required" id="file" onchange="return ValidateFileUpload(this)" accept=".pdf, .doc, .docx" size="20"/>
                                    <small>(max upload file size is 2MB and allowed file types are PDF,DOC,DOCX)</small>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="col-md-8">
                                    Contents
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <th style="width: 70%">Content</th>
                                                <th>Action</th>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                if($contentList){
                                                    foreach ($contentList as $key => $value) {
                                                        if($value['file_type'] == 0){
                                                ?>
                                                <tr>
                                                    <td><?php echo '<a target="_blank" href="'.site_url('get/files?id='.$value['id'].'&type=course').'">'.$value['file_name'].'</a>' ; ?></td>
                                                    <!--<td><a href="<?php echo base_url().'uploads/files/courses/'.$value['id'].'.'.$value['ext']; ?>"><?php echo $value['file_name'];?></a></td>-->
                                                    <td><a class="btn btn-sm btn-info" onclick="delete_file(<?php echo $value['id']; ?>)">Delete</a></td>
                                                </tr>
                                                <?php } } } else {
                                                    echo '<tr><td colspan="2">No records found!</td></tr>';
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-8">
                                    Homeworks
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <th style="width: 35%">Homework</th>
                                                <th style="width: 35%">Solution</th>
                                                <th>Action</th>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                if($homeworkList){
                                                    foreach ($homeworkList as $key => $value) {
                                                ?>
                                                <tr>
                                                    <td>
                                                        <!--<a href="<?php echo base_url().'uploads/files/courses/'.$value['id'].'.'.$value['ext']; ?>"><?php echo $value['file_name'];?></a>-->
                                                        <?php echo '<a target="_blank" href="'.site_url('get/files?id='.$value['id'].'&type=course').'">'.$value['file_name'].'</a>'; ?>
                                                    </td>
                                                    <td>
                                                        <?php 
                                                        if($value['solutionList']){
                                                            foreach ($value['solutionList'] as $key => $file){
                                                                echo '<div class="col-md-12">';
                                                                echo '<div class="col-md-9">';
//                                                                echo '<a href="'.base_url().'uploads/files/courses/'.$file['id'].'.'.$file['ext'].'">'.$file['file_name'].'</a><br>';
                                                                echo '<a target="_blank" href="'.site_url('get/files?id='.$file['id'].'&type=course').'">'.$file['file_name'].'</a><br>';
                                                                echo '</div>';
                                                                echo '<div class="col-md-3">';
                                                                echo '<button style="margin-bottom : 1px;" type="button" class="btn btn-info btn-xs" title="Delete" onclick="delete_file('.$file['id'].')"><i class="fas fa-trash-alt"></i></button>';
                                                                echo '</div>';
                                                                echo '</div>';
                                                                
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <a class="btn btn-sm btn-info" onclick="openModal(<?php echo $value['id']; ?>)">Solution Upload</a>
                                                        <a class="btn btn-sm btn-info" onclick="delete_file(<?php echo $value['id']; ?>)">Delete</a>
                                                    </td>
                                                </tr>
                                                <?php } } else {
                                                    echo '<tr><td colspan="3">No records found!</td></tr>';
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
        <a class="btn btn-info" href="<?php echo site_url('teacher/course/lectures?courseId='.$courseId); ?>">Back</a>
    </div>
      <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <form name="solutionFileForm" enctype="multipart/form-data" id="solutionFileForm" class="form-horizontal" method="post" action="<?php echo site_url('teacher/course/lectures/save/solution-file'); ?>" >
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                <input type="hidden" name="lecture_id" id="lecture_id" value="<?php echo $id; ?>" class="form-control"/>
                <input type="hidden" name="course_id" id="course_id" value="<?php echo $courseId; ?>" class="form-control"/>
                <input type="hidden" name="file_id" id="file_id" value="" class="form-control"/>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Upload Solution File</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group"> 
                            <div class="col-md-6">
                                <label class="control-label">File Name <span class="mandetory_field">*</span></label>
                                <input type="text" name="file_name" class="form-control" onkeyup="alphanumeric(document.solutionFileForm.file_name)"/>
                                <input style="margin-top: 10px;" type="file" name="userfile[]" required="required" id="solution_file" onchange="return ValidateSolutionFileUpload(this)" accept=".pdf, .doc, .docx" size="20" >
                                <small>(max upload file size is 2MB and allowed file types are PDF,DOC,DOCX)</small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info" ><i class="fas fa-upload"></i> Upload</button>
                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            $("#lectureForm").validate({
                rules : {
                    name : {required : true},
                    date : {required :true},
                }
            });
            
            $("#lectureFileForm").validate({
                rules : {
                    file_name : {required : true},
                    userfile : {required : true},
                }
            });
            
            $('#solutionFileForm').validate({
                rules : {
                    file_name : {required : true},
                    userfile : {required : true},
                }
            });
            
            $("#datepicker").daterangepicker({        
                showDropdowns: true,
                singleDatePicker: true,
                locale: {
                    format: 'DD/MM/YYYY'
                }        
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
            
        });
        
        function delete_file(id){
            BootstrapDialog.show({      
                    message: 'Are you sure you want to delete this File?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'Delete',
                        cssClass:'btn-danger',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('teacher/course/lectures/delete/file?id='); ?>"+id+'&course_id='+<?php echo $courseId; ?>+'&lecture_id='+<?php echo (($id=='')?0:$id); ?>;
                            window.location = url;
                        }
                    }]
                });
        }
        
        function ValidateFileUpload(file){
            
                var fuData = document.getElementById('file');
                var FileUploadPath = fuData.value;
                var Extension = FileUploadPath.substring(
                        FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

                //The file uploaded is an image
                if (Extension == "pdf" || Extension == "doc" || Extension == "docx") {
                    var id = '<?php echo $id; ?>';
                    var fileName = $(file).val();
                    var fileSize = file.files[0].size;
                    if(fileName){
                        if( fileSize > 2097152){//5242880
                              $(file).val("");
                             alert('Please upload less than 2mb file');
                             return false;
                          } 
                    }
                    return;
                } else {
                    $('#file').val('');
                    alert("Only allows file types of DOC, DOCX, PDF");
                }
        }
        
        function ValidateSolutionFileUpload(file){
            var fuData = document.getElementById('solution_file');
                var FileUploadPath = fuData.value;
                var Extension = FileUploadPath.substring(
                        FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

                //The file uploaded is an image
                if (Extension == "pdf" || Extension == "doc" || Extension == "docx") {
                    var id = '<?php echo $id; ?>';
                    var fileName = $(file).val();
                    var fileSize = file.files[0].size;
                    if(fileName){
                        if( fileSize > 2097152){//5242880
                              $(file).val("");
                             alert('Please upload less than 2mb file');
                             return false;
                          } 
                    }
                    return;
                } else {
                    $('#solution_file').val('');
                    alert("Only allows file types of DOC, DOCX, PDF");
                }
        }
        
        function openModal(id){
            $('#file_id').val(id);
            $('#myModal').modal('show');
        }

    </script>
  </body>
</html>