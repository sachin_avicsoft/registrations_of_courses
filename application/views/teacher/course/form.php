<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
  </head>
  <body>
    
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            Courses
            <a class="btn btn-info pull-right" href="<?php echo site_url('teacher/courses'); ?>">Back</a>
        </h1>
        <div class="clearfix"></div>
    	<div class="row">
            
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>
                        <?php 
                            if($id == ''){
                                echo 'Add Course';
                            } else {
                                
                                echo $name.' - Edit Course';
                            }
                        ?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        
                        <form id="courseForm" class="form-horizontal" method="post" action="<?php echo site_url('teacher/courses/save'); ?>">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control"/>
                            <input type="hidden" value="<?php echo $status ?>" name="status" class="form-control" />
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label">Name <span class="mandetory_field">*</span></label>
                                    <input type="text" name="name" value="<?php echo $name; ?>" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label">Start Date <span class="mandetory_field">*</span></label>
                                    <input type="text" id="datepicker" onkeypress="return false;" name="start_date" value="<?php echo $start_date; ?>" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label">End Date <span class="mandetory_field">*</span></label>
                                    <input type="text" id="datepicker1"  onkeypress="return false;" name="end_date" value="<?php echo $end_date; ?>" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label">Description (Maximum 240 characters) <span class="mandetory_field">*</span></label>
                                    <textarea class="form-control" name="description" rows="5" id="textarea" maxlength="240"><?php echo $description; ?></textarea>
                                    <span id="rchars"><?php echo $length; ?></span> Character(s) Remaining
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <button type="button" onclick="checkDate()" class="btn btn-info" ><i class="fa fa-check"></i> Save</button>
                                    <!--<button type="submit" class="btn btn-info" ><i class="fa fa-check"></i> Save</button>-->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <?php 
                    if($id != ''){
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Course Forum</h3>
                    </div>
                    <div class="panel-body">
                            <div class="col-md-12">
                                <form id="commentForm" class="form" action="<?php echo site_url('teacher/course/comments/save'); ?>" method="post">
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control"/>
                                    <div class="col-md-12">
                                        <textarea name="comment" class="form-control" rows="3"></textarea>
                                        <button style="margin-top: 5px;" type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                            
                            <hr>
                            <div class="col-md-12">
                                <?php 
                                    if($forumList){
                                        foreach ($forumList as $key => $value) {
                                ?>
                                <div style="padding: 10px;border: 1px solid #eee;margin-bottom: 20px;">
                                    <p><?php echo $value['createdAt'].' - '.$value['first_name'].' '.$value['last_name']; ?></p>
                                    <p><?php echo $value['comment']; ?></p>
                                </div>
                                <div class="clearfix"></div>
                                <?php
                                        }
                                    }
                                ?>
                            </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            </div>
        <a class="btn btn-info" href="<?php echo site_url('teacher/courses'); ?>">Back</a>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            
            $('#courseForm').validate({
                rules : {
                    name : {required : true},
                    start_date : {required :true},
                    end_date : {required :true},
                    description : {required : true, minlength : 10},
                }
            });
            
            $('#commentForm').validate({
                rules : {
                    comment : {required : true, minlength : 10},
                }
            });
            
            $("#courseFileForm").validate({
                rules : {
                    file_name : {required : true},
                    userfile : {required : true},
                }
            });
            
            var maxLength = 240;
            
            $('#textarea').keyup(function() {
              var textlen = maxLength - $(this).val().length;
              $('#rchars').text(textlen);
            });
            
            $("#datepicker").daterangepicker({        
                showDropdowns: true,
                singleDatePicker: true,
                locale: {
                    format: 'DD/MM/YYYY'
                }        
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
            
            $("#datepicker1").daterangepicker({        
                showDropdowns: true,
                singleDatePicker: true,
                locale: {
                    format: 'DD/MM/YYYY'
                }        
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
            
        });
        
        function checkDate(){
            var start_date = $('#datepicker').val();
            var end_date = $('#datepicker1').val();
//            var s = new Date(start_date);
            function process(date){
                var parts = date.split("/");
                return new Date(parts[2], parts[1] - 1, parts[0]);
             }
             
             if(process(start_date) > process(end_date)){
                alert('End date must be is greater than or equal to Start date');
             } else {
                 $( "#courseForm" ).submit();
             }
        }
    </script>
  </body>
</html>