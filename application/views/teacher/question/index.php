<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
  </head>
  <body>
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            <?php echo $course['name'].' - '.$exam['name'].' - '; ?>Questions
            <a class="btn btn-info pull-right" href="<?php echo site_url('teacher/course/exams?courseId='.$courseId.'&examId='.$examId); ?>">Back</a>
        </h1>
        
        <div class="clearfix"></div>
    	<div class="row">
            
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>
                            Question List
                            <?php if($buttonShow){ ?>
                            <a class="btn btn-info pull-right" href="<?php echo site_url('teacher/course/exam/questions/add?courseId='.$courseId.'&examId='.$examId); ?>">Add Question</a>
                            <?php } ?>
                        </h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-responsive table-striped" style="background-color: #eee">
                                <thead>
                                    <tr>
                                        <th style="width: 40%">Question</th>
                                        <th style="width: 10%">Question Type</th>
                                        <th style="width: 10%">Marks</th>
                                        <th style="width: 10%">Status</th>
                                        <th style="width: 30%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if($questionList){
                                            foreach ($questionList as $key => $value) {
                                    ?>
                                        <tr>
                                            <td><?php echo $value['question']?></td>
                                            <td>
                                                <?php 
                                                    if($value['question_type'] == 'multichoice'){
                                                        echo 'Multiple Choice';
                                                    } else if($value['question_type'] == 'truefalse'){
                                                        echo 'True/False';
                                                    } else if($value['question_type'] == 'shortdescription'){
                                                        echo 'Short Description';
                                                    } else if($value['question_type'] == 'fillintheblank'){
                                                        echo 'Fill in the blank';
                                                    }
                                                ?>
                                            </td>
                                            <td><?php echo $value['marks']?></td>
                                            <td><?php echo (($value['status'] == 'enable')?"Enable":"Disable"); ?></td>
                                            <td>
                                                <a class="btn btn-info btn-sm" href="<?php echo site_url('teacher/course/exam/questions/edit?id='.$value['id'].'&courseId='.$courseId.'&examId='.$examId); ?>">Edit</a>
                                                <a class="btn btn-info btn-sm" onclick="deleteInfo(<?php echo $value['id'];?>)">Delete</a>
                                                <a class="btn <?php echo (($value['status'] == 'enable')?"btn-warning":"btn-warning"); ?> btn-sm" onclick="changeStatus(<?php echo (($value['status'] == 'enable')?1:0); ?>,<?php echo $value['id']; ?>)" ><?php echo (($value['status'] == 'enable')?'Disable':'Enable'); ?></a>
                                            </td>
                                        </tr>
                                    <?php
                                            }
                                        } else {
                                    ?>
                                        <tr><td colspan="5">No record found!</td></tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-info" href="<?php echo site_url('teacher/course/exams?courseId='.$courseId.'&examId='.$examId); ?>">Back</a>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
//            $("#loginForm").validate({
//                rules : {
//                    email : {required : true, email : true},
//                    password : {required :true},
//                }
//            });
        });
        
        function deleteInfo(id){
            BootstrapDialog.show({      
                    message: 'Are you sure you want to delete this Question?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'Delete',
                        cssClass:'btn-danger',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('teacher/course/exam/questions/delete?id='); ?>"+id+'&courseId='+<?php echo $courseId; ?>+'&examId='+<?php echo $examId; ?>;
                            window.location = url;
                        }
                    }]
                });
        }
        
        function changeStatus(status,id){
            if(status == 1){
                var data = 'Disable';
            } else {
                var data = 'Enable';
            }
            BootstrapDialog.show({      
                    message: 'Are you sure you want to change the status to '+data+'?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'Change',
                        cssClass:'btn-warning',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('teacher/course/exam/questions/change-status?status='); ?>"+status+'&id='+id+'&courseId='+<?php echo $courseId; ?>+'&examId='+<?php echo $examId; ?>;
                            window.location = url;
                        }
                    }]
                });
        }
    </script>
  </body>
</html>