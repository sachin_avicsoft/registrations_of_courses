<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
  </head>
  <body>
    
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            <?php echo $course['name'].' - '.$exam['name'].' - '; ?>Questions
            <a class="btn btn-info pull-right" href="<?php echo site_url('teacher/course/exam/questions?courseId='.$courseId.'&examId='.$examId); ?>">Back</a>
        </h1>
        <div class="clearfix"></div>
    	<div class="row">
            
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>
                            <?php 
                            if($id == ''){
                                echo 'Add Question';
                            } else {
                                echo 'Edit Question';
                            }
                            ?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form id="questionForm" class="form-horizontal" method="post" action="<?php echo site_url('teacher/course/exam/questions/save?courseId='.$courseId.'&examId='.$examId); ?>" >
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control">
                            <input type="hidden" name="course_id" value="<?php echo $courseId; ?>" class="form-control">
                            <input type="hidden" name="exam_id" value="<?php echo $examId; ?>" class="form-control">
                            <input type="hidden" value="<?php echo $status ?>" name="status" class="form-control" />
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">Question (Maximum 240 characters) <span class="mandetory_field">*</span></label>
                                    <input type="text" name="question" value="<?php echo $question; ?>" class="form-control" maxlength="240" id="question" >
                                    <span id="rchars"><?php echo $length; ?></span> Character(s) Remaining
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label">Marks <span class="mandetory_field">*</span></label>
                                    <input type="text" name="marks" value="<?php echo $marks; ?>" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label">Question Type <span class="mandetory_field">*</span></label>
                                    <select class="form-control" id="question_type" name="question_type" onchange="getQuestionOption()">
                                        <option value="">--Select--</option>
                                        <option value="truefalse" <?php echo (($question_type == 'truefalse')?"selected=selected":""); ?>>True/False</option>
                                        <option value="multichoice" <?php echo (($question_type == 'multichoice')?"selected=selected":""); ?>>Multiple Choice</option>
                                        <option value="fillintheblank" <?php echo (($question_type == 'fillintheblank')?"selected=selected":""); ?>>Fill in the blank</option>
                                        <option value="shortdescription" <?php echo (($question_type == 'shortdescription')?"selected=selected":""); ?>>Short Description</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="question_option" <?php echo (($question_type == 'multichoice' || $question_type == 'fillintheblank')?'':'style="display:none"'); ?>>
                                <div class="col-md-6">
                                    <label class="control-label">option <span class="mandetory_field">*</span></label><button type="button" class="btn btn-xs btn-primary pull-right" onclick="addNewRow()">Add Option</button>
                                    <div id="option_inputs">
                                        <?php
                                        if($optionList){
                                            foreach ($optionList as $key => $value) {
                                                echo '<input style="margin-bottom : 5px;" name="option[]" class="form-control" value="'.$value['option'].'" />';
                                            }
                                        } else {
                                            echo '<input style="margin-bottom : 5px;" name="option[]" class="form-control" value="">';
                                        }
                                        ?>
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-info" ><i class="fa fa-check"></i> Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-info" href="<?php echo site_url('teacher/course/exam/questions?courseId='.$courseId.'&examId='.$examId); ?>">Back</a>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            $("#questionForm").validate({
                rules : {
                    question : {required : true},
                    question_type : {required :true},
                    marks : {required : true,number : true, min : 1 }
                }
            });
            
//            $("#courseFileForm").validate({
//                rules : {
//                    file_name : {required : true},
//                    userfile : {required : true},
//                }
//            });
            
            $("#datepicker").daterangepicker({        
                showDropdowns: true,
                singleDatePicker: true,
                locale: {
                    format: 'DD/MM/YYYY'
                }        
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
            
            $("#datepicker1").daterangepicker({        
                showDropdowns: true,
                singleDatePicker: true,
                locale: {
                    format: 'DD/MM/YYYY'
                }        
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
            
            var maxLength = 240;
            
            $('#question').keyup(function() {
              var textlen = maxLength - $(this).val().length;
              $('#rchars').text(textlen);
            });
        });
        
        function getQuestionOption(){
            var question_type = $('#question_type').val();
            if(question_type == 'multichoice' || question_type == 'fillintheblank'){
                $("#question_option").show();
                //add options for question show
            } else {
                $("#question_option").hide();
            }
        }
        
        function addNewRow(){
            $('#option_inputs').append("<input style='margin-bottom : 5px;' name='option[]' class='form-control'>");
        }
    </script>
  </body>
</html>