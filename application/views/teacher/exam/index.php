<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration of Courses</title>   
    
    <?php $this->load->view('include/css', 'refresh'); ?> 
    <style type="text/css">
        .nav-side-menu {
  overflow: auto;
  font-family: verdana;
  font-size: 12px;
  font-weight: 200;
  background-color: #EC971F;
  width: 300px;
  color: #e1ffff;
}
.nav-side-menu .brand {
  background-color: #EC971F;
  line-height: 50px;
  display: block;
  text-align: center;
  font-size: 14px;
}
.nav-side-menu .toggle-btn {
  display: none;
}
.nav-side-menu ul,
.nav-side-menu li {
  list-style: none;
  padding: 0px;
  margin: 0px;
  line-height: 33px;
  cursor: pointer;
      
    .collapsed{
       .arrow:before{
/*                 font-family: FontAwesome;
                 content: "\f053";*/
                 /*display: inline-block;*/
                 padding-left:10px;
                 padding-right: 10px;
                 vertical-align: middle;
                 float:right;
            }
     }

}
.nav-side-menu ul :not(collapsed) .arrow:before,
.nav-side-menu li :not(collapsed) .arrow:before {
/*  font-family: FontAwesome;
  content: "\f078";*/
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
  float: right;
}
.nav-side-menu ul .active,
.nav-side-menu li .active {
  border-left: 3px solid #d19b3d;
  background-color: #EC971F;
}
.nav-side-menu ul .sub-menu li.active,
.nav-side-menu li .sub-menu li.active {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li.active a,
.nav-side-menu li .sub-menu li.active a {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li,
.nav-side-menu li .sub-menu li {
  background-color: #fff;
  border: none;
  line-height: 33px;
  border-bottom: 1px solid #eee;
  margin-left: 0px;
}
.nav-side-menu ul .sub-menu li:hover,
.nav-side-menu li .sub-menu li:hover {
  background-color: #e6e6e6;
}
.nav-side-menu ul .sub-menu li:before,
.nav-side-menu li .sub-menu li:before {
/*  font-family: FontAwesome;
  content: "\f105";*/
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
}
.nav-side-menu li {
  padding-left: 0px;
/*  border-left: 3px solid #2e353d;
  border-bottom: 1px solid #23282e;*/
}
.nav-side-menu li a {
  text-decoration: none;
  color: #e1ffff;
}
.nav-side-menu li a i {
  padding-left: 10px;
  width: 20px;
  padding-right: 20px;
}
.nav-side-menu li:hover {
  /*border-left: 3px solid #4f5b69;*/
  background-color: #EC971F;
  -webkit-transition: all 1s ease;
  -moz-transition: all 1s ease;
  -o-transition: all 1s ease;
  -ms-transition: all 1s ease;
  transition: all 1s ease;
}
@media (max-width: 767px) {
  .nav-side-menu {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
  }
  .nav-side-menu .toggle-btn {
    display: block;
    cursor: pointer;
    position: absolute;
    right: 10px;
    top: 10px;
    z-index: 10 !important;
    padding: 3px;
    background-color: #ffffff;
    color: #000;
    width: 40px;
    text-align: center;
  }
  .brand {
    text-align: left !important;
    font-size: 22px;
    padding-left: 20px;
    line-height: 50px !important;
  }
}
@media (min-width: 767px) {
  .nav-side-menu .menu-list .menu-content {
    display: block;
  }
}

@media (min-width: 481px) and (max-width: 766px){
    .nav-side-menu {
              position: relative;
              width: 100%;
              margin-bottom: 10px;
            }
            .nav-side-menu .toggle-btn {
              display: block;
              cursor: pointer;
              position: absolute;
              right: 10px;
              top: 10px;
              z-index: 10 !important;
              padding: 3px;
              background-color: #ffffff;
              color: #000;
              width: 40px;
              text-align: center;
            }
            .brand {
              text-align: left !important;
              font-size: 22px;
              padding-left: 20px;
              line-height: 50px !important;
            }
            
            .nav-side-menu .menu-list .menu-content {
              display: block;
            }
}

@media only screen and (max-width : 480px) {
              .nav-side-menu {
              position: relative;
              width: 100%;
              margin-bottom: 10px;
            }
            .nav-side-menu .toggle-btn {
              display: block;
              cursor: pointer;
              position: absolute;
              right: 10px;
              top: 10px;
              z-index: 10 !important;
              padding: 3px;
              background-color: #ffffff;
              color: #000;
              width: 40px;
              text-align: center;
            }
            .brand {
              text-align: left !important;
              font-size: 22px;
              padding-left: 20px;
              line-height: 50px !important;
            }
            
            .nav-side-menu .menu-list .menu-content {
              display: block;
            }
          }

          /* Custom, iPhone Retina */ 
          @media only screen and (max-width : 320px) {
              .nav-side-menu {
              position: relative;
              width: 100%;
              margin-bottom: 10px;
            }
            .nav-side-menu .toggle-btn {
              display: block;
              cursor: pointer;
              position: absolute;
              right: 10px;
              top: 10px;
              z-index: 10 !important;
              padding: 3px;
              background-color: #ffffff;
              color: #000;
              width: 40px;
              text-align: center;
            }
            .brand {
              text-align: left !important;
              font-size: 22px;
              padding-left: 20px;
              line-height: 50px !important;
            }
            
            .nav-side-menu .menu-list .menu-content {
              display: block;
            }
          }
          
body {
  margin: 0px;
  padding: 0px;
}

    </style>
  </head>
  <body>
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            <?php echo $course['name'].' - '; ?>Exams
            <a class="btn btn-info pull-right" href="<?php echo site_url('teacher/courses'); ?>">Back</a>
        </h1>
        
        <div class="clearfix"></div>
    	<div class="row">
            
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>
                            Exam List
                            <?php if($buttonShow){ ?>
                            <a class="btn btn-info pull-right" href="<?php echo site_url('teacher/course/exams/add?courseId='.$courseId); ?>">Add Exam</a>
                            <?php } ?>
                        </h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-responsive table-striped" style="background-color: #eee">
                                <thead>
                                    <tr>
                                        <th style="width: 25%">Name</th>
                                        <th style="width: 15%">Start Date</th>
                                        <th style="width: 15%">End Date</th>
                                        <th style="width: 15%">Time Limit</th>
                                        <th style="width: 10%">Status</th>
                                        <th style="width: 20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if($examList){
                                            foreach ($examList as $key => $value) {
                                    ?>
                                        <tr>
                                            <td><?php echo $value['name']?></td>
                                            <td><?php echo $value['start_date']?></td>
                                            <td><?php echo $value['end_date']?></td>
                                            <td><?php echo $value['time_limit']?></td>
                                            <td><?php echo (($value['status'] == 'enable')?"Enable":"Disable"); ?></td>
                                            <td>
                                                <div class="nav-side-menu">
                                                    <div class="menu-list">
                                                        <ul id="menu-content" class="menu-content collapse out" >
                                                            <li data-toggle="collapse" data-target="#<?php echo $value['id']; ?>" class="collapsed">
                                                                <a style="cursor : pointer;width: 100%;border-radius: 0px;" class="btn btn-warning"> Action <span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                                                            </li>  
                                                            <ul class="sub-menu collapse" id="<?php echo $value['id']; ?>">
                                                                <li style="text-align : center;"><a style="display: block;color: #000;padding: 0px 20px;" href="<?php echo site_url('teacher/course/exams/edit?id='.$value['id'].'&courseId='.$courseId); ?>">Edit</a></li>
                                                                <li style="text-align : center;"><a style="display: block;color: #000;padding: 0px 20px;" onclick="deleteInfo(<?php echo $value['id'];?>)">Delete</a></li>
                                                                <li style="text-align : center;"><a style="display: block;color: #000;padding: 0px 20px;" onclick="changeStatus(<?php echo (($value['status'] == 'enable')?1:0); ?>,<?php echo $value['id']; ?>)" ><?php echo (($value['status'] == 'enable')?'Disable':'Enable'); ?></a></li>
                                                                <?php if($value['status'] == 'enable'){ ?>
                                                                <li style="text-align : center;"><a style="display: block;color: #000;padding: 0px 20px;" href="<?php echo site_url('teacher/course/exam/questions?courseId='.$courseId.'&examId='.$value['id']); ?>">Questions</a></li>
                                                                <?php } ?>
                                                            </ul>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php
                                            }
                                        } else {
                                    ?>
                                        <tr><td colspan="6">No record found!</td></tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-info" href="<?php echo site_url('teacher/courses'); ?>">Back</a>
    </div>
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
    <script type="text/javascript">

        $(function(){
            $("#loginForm").validate({
                rules : {
                    email : {required : true, email : true},
                    password : {required :true},
                }
            });
            
//            var width1 = $("#action_button").width();
//            console.log(width1);
//            var width2 = width1 - Number(7);
//            console.log(width2);
//            $("#dropdown_button").width(width2);
//            $('.dropdown-item').width(width2);
//            $("#dropdown_button").width()
        });
        
        function changeStatus(status,id){
            if(status == 1){
                var data = 'Disable';
            } else {
                var data = 'Enable';
            }
            BootstrapDialog.show({      
                    message: 'Are you sure you want to change the status to '+data+'?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'Change',
                        cssClass:'btn-warning',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('teacher/course/exams/change-status?status='); ?>"+status+'&id='+id+'&courseId='+<?php echo $courseId; ?>;
                            window.location = url;
                        }
                    }]
                });
        }
        
        function deleteInfo(id){
            BootstrapDialog.show({      
                    message: 'Are you sure you want to delete this Exam?',
                    title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                            dialogItself.close();
                        },

                    },{
                        label: 'Delete',
                        cssClass:'btn-danger',
                        action: function(dialogItself){
                            var url= "<?php echo site_url('teacher/course/exams/delete?id='); ?>"+id+'&courseId='+<?php echo $courseId; ?>;
                            window.location = url;
                        }
                    }]
                });
        }
    </script>
  </body>
</html>