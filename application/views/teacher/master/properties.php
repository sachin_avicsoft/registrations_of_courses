<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- This should be from i18n file -->    
    <title>Registration of Courses</title>   
    
    <!-- Navigation -->
    <?php $this->load->view('include/css', 'refresh'); ?> 
    <style type="text/css">
        .form-group {
            margin-bottom: 10px;
        }
    </style>
  </head>
  <body>
    
    <!-- Navigation -->
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <!------ Include the above in your HEAD tag ---------->
    <div class="container" style="margin-top: 5%;">
        <?php $this->load->view('include/nav', 'refresh'); ?> 
        <h1 class="main-heading">
            Configure Properties
            <a class="btn btn-info pull-right" href="<?php echo site_url('teacher/dashboard'); ?>">Back</a>
        </h1>
        <div class="clearfix"></div>
    	<div class="row">
            <div class="col-md-12">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Configuration</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form id="adminForm" class="form-horizontal" method="post" action="<?php echo site_url('teacher/properties/save'); ?>" >
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label class="control-label">Max Number of Courses <span class="mandetory_field">*</span></label>
                                            <input type="text" name="no_of_courses" value="<?php echo $no_of_courses; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label class="control-label">Max Number of Exams per Course <span class="mandetory_field">*</span></label>
                                            <input type="text" name="no_of_exams_per_course" value="<?php echo $no_of_exams_per_course; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label class="control-label">Max Number of Questions per Exam <span class="mandetory_field">*</span></label>
                                            <input type="text" name="no_of_questions_per_exam" value="<?php echo $no_of_questions_per_exam; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-info" ><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-info" href="<?php echo site_url('teacher/dashboard'); ?>">Back</a>
    </div>
    
    <!--/ Contact-->
    
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
<!--    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>-->
    <script type="text/javascript">

        $(function(){
            $("#adminForm").validate({
                rules : {
                    no_of_courses : {required : true, number : true, min : 1},
                    no_of_exams_per_course : {required : true, number : true, min : 1},
                    no_of_questions_per_exam : {required : true, number : true, min : 1},
                }
            });

        });

    </script>
  </body>
</html>






