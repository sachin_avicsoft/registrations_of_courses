<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- This should be from i18n file -->    
    <title>Registration of Courses</title>   
    
    <!-- Navigation -->
    <?php $this->load->view('include/css', 'refresh'); ?> 
    <style type="text/css">
        .form-group {
            margin-bottom: 10px;
        }
    </style>
    <!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">-->
  </head>
  <body>
    
    <!-- Navigation -->
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <!------ Include the above in your HEAD tag ---------->
    <div class="container" style="margin-top: 5%;">
    	<div class="row">
            <div class="col-md-6 col-md-offset-3">
                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                <div class="panel panel-login" style="border: 1px solid #e6e6e6">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center">
                                <h3>Create Admin User</h3>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form class="form" id="adminForm" action="<?php echo site_url('create-admin/save'); ?>" method="post" role="form" >
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>First Name</label>
                                            <input type="text" name="first_name" class="form-control" >
                                        </div>
                                        <div class="col-md-12">
                                            <label>Last Name</label>
                                            <input type="text" name="last_name" class="form-control" >
                                        </div>
                                        <div class="col-md-12">
                                            <label>Username</label>
                                            <input type="text" name="email" class="form-control" >
                                        </div>
                                        <div class="col-md-12">
                                            <label>Contact Number</label>
                                            <input type="text" name="contact_number" class="form-control" >
                                        </div>
                                        <div class="col-md-12">
                                            <label>Password</label>
                                            <input type="password" name="password" id="password" class="form-control" >
                                        </div>
                                        <div class="col-md-12">
                                            <label>Confirm Password</label>
                                            <input type="password" name="confirm_password" class="form-control" >
                                        </div>
                                        <div class="col-md-12" style="margin-top : 10px;">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login btn-info" value="Save">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--/ Contact-->
    
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
<!--    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>-->
    <script type="text/javascript">

        $(function(){
            $("#adminForm").validate({
                rules : {
                    first_name : {required : true},
                    last_name : {required : true},
                    email : {required : true, email : true},
                    contact_number : {required : true, number : true, minlength : 10, maxlength : 10},
                    password : {required :true, minlength : 8},
                    confirm_password : {required : true, equalTo : "#password"},
                }
            });

        });

    </script>
  </body>
</html>






