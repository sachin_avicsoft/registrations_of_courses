<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- This should be from i18n file -->    
    <title>Registration of Courses</title>   
    
    <!-- Navigation -->
    <?php $this->load->view('include/css', 'refresh'); ?> 
    <!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">-->
  </head>
  <body>
    
    <!-- Navigation -->
    <?php // $this->load->view('include/nav', 'refresh'); ?> 
    <!------ Include the above in your HEAD tag ---------->
    <div class="container" style="margin-top: 10%;">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
                            <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                            <div class="panel panel-login" style="border: 1px solid #e6e6e6">
                                        <div class="panel-heading" style="text-align: center">
                                            <h3>Login</h3>
                                            <hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
                                                            <form id="loginForm" action="<?php echo site_url(); ?>" method="post" role="form" style="display: block;">
                                                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                                    <input name="action" type="hidden" value="login" class="form-control">
                                                                    <div class="form-group">
                                                                        <input type="text" name="email" id="username" tabindex="1" class="form-control" placeholder="Username" value="">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-sm-6 col-sm-offset-3">
                                                                                <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login btn-info" value="Log In">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    
    <!--/ Contact-->
    
    <?php //$this->load->view('include/footer', 'refresh'); ?>     
    <?php $this->load->view('include/js', 'refresh'); ?>     
<!--    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>-->
    <script type="text/javascript">

        $(function(){
            $("#loginForm").validate({
                rules : {
                    email : {required : true, email : true},
                    password : {required :true},
                }
            });

//            $("#forgetPasswordForm").validate({
//                rules : {
//                    email : {required : true, email : true},
//                }
//            });
//
//            $('#forget_password_form_link').on('click',function(){
//                $('#forgetPasswordForm').show();
//                $('#loginForm').hide();
//                $('#forget_password_form_link').hide();
//                $('#login_form_link').show();
//            });
//
//            $('#login_form_link').on('click',function(){
//                $('#forgetPasswordForm').hide();
//                $('#loginForm').show();
//                $('#forget_password_form_link').show();
//                $('#login_form_link').hide();
//            });

        });

    </script>
  </body>
</html>






