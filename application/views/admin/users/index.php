<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Milestone</title>    
    <?php $this->load->view('admin/include/css', 'refresh'); ?>    
</head>
<body >
    <?php $this->load->view('admin/include/header', 'refresh'); ?> 
<div class="wrapper">     
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                  <?php $this->load->view('include/sidebar', 'refresh'); ?>  
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <div class="content-wrapper">
                    <section class="content">       
                        <div class="row">                
                            <div class="col-lg-12">
                                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                                <p class="clearfix">
                                    <a target="_self" class="btn btn-info pull-right" href="<?php echo site_url('admin/users/add') ?>">Add User</a>
                                </p>
                                <div class="panel panel-default clearfix">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">User List</h3>
                                    </div>
                                    <div class="panel-body">                                                       
                                        
                                        <table id="example" class="table table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th style="width: 20px">SN</th>
                                                    <th>Firstname</th>
                                                    <th>Lastname</th>
                                                    <th>Role</th>
                                                    <th>Last Login</th>
                                                    <th style="width: 16%">Action</th>                                    
                                                </tr>
                                            </thead>
                                        </table>                                
                                    </div>
                                </div>                                        
                            </div>              
                        </div>  
                    </section>        
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('admin/include/js', 'refresh'); ?> 
<script type="text/javascript">

laodUsers();
$("#nav_users").addClass("active-bg-color");
 

function laodUsers(){
    
   $('#example').DataTable( {
       
        "bLengthChange": false,
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "bFilter": true,
        "sDom":"lrtip",
        "ajax": {
            "url" :"<?php echo site_url("admin/users/list"); ?>",
            "type": "get",
        },        
        "columns": [
            { "data": "" },
            { "data": "firstname" },
            { "data": "lastname" },
            { "data": "role" },
            { "data": "" },
            { "data": "" }
        ],
        
          "columnDefs": [ 
        
        {         
            "targets": 0,
             render: function ( data, type, row, meta ) {                
                return meta.settings._iDisplayStart + meta.row + 1;
             }
         },
        {         
            "targets": 4,
             render: function ( data, type, row, meta ) {         
                var date1 = row.date_time;
                var date_time = moment(date1).format('MM/DD/YYYY HH:mm:ss');
                return date_time;
             }
         }, 
        {
            "targets": 5,
             render: function ( data, type, row, meta ) {
                 
                 var eurl ="<?php echo site_url('admin/users/edit?id=');?>" + row.id;
                 var edit = '<a target="_self" class="btn btn-xs btn-success" href="'+eurl+'" title="Edit"><li class="fa fa-edit"></li></a>';
                 var del = '<button class="btn btn-xs btn-danger"  onclick="deleteUser('+row.id+')" title="Delete"><li class="fa fa-remove"></li></button>';
                 return edit+" "+del;
             }
        },
        
        
        
        ]
    } );
} 

function deleteUser(id){
    
    if(confirm("Are you sure you want to delete the user?")){    
        $.ajax({
        url :'<?php echo site_url("admin/users/delete?id=") ?>'+id,
        type:'get',
        dataType:'json',
        success:function(data){
          var jsonData = eval(data);          
          if(jsonData.result === "success") {
              $('#example').DataTable().draw();
           } else {
                alert(jsonData.msg);   
           }
        },
        error:function(jqXhr, status){
            alert("Error occured while processing the request.");                                
        }                     
    });
    }
     
    
}
            
</script>   
</body>
</html>