<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Milestone</title>
    <?php $this->load->view('admin/include/css', 'refresh'); ?>    
</head>
<body>
    <?php $this->load->view('admin/include/header', 'refresh'); ?>
    <div class="wrapper">
        <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                  <?php $this->load->view('include/sidebar', 'refresh'); ?>  
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <div class="content-wrapper">
                    <section class="content">       
                        <div class="row">                
                            <div class="col-lg-12">
                                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                                <div class="panel panel-default clearfix">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <?php

                                                if($id == ""){
                                                        echo "Add User";
                                                } else {
                                                        echo "Edit User";
                                                }

                                            ?>
                                        </h3>
                                    </div>
                                    <div class="panel-body">                                                       

                                        <form id="userForm" class="form form-horizontal" action="<?php echo site_url("admin/users/save") ?>" method="post">
	                                
	                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
	                                <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control"/>
                                        
                                        <div class="form-group">
	                                    <label for="firstname" class="control-label col-md-2">Firstname</label>
	                                    <div class="col-md-8">
	                                        <input type="input" value="<?php echo $firstname; ?>" name="firstname" class="form-control"/>
	                                        <span><?php echo form_error("firstname"); ?></span>
	                                    </div>
	                                </div>
                                        <div class="form-group">
	                                    <label for="lastname" class="control-label col-md-2">Lastname</label>
	                                    <div class="col-md-8">
	                                        <input type="input" value="<?php echo $lastname; ?>" name="lastname" class="form-control"/>
	                                        <span><?php echo form_error("lastname"); ?></span>
	                                    </div>
	                                </div>
                                        <div class="form-group">
	                                    <label for="tgi_number" class="control-label col-md-2">TGI Number</label>
	                                    <div class="col-md-8">
                                                <input type="input" value="<?php echo $tgi_number; ?>" id="tgi_number" name="tgi_number" class="form-control" maxlength="8"/>
	                                        <span><?php echo form_error("tgi_number"); ?></span>
                                                <div class="error" id="messagechecked"></div>
	                                    </div>
	                                </div>
                                        <?php if($id == "") { ?>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Password</label>
                                                <div class="col-md-8">
                                                    <input type="password" id="password" name="password" class="form-control" />
                                                    <span class="error"><?php echo form_error('password'); ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Confirmed Password</label>
                                                <div class="col-md-8">
                                                    <input type="password" id="confirmpassword" name="confirmpassword" class="form-control" />
                                                    <span class="error"><?php echo form_error('password'); ?></span>
                                                </div>
                                            </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Security Question</label>
                                            <div class="col-md-4">
                                                <select name="question_id" class="form-control">
                                                    <?php foreach ($question_list as $key => $value) {
                                                        if($question_id == $value['id']){
                                                            echo '<option value="'.$value["id"].'" selected="selected">'.$value["question"].'</option>';
                                                        } else {
                                                            echo '<option value="'.$value["id"].'">'.$value["question"].'</option>';
                                                        }
                                                        
                                                    }?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Answer</label>
                                            <div class="col-md-4">
                                                <input type="text" name="answer" class="form-control" value="<?php echo $answer?>" />
                                                <span class="error"><?php echo form_error('answer'); ?></span>
                                            </div>
                                        </div>
                                        <?php } else { ?>
                                            <input type="hidden" name="question_id" value="<?php echo $question_id; ?>" class="form-control"/>
                                            <input type="hidden" name="answer" value="<?php echo $answer; ?>" class="form-control"/>
                                         <?php } ?>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Role</label>
                                            <div class="col-md-4">
                                                <select name="role" class="form-control">
                                                    <option value="USER" <?php if($role == "USER") { ?> selected = "selected" <?php } ?>>USER</option>
                                                    <option value="ADMIN" <?php if($role == "ADMIN") { ?> selected = "selected" <?php } ?>>ADMIN</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: none">
                                            <label class="control-label col-md-2">Select User Designation</label>
	                                    <div class="col-md-4">
	                                        <select name="user_designation_id" class="form-control">
                                                    <option value="">Select Designation</option>
                                                    //<?php 
//                                                        if($user_designation_list){
//                                                            foreach ($user_designation_list as $key => $value) {
//                                                                if($user_designation_id == $value['id']){
//                                                                    echo "<option value='".$value['id']."' selected='selected'>".$value['role_name']."</option>";
//                                                                } else {
//                                                                    echo "<option value='".$value['id']."'>".$value['role_name']."</option>";
//                                                                }
//                                                            }
//                                                        }
//                                                    ?>
                                                </select>
	                                    </div>
                                        </div>
	                                <div class="form-group">  
	                                    <div class="col-md-offset-2 col-md-8">            
	                                         <button type="submit" class="btn btn-success">
	                                           <li class="fa fa-check"></li> Save
	                                        </button>
	                                        <a href="<?php echo site_url("admin/users") ?>" class="btn btn-danger">
	                                           <li class="fa fa-reply"></li> Back
	                                        </a>
	                                    </div>
	                                </div>
	                            </form>                                

                                    </div>
                                </div>
                                <?php if($id != "") { ?>
                                <div class="panel panel-default clearfix">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <?php
                                                echo "Change Password";
                                            ?>
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <form id="userForm" class="form form-horizontal" action="<?php echo site_url("admin/users/changepassword") ?>" method="post">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                            <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control"/>

<!--                                            <div class="form-group">
                                                <label for="password" class="control-label col-md-2">New Password</label>
                                                <div class="col-md-8">
                                                    <input type="input" name="password" class="form-control"/>
                                                    <span><?php echo form_error("password"); ?></span>
                                                </div>
                                            </div>-->
                                            <div class="form-group">  
                                                <div class="col-md-offset-2 col-md-8">            
                                                    <button type="submit" class="btn btn-success">
                                                        <li class="fa fa-check"></li> Reset Password
                                                    </button>
                                                </div>
                                            </div>
                                            
                                        </form>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>              
                        </div>  
                    </section>        
                </div>
            </div>
        </div>
    </div>
</div>
    <?php $this->load->view('admin/include/js', 'refresh'); ?>
<script type="text/javascript">
    $("#nav_users").addClass("active-bg-color");
    
    $('#userForm').validate({
        rules : {
            firstname : {required : true},
            lastname : {required : true},
            role : {required : true},
            tgi_number : {required : true},
            answer : {required : true},
            password : {required:true},
           confirmpassword : {required : true, equalTo: "#password"},
        },
        messages : {
           confirmpassword : {"equalTo" :"Password and confirm password must be same."}
       }
    });
    
    $("#tgi_number").on('change',function(){
    var id = $('#tgi_number').val();
    $.ajax({
        url : '<?php echo site_url("check?id=")?>' + id,
        dataType : 'json',
        type : 'GET',
        success:function(res){
            if(res === true){
                $('#tgi_number').val("");
                $('#messagechecked').html('The TGI number is already taken.Please enter another TGI number');
            } else {
                $('#messagechecked').html("");
            }
        },
        error:function(){
            console.log("Unable to load the check");
        }
    });
});
    
</script>
</body>
</html>