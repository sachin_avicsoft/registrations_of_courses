<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TeacherQuestionController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();       
        $this->checkLogin();
        $this->checkRoles(array("TEACHER"));
        $this->getConfigProperties();
        $this->load->model('Course');
        $this->load->model('Exam');
        $this->load->model('Question');
        $this->load->library('upload');
     }
     /**
      * Check for course and exam exists
      */
     public function checker($courseId, $examId){
        if($courseId == '' && $courseId == null){
            
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course is not present', "error"));
            redirect('teacher/courses');
        } else {
            $course = $this->Course->get($courseId);
            if(!$course){
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course is not present', "error"));
                redirect('teacher/courses');
            } else if($course['status'] != 'enable'){
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course is not enable', "error"));
                redirect('teacher/courses');
            } else if($examId == '' && $examId == null){
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Exam is not present', "error"));
                redirect('teacher/course/exams?courseId='.$courseId);
            } else {
                $exam = $this->Exam->get($examId);
                if(!$exam){
                    $this->session->set_flashdata('msg', 
                        $this->alertmessage->printResultMessage('Exam is not present', "error"));
                    redirect('teacher/course/exams?courseId='.$courseId);
                } else if($exam['status'] != 'enable'){
                    $this->session->set_flashdata('msg', 
                        $this->alertmessage->printResultMessage('Exam is not enable', "error"));
                    redirect('teacher/course/exams?courseId='.$courseId);
                }
            } 
        }
    }

    /**
     * Load questions list
     */
     public function index(){
        
        $courseId = $this->input->get('courseId');
        $examId = $this->input->get('examId');
        $this->checker($courseId,$examId);
        $questionList = $this->Question->get_question_list_by_examId($examId);
        if(count($questionList) < $this->getMaxQuestionsCount()){
            $data['buttonShow'] = TRUE;
        } else {
            $data['buttonShow'] = FALSE;
        }
        $data['exam'] = $this->Exam->get($examId);
        $data['course'] = $this->Course->get($courseId);
        $data['questionList'] = $questionList;
        $data['courseId'] = $courseId;
        $data['examId'] = $examId;
        $this->load->view('teacher/question/index',$data);
    }
    /**
     * Load view to add new question to exam
     */
    public function add(){
        $courseId = $this->input->get('courseId');
        $examId = $this->input->get('examId');
        $this->checker($courseId,$examId);
        
        $questionList = $this->Question->get_question_list_by_examId($examId);
        if(count($questionList) == $this->getMaxQuestionsCount() || count($questionList) > $this->getMaxQuestionsCount()){
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Max limit for questions reached!!', "error"));
                redirect('teacher/course/exam/questions?courseId='.$courseId.'&examId='.$examId);
        }
        
        $data = array(
            'id' => '',
            'question' => '',
            'question_type' => '',
            'status' => 'enable',
            'marks' => '',
        );
        $data['length'] = 240;
        $data['courseId'] = $courseId;
        $data['examId'] = $examId;
        $data['exam'] = $this->Exam->get($examId);
        $data['course'] = $this->Course->get($courseId);
        $data['optionList'] = array();
        $this->load->view('teacher/question/form',$data);
    }
    /**
     * Load view to edit question from exam
     */
    public function edit(){
        
        $id = $this->input->get('id');
        $courseId = $this->input->get('courseId');
        $examId = $this->input->get('examId');
        $this->checker($courseId,$examId);
        
        
        
        $row = $this->Question->get($id);
        if($row){
            $data = array(
                'id' => $row['id'],
                'question' => $row['question'],
                'question_type' => $row['question_type'],
                'status' => $row['status'],
                'marks' => $row['marks'],
            );
            $length = strlen($row['question']); 
            $data['length'] = 240 - $length;
            $data['courseId'] = $courseId;
            $data['examId'] = $examId;
            $data['exam'] = $this->Exam->get($examId);
            $data['course'] = $this->Course->get($courseId);
            $data['optionList'] = $this->Question->get_question_option_list($id);
            $this->load->view('teacher/question/form',$data);
        } else {
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Id is not present', "error"));
            redirect('teacher/course/exam/questions?courseId='.$courseId.'&examId='.$examId);
        }
    }
    /**
     * Save or update the question data
     */
    public function save(){
        $courseId = $this->input->get('courseId');
        $examId = $this->input->get('examId');
        $this->checker($courseId,$examId);
        $this->form_validation->set_rules('question', 'question', 'required');
        $this->form_validation->set_rules('question_type', 'question_type', 'required');
        $this->form_validation->set_rules('status', 'status', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->alertmessage->printResultMessage('All fields are required', "error"));
            if(isset($_POST['id']) && $_POST['id'] == ""){  
                redirect('teacher/course/exam/questions/add?courseId='.$courseId.'&examId='.$examId);
            } else {
                redirect('teacher/course/exam/questions/edit?courseId='.$courseId.'&examId='.$examId.'&id='.$_POST['id']);
            }
        } else {
            if(isset($_POST['id']) && $_POST['id'] == ""){
                $questionList = $this->Question->get_question_list_by_examId($examId);
                if(count($questionList) == $this->getMaxQuestionsCount() || count($questionList) > $this->getMaxQuestionsCount()){
                    $this->session->set_flashdata('msg', 
                            $this->alertmessage->printResultMessage('Max limit for questions reached!!', "error"));
                        redirect('teacher/course/exam/questions?courseId='.$courseId.'&examId='.$examId);
                }
                
                $check= FALSE;
                $question_type = $this->input->post('question_type');
                if($question_type == 'multichoice' || $question_type == 'fillintheblank'){
                    $my_array = array();foreach($_POST['option'] as $row){$my_array[] = $row;}$options = $my_array;
                    for($i = 0; $i < count($options); $i++){
                        if($options[$i] != ''){
                            $check = TRUE;
                        }
                    }
                    if(!$check){
                        $this->session->set_flashdata('msg', 
                            $this->alertmessage->printResultMessage('No option present for this question', "error"));
                        redirect('teacher/course/exam/questions?courseId='.$courseId.'&examId='.$examId);
                    }
                    $questionId = $this->Question->add();
                    for($i = 0; $i < count($options); $i++){
                        if($options[$i] != ''){
                            $this->Question->add_option($questionId,$options[$i]);
                        }
                    }
                } else {
                    $questionId = $this->Question->add();
                }
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Question added successfully', "success"));
                redirect('teacher/course/exam/questions?courseId='.$courseId.'&examId='.$examId);
            } else {
                $questionId = $this->input->post('id');
                $question_options = $this->Question->get_question_option_list($questionId);
                $question_type = $this->input->post('question_type');
                if($question_type == 'multichoice' || $question_type == 'fillintheblank'){
                    $my_array = array();foreach($_POST['option'] as $row){$my_array[] = $row;}$options = $my_array;
                    $check= FALSE;
                    for($i = 0; $i < count($options); $i++){
                        if($options[$i] != ''){
                            $check = TRUE;
                        }
                    }
                    if(!$check){
                        $this->session->set_flashdata('msg', 
                            $this->alertmessage->printResultMessage('No option present for this question', "error"));
                        redirect('teacher/course/exam/questions/edit?courseId='.$courseId.'&examId='.$examId.'&id='.$questionId);
                    }
                    for($i = 0; $i < count($options); $i++){
                        if($options[$i] != ''){
                            $this->Question->add_option($questionId,$options[$i]);
                        }
                    }
                }
                
                foreach ($question_options as $key => $value) {
                    $this->Question->delete_option($value['id']);
                }
                
                $this->Question->update();
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Question updated successfully', "success"));
                redirect('teacher/course/exam/questions?courseId='.$courseId.'&examId='.$examId);
            }
        }
    }
    /**
     * Delete the question
     */
    public function delete(){
        $id = $this->input->get('id');
        $courseId = $this->input->get('courseId');
        $examId = $this->input->get('examId');
        $this->checker($courseId,$examId);
        $ajaxResult = array();
        if($id == ""){
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Id is not present', "error"));
        } else {
            $this->Question->delete();
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Question deleted successfully', "success"));
        }
        redirect('teacher/course/exam/questions?courseId='.$courseId.'&examId='.$examId);
    }
    /**
     * Change status to enable or disable
     */
    public function change_status(){
        $id = $this->input->get('id');
        $status = $this->input->get('status');
        $courseId = $this->input->get('courseId');
        $examId = $this->input->get('examId');
        if($status == 1){
            $status = 'disable';
        } else {
            $status = 'enable';
        }
        if($id == ""){
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Id is not present', "error"));
        } else {
            $this->Question->change_status($id, $status);
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Question status change successfully', "success"));
        }
        redirect('teacher/course/exam/questions?courseId='.$courseId.'&examId='.$examId);
    }
    
}