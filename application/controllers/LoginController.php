<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {
    
    public function __construct() {
        parent::__construct();      
    }

    /**
     * Load login page and after login redirect to particular users dashboard
     */
    public function index()
    {
        $path = 'config.php';
        $file = file_exists($path);
        if($file == 1){
            redirect('configurations');
        }
        $this->load->model('User');
        $this->load->database();
        if(array_key_exists('REMOTE_USER', $_SERVER) && strlen($_SERVER['REMOTE_USER'])){
            $role = '';
            if (strpos($_SERVER['HTTP_SHIB_EP_UNSCOPEDAFFILIATION'], 'student') !== false) {
                $role = 'STUDENT';
            } elseif (strpos($_SERVER['HTTP_SHIB_EP_UNSCOPEDAFFILIATION'], 'teacher') !== false) {
                $role = 'TEACHER';
            } elseif (strpos($_SERVER['HTTP_SHIB_EP_UNSCOPEDAFFILIATION'], 'admin') !== false) {
                $role = 'ADMIN';
            }
            //check user is present in database if not create entry username. orgunit
            $username = $_SERVER['REMOTE_USER'];
            $user_chemnitz = $this->User->get_user_by_username($username);
            $userId = '';
            if(!$user_chemnitz){
                
                $first_name = $_SERVER['HTTP_SHIB_INETORGPERSON_GIVENNAME'];
                $last_name = $_SERVER['HTTP_SHIB_PERSON_SURNAME'];
                $org_unit_number = $_SERVER['HTTP_SHIB_ORGPERSON_ORGUNITNUMBER'];
                $email = $_SERVER['HTTP_SHIB_ORGPERSON_EMAILADDRESS'];
                $userId = $this->User->add_chemnitz_user($username, $first_name, $last_name, $email, $org_unit_number, $role);
                
            } else {
                $userId = $user_chemnitz['id'];
            }
            $user = $this->User->get($userId);
            //take user with username
            $sessiondata = array(
                'user_id' => $user['id'],
                'email' => $user['email'],
                'username' => $user['username'],
                'role' => $user['role'],
                'loginuser' => TRUE
            );
            $this->session->set_userdata('logged_in', $sessiondata);

            if($user['role'] == "ADMIN"){
                   redirect('admin/dashboard');
            } else if($user['role'] == "TEACHER"){
                    redirect("teacher/dashboard");
            } else if($user['role'] == "STUDENT") {
                   redirect("student/dashboard");
            } else {
                redirect();
            }

        } else {
            $this->load->view('login');
        }
    }
    /**
     * Sign out loged in user
     */
    public function logout(){
        $this->session->unset_userdata('logged_in');       
        redirect('', 'refresh');		
    }   
}
