<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudentExamController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();      
        $this->checkLogin();
        $this->checkRoles(array("STUDENT"));
        $this->load->model('Course');
        $this->load->model('Exam');
        $this->load->model('StudentCourse');
        $this->load->model('StudentExam');
        $this->load->model('StudentAnswer');
        $this->load->model('Question');
        $this->load->library('upload');
     }
     /**
      * Check for course and exam exists for particular user
      */
    public function checker($courseId, $examId){
        if($courseId == '' && $courseId == null){
            
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course is not present', "error"));
            redirect('teacher/courses');
        } else {
            $course = $this->Course->get($courseId);
            if(!$course && $course['status'] != 'enable'){
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course is not present', "error"));
                redirect('teacher/courses');
            } 
        }
    }
    /**
     * Load exam information page
     */
    public function index(){
        
        $courseId = $this->input->get('courseId');
        $examId = $this->input->get('id');
        $this->checker($courseId, $examId);
        
        $exam = $this->Exam->get($examId);
        if($exam['course_id'] != $courseId){
            show_404();
        }
        if($exam['status'] == 'enable'){
            $examList = $this->Question->get_question_list_by_examId_by_enable_status($examId);
            if($examList){
                $data['examCount'] = count($examList);
            } else {
                $data['examCount'] = 0;
            }
        } else {
            show_404();
        }
        $data['exam'] = $exam;
        $data['course'] = $this->Course->get($courseId);
        $data['courseId'] = $courseId;
        
        $this->load->view('student/exam/index',$data);
    }
    /**
     * Load list of exams for particular student
     */
    public function examList(){
        $courseId = $this->input->get('id');
        $course = $this->Course->get($courseId);
        $userdata = $this->session->userdata('logged_in');
        $userId = $userdata['user_id'];
        $student_course = $this->StudentCourse->get_student_course_by_user_id_and_course_id($userId, $courseId);
        if(!$student_course){
            show_404();
        }
        $examList = $this->Exam->get_exam_list_by_courseId_by_enable_status($courseId);
        if($examList){
            for($i = 0; $i < count($examList); $i++){
                $date1=date('Y/m/d');
                $tempArr=explode('/', $examList[$i]['end_date']);
                $tempArr1 = explode('/', $examList[$i]['start_date']);
                $date2 = date("Y/m/d", mktime(0, 0, 0, $tempArr[1], $tempArr[0], $tempArr[2]));
                $date3 = date("Y/m/d", mktime(0, 0, 0, $tempArr1[1], $tempArr1[0], $tempArr1[2]));
                if($date2 >= $date1 && $date3 <= $date1){
                    $examList[$i]['is_expired'] = 'active';
                } else {
                    $examList[$i]['is_expired'] = 'expired';
                }
                    $questionList = $this->Question->get_question_list_by_examId_by_enable_status($examList[$i]['id']);
                    if($questionList){
                        $examList[$i]['questionCount'] = count($questionList);
                    } else {
                        $examList[$i]['questionCount'] = 0;
                    }

                    $studentExam = $this->StudentExam->get_exam_by_user_and_student_course_id($userId, $examList[$i]['id'], $student_course['id']);
                    if($studentExam){
                        $examList[$i]['studentExamId'] = $studentExam['id'];
                        $examList[$i]['resultStatus'] = $studentExam['status'];
                        $examList[$i]['resultGrade'] = $studentExam['grade'];
                        if($studentExam['submitted'] == 1){
                            $examList[$i]['submitStatus'] = 'Appeared';
                        } else {
                            $examList[$i]['submitStatus'] = 'Not Appeared';
                        }
                    } else {
                        $examList[$i]['resultStatus'] = '';
                        $examList[$i]['resultGrade'] = '';
                        $examList[$i]['submitStatus'] = 'Not Appeared';
                        $examList[$i]['studentExamId'] = 0;
                    }
            }
        }
        $data['examList'] = $examList;
        $data['course'] = $course;
        $data['courseId'] = $courseId;
        $this->load->view('student/exam/list',$data);
    }
    /**
     * Submit the exam of particular student
     */
    public function submit(){
        $userdata = $this->session->userdata('logged_in');
        $userId = $userdata['user_id'];
        $examId = $this->input->post('examId');
        $exam = $this->Exam->get($examId);
        $courseId = $exam['course_id'];
        $student_course = $this->StudentCourse->get_student_course_by_user_id_and_course_id($userId, $courseId);
        $studentExam = $this->StudentExam->get_exam_by_user_and_student_course_id($userId, $examId, $student_course['id']);
        if($studentExam){
            $my_array = array();foreach($_POST['questionId'] as $row){$my_array[] = $row;}$questionIds = $my_array;
            $my_array = array();foreach($_POST['questionType'] as $row){$my_array[] = $row;}$questionTypes = $my_array;
            if(count($questionIds) > 0){
                for($i = 0; $i < count($questionIds); $i++){
                    $questionId = $questionIds[$i];
                    if($questionId != '' && $questionId != null){
                        $question_type = $questionTypes[$i];
                        if($question_type == 'multichoice' || $question_type == 'fillintheblank'){
                            if(isset($_POST['solution_'.$questionId])){
                                $my_array = array();foreach($_POST['solution_'.$questionId] as $row){$my_array[] = $row;}$solutions = $my_array;
                                $solution = implode(",",$solutions);
                            } else {
                                $solution = '';
                            }
                            $this->StudentAnswer->submit_answer($questionId, $solution);
                        } else {
                            $solution = $this->input->post('solution_'.$questionId);
                            $this->StudentAnswer->submit_answer($questionId, $solution);
                        }
                    }
                }
            }

            $value = 1;
            $this->StudentExam->update_submitted($studentExam['id'], $value);
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Your exam is submitted successfully.', "success"));
            redirect('student/course/exam-list?id='.$courseId);
        } else {
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Your exam is not submitted.Please try again!', "error"));
            redirect('student/course/exam-list?id='.$courseId);
        }
    }
    
    public function submit_answer(){
        $id = $this->input->get('id');
        $value = $this->input->get('value');
        $data = $this->StudentAnswer->submit_answer($id, $value);
        echo json_encode($data); 
        exit;
        
    }
    /**
     * Start the exam of particular student
     */
    public function start(){
        $userdata = $this->session->userdata('logged_in');
        $userId = $userdata['user_id'];
        $examId = $this->input->get('examId');
        $exam = $this->Exam->get($examId);
        
        
        $courseId = $exam['course_id'];
        $this->checker($courseId, $examId);
        $date1=date('Y/m/d');
        $tempArr=explode('/', $exam['end_date']);
        $tempArr1 = explode('/', $exam['start_date']);
        $date2 = date("Y/m/d", mktime(0, 0, 0, $tempArr[1], $tempArr[0], $tempArr[2]));
        $date3 = date("Y/m/d", mktime(0, 0, 0, $tempArr1[1], $tempArr1[0], $tempArr1[2]));
        if($date2 >= $date1 && $date3 <= $date1){
            
        } else {
            $this->session->set_flashdata('msg', 
                $this->alertmessage->printResultMessage('Not able to start this exam now.', "error"));
            redirect('student/course/exam-list?id='.$courseId);
        }
        $course = $this->Course->get($courseId);
        //check for user is registered for that course
        $studentCourse = $this->StudentCourse->get_student_course_by_user_id_and_course_id($userId, $courseId);
        if(!$studentCourse){
            show_404();
        }
        
        $studentExam = $this->StudentExam->get_exam_by_user_and_student_course_id($userId, $examId, $studentCourse['id']);
        if($studentExam){
            $studentExamId = $studentExam['id'];
            if($studentExam['submitted'] == 1){
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('You have already appeared for this exam.', "error"));
            redirect('student/course/exam-list?id='.$courseId);
            } else {
                $startTime = $studentExam['start_time'];
                $examTimeLimit = $studentExam['time_limit'];
                $currentTime = date('Y-m-d H:i:s');
                $to_time = strtotime($currentTime);
                $from_time = strtotime($startTime);
//                echo round(abs($to_time - $from_time) / 60,2). " minute";
                $timeDifference = round(abs($to_time - $from_time) / 60,2);
                if($examTimeLimit < $timeDifference){
                    $studentExam = $this->StudentExam->get_exam_by_user_and_student_course_id($userId, $examId, $studentCourse['id']);
                    $value = 1;
                    $this->StudentExam->update_submitted($studentExam['id'], $value);
                    $this->session->set_flashdata('msg', 
                        $this->alertmessage->printResultMessage('You have already appeared for this exam.', "error"));
                    redirect('student/course/exam-list?id='.$courseId);
                }
            }
        } else {
            $studentExamId = $this->StudentExam->add($userId, $examId, $studentCourse['id']);
            $questionList = $this->Question->get_question_list_by_examId_by_enable_status($examId);
            if($questionList){
                foreach ($questionList as $key => $value) {
                    $this->StudentAnswer->add($userId, $studentCourse['id'], $value['id'], $studentExamId);
                }
            }
        }
        
        $studentQuestionList = $this->StudentAnswer->get_student_questions_and_answers_by_user_course_list($userId, $studentCourse['id'], $studentExamId);
        if($studentQuestionList){
            for ($i = 0; $i < count($studentQuestionList); $i++) {
                $questionId = $studentQuestionList[$i]['question_id'];
                $question = $this->Question->get($questionId);
                if($question['question_type'] == 'multichoice' || $question['question_type'] == 'fillintheblank'){
                    $questionOptions = $this->Question->get_question_option_list($questionId);
                    $question['questionOptions'] = $questionOptions;
                }
                $studentQuestionList[$i]['question'] = $question;
            }
        }
//        var_dump($studentQuestionList);
        $data['studentQuestionList'] = $studentQuestionList;
        $data['courseId'] = $courseId;
        $data['course'] = $course;
        $data['examId'] = $examId;
        $data['exam'] = $exam;
        $this->load->view('student/exam/form',$data);
    }
    
}