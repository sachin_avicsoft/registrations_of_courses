<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TeacherStudentController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();   
        $this->checkLogin();
        $this->checkRoles(array("TEACHER"));
        $this->load->model('Course');
        $this->load->model('StudentSolution');
        $this->load->model('StudentCourse');
        $this->load->model('StudentAnswer');
        $this->load->model('StudentExam');
        $this->load->model('TeacherStudent');
        $this->load->model('Question');
        $this->load->model('User');
        $this->load->model('Lecture');
        $this->load->model('Exam');
        $this->load->model('Comment');
        $this->load->library('upload');
     }
     /**
      * Load list solutions uploaded by student of particular course
      */
    public function allstudentsSolutionsSummary(){
        $courseId = $this->input->get('id');
        $course = $this->Course->get($courseId);
        if(!$course){
            show_404();
        }
        $studentCourseList = $this->StudentCourse->get_student_list_by_course($courseId);
        if($studentCourseList){
            for($i = 0; $i < count($studentCourseList); $i++){
                $student_course = $this->StudentCourse->get_student_course_by_user_id_and_course_id($studentCourseList[$i]['id'], $courseId);
                $solutions = $this->StudentSolution->get_solution_list_by_course_id_and_user_id_and_course_id($courseId, $studentCourseList[$i]['id'], $student_course['id']);
                if($solutions){
                    $studentCourseList[$i]['solutions'] = $solutions;
                }  else {
                    $studentCourseList[$i]['solutions'] = array();
                }
            }
            $studentCourseList = $studentCourseList;
        }
        $data['studentCourseList'] = $studentCourseList;
        $data['course'] = $course;
        
        $this->load->view('teacher/student/solutions', $data);
    }
    /**
     * Deregister all student from particular course 
     */
    public function deregistered_students(){
        $courseId = $this->input->get('id');
        $studentCourseList = $this->StudentCourse->get_student_list_by_course_id($courseId);
//        var_dump($studentList);
        if($studentCourseList){
            foreach ($studentCourseList as $key => $value) {
                $this->StudentCourse->delete($value['id']);
            }
        }
        $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('All students are deregistered for this course successfully', "success"));
        redirect('teacher/students?course_id='.$courseId);
    }
    /**
     * Load student data regarding particular course
     */
    public function index(){
        $courseId = '';
        $courseList = $this->Course->get_course_list();
        if($courseList){

            if(isset($_POST['action']) && $_POST['action'] == 'course'){
                $courseId = $this->input->post('course_id');
                $studentList = $this->TeacherStudent->get_student_list_by_course_id($courseId);
            } else {
                $courseId = $this->input->get('course_id');
                
                if($courseId != '' && $courseId != NULL){
                    $course = $this->Course->get($courseId);
                    if(!$course){
                        show_404();
                    }
                    $studentList = $this->TeacherStudent->get_student_list_by_course_id($courseId);
                } else {
                    $courseId = $courseList[0]['id'];
                    $studentList = $this->TeacherStudent->get_student_list_by_course_id($courseId);
                }

            } 
        } else {
            $studentList = false;
        }
         
        $data['courseList'] = $courseList;
        $data['studentList'] = $studentList;
        $data['course_id'] = $courseId;
        $this->load->view('teacher/student/index',$data);
    }
    /**
     * Load exams list of particular student regarding particular course
     */
    public function exams(){
        $userId = $this->input->get('user_id');
        $user = $this->User->get($userId);
        if(!$user || $user['role'] != 'STUDENT'){
            show_404();
        }
        $courseId = $this->input->get('course_id');
        $student = $this->User->get($userId);
        $course = $this->Course->get($courseId);
        if(!$course){
            show_404();
        }
        $student_course = $this->StudentCourse->get_student_course_by_user_id_and_course_id($userId, $courseId);
        if(!$student_course){
            show_404();
        }
        $examList = $this->Exam->get_exam_list_by_courseId($courseId);
        if($examList){
            for($i = 0; $i < count($examList); $i++){
                $studentExam = $this->StudentExam->get_exam_by_user_and_student_course_id($userId, $examList[$i]['id'], $student_course['id']);
                if($studentExam){
                    $examList[$i]['studentExamId'] = $studentExam['id'];
                    $examList[$i]['resultStatus'] = $studentExam['status'];
                    $examList[$i]['resultGrade'] = $studentExam['grade'];
                    if($studentExam['submitted'] == 1){
                        $examList[$i]['submitStatus'] = 'Appeared';
                    } else {
                        $examList[$i]['submitStatus'] = 'Not Appeared';
                    }
                } else {
                    $examList[$i]['resultStatus'] = '';
                    $examList[$i]['resultGrade'] = '';
                    $examList[$i]['submitStatus'] = 'Not Appeared';
                    $examList[$i]['studentExamId'] = 0;
                }
            }
        }
        $data['examList'] = $examList;
        $data['student'] = $student;
        $data['course'] = $course;
        $this->load->view('teacher/student/exam/index', $data);
    }
    /**
     * Load answers submitted by particular student for particular exam
     */
    public function getStudentExamAnswers(){
        $studentExamId = $this->input->get('id');
        $studentExam = $this->StudentExam->get($studentExamId);
        if(!$studentExam){
            show_404();
        }
        $userId = $studentExam['user_id'];
        $examId = $studentExam['exam_id'];
        $studentCourseId = $studentExam['student_course_id'];
        $studentCourse = $this->StudentCourse->get($studentCourseId);
        if(!$studentCourse){
            show_404();
        }
        $courseId = $studentCourse['course_id'];
        
        if($studentExam['submitted'] == 1){
            $examQuestions = $this->StudentAnswer->get_student_questions_and_answers_by_user_exam_id($userId, $studentExam['id']);
            for($i = 0; $i < count($examQuestions); $i++){
                $questionId = $examQuestions[$i]['question_id'];
                $question = $this->Question->get($questionId);
                if($question['question_type'] == 'multichoice' || $question['question_type'] == 'fillintheblank'){
                    $questionOptions = $this->Question->get_question_option_list($questionId);
                    $question['questionOptions'] = $questionOptions;
                }
                $examQuestions[$i]['question'] = $question;
//                var_dump($questionOptions[$i]['question']);
            }
            $examQuestions = $examQuestions;
//            var_dump($examQuestions);
            $student = $this->User->get($userId);
            $course = $this->Course->get($courseId);
            $exam = $this->Exam->get($examId);
            $data['examQuestions'] = $examQuestions;
            $data['student'] = $student;
            $data['course'] = $course;
            $data['exam'] = $exam;
            $data['status'] = $studentExam['status'];
            $data['grade'] = $studentExam['grade'];
            $data['studentExamId'] = $studentExam['id'];
            $this->load->view('teacher/student/exam/answers', $data);
        } else {
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('This exam is not submitted by student', "error"));
            redirect('teacher/students/exams?course_id='.$courseId.'&user_id='.$userId);
        }
        
    }
    /**
     * Save the result for particular exam
     */
    public function saveResult(){
        $this->form_validation->set_rules('status', 'status', 'required');
        $this->form_validation->set_rules('grade', 'grade', 'required');
        $this->form_validation->set_rules('id', 'id', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->alertmessage->printResultMessage('All fields are required', "error"));
            redirect('teacher/students/exams/answers?id='.$_POST['id']);
        } else {         
            $studentExamId = $this->input->post('id');
            $status = $this->input->post('status');
            $grade = $this->input->post('grade');
            $this->StudentExam->update_result($studentExamId, $status, $grade);
            $this->session->set_flashdata('msg', 
                $this->alertmessage->printResultMessage('Result submitted successfully', "success"));
            redirect('teacher/students/exams/answers?id='.$studentExamId);
        }
    }
    /**
     * Load solutions list of particular student regarding particular course
     */
    public function solutions(){
        $userId = $this->input->get('user_id');
        $user = $this->User->get($userId);
        if(!$user || $user['role'] != 'STUDENT'){
            show_404();
        }
        $courseId = $this->input->get('course_id');
        $course = $this->Course->get($courseId);
        if(!$course){
            show_404();
        }
        $student = $this->User->get($userId);
        $course = $this->Course->get($courseId);
        $student_course = $this->StudentCourse->get_student_course_by_user_id_and_course_id($userId, $courseId);
        if(!$student_course){
            show_404();
        }
        $studentSolutions = $this->StudentSolution->get_solution_list_by_course_id_and_user_id_by_student_course_id($courseId, $userId, $student_course['id']);
        if($studentSolutions){
            for($i = 0; $i < count($studentSolutions); $i++){
                $lectureId = $studentSolutions[$i]['lecture_id'];
                $lecture = $this->Lecture->get($lectureId);
                $studentSolutions[$i]['lecture'] = $lecture;
            }
        }
        $data['studentSolutions'] = $studentSolutions;
        $data['student'] = $student;
        $data['course'] = $course;
        $this->load->view('teacher/student/solution/index', $data);
    }
    /**
     * Load the comments of particular solution by teacher and student user
     */
    public function getCommentOnSolution(){
        $solutionId = $this->input->get('id');
        $studentSolution = $this->StudentSolution->get($solutionId);
        if(!$studentSolution){
            show_404();
        }
        $courseId = $studentSolution['course_id'];
        $course = $this->Course->get($courseId);
        $userId = $studentSolution['user_id'];
        $student = $this->User->get($userId);
        $studentSolutionCommentList = $this->StudentSolution->get_student_solution_comment_by_solution_id($solutionId);
        $data['forumList'] = $studentSolutionCommentList;
        $data['course'] = $course;
        $data['courseId'] = $courseId;
        $data['solutionId'] = $solutionId;
        $data['student'] = $student;
        $data['file_name'] = $studentSolution['file_name'];
        if(isset($_REQUEST['type']) && $_REQUEST['type'] == 'summary'){
            $data['type'] = 'summary';
        } else {
            $data['type'] = 'comment';
        }
        $this->load->view('teacher/student/solution/solution-comment',$data);
    }
    /**
     * Add cooment for particular solution uploaded by student
     */
    public function addCommentOnSolution(){
        $solutionId = $this->input->post('student_solution_id');
        $comment = $this->input->post('comment');
        $this->form_validation->set_rules('comment', 'comment', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Comment required!', "error"));
            if(isset($_POST['action']) && $_POST['action'] == 'summary' ){
                redirect('teacher/students/solutions/comments?id='.$solutionId.'&type=summary');
            }
            redirect('teacher/students/solutions/comments?id='.$solutionId);
        } else {
            $studentSolution = $this->StudentSolution->get($solutionId);
            if($studentSolution){
                $userdata = $this->session->userdata('logged_in');
                $teacherId = $userdata['user_id'];
                $this->StudentSolution->addCommentforStudentSolutionByTeacher($teacherId);
                $courseId = $studentSolution['course_id'];
                $userId = $studentSolution['user_id'];
                $this->session->set_flashdata('msg', 
                        $this->alertmessage->printResultMessage('Comment added successfully', "success"));
                if(isset($_POST['action']) && $_POST['action'] == 'summary' ){
                    redirect('teacher/students/solutions/comments?id='.$solutionId.'&type=summary');
                }
                redirect('teacher/students/solutions/comments?id='.$solutionId);
            }
        }
    }
    
    public function studentAnswerMarks(){
        $id = $this->input->get('id');
        $marks = $this->input->get('value');
        $this->StudentAnswer->update_marks($id, $marks);
        $data = "success";
        echo json_encode($data); 
        exit;
        
    }
    
}