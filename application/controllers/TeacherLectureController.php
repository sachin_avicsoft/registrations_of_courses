<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TeacherLectureController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();      
        $this->checkLogin();
        $this->checkRoles(array("TEACHER"));
        $this->load->model('Course');
        $this->load->model('Lecture');
        $this->load->model('Exam');
        $this->load->library('upload');
    }
    /**
     * Check for particular lecture is related to given course
     */
    public function checker($courseId){
        
        if($courseId == '' && $courseId == null){
            
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course is not present', "error"));
            redirect('teacher/courses');
        } else {
            $course = $this->Course->get($courseId);
            if(!$course){
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course is not present', "error"));
                redirect('teacher/courses');
            } else if($course['status'] != 'enable'){
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course is not enable', "error"));
                redirect('teacher/courses');
            }
        }
    }
    /**
     * Load list of lectures for particular course
     */
    public function index(){
        
        $courseId = $this->input->get('courseId');
        $this->checker($courseId);
        
        $lectureList = $this->Lecture->get_lecture_list_by_courseId($courseId);
        if($lectureList){
            for($i = 0; $i < count($lectureList); $i++){
                $fileList = $this->Lecture->get_lecture_file_list($lectureList[$i]['id']);
                $contentList = $this->Lecture->get_file_list_by_lecture_id_and_type($lectureList[$i]['id']);
                $homeworkList = $this->Lecture->get_file_list_by_lecture_id_and_type($lectureList[$i]['id'], '1');
                if($homeworkList){
                    for($j = 0; $j < count($homeworkList); $j++){
                        $teacherSolutionList = $this->Lecture->get_file_list_by_lecture_id_and_file_id($lectureList[$i]['id'], $homeworkList[$j]['id']);
                        $homeworkList[$j]['teacherSolutionList'] = $teacherSolutionList;
                    }
                }
                $lectureList[$i]['fileList'] = $fileList;
                $lectureList[$i]['contentList'] = $contentList;
                $lectureList[$i]['homeworkList'] = $homeworkList;
            }
        }
        $course = $this->Course->get($courseId);
        $data['course'] = $course;
        $data['lectureList'] = $lectureList;
        $data['courseId'] = $courseId;
        
        $this->load->view('teacher/lecture/index',$data);
    }
    /**
     * Load view to add new lecture
     */
    public function add(){
        
        $courseId = $this->input->get('courseId');
        $this->checker($courseId);
        $data = array(
            'id' => '',
            'name' => '',
            'date' => '',
        );
        $course = $this->Course->get($courseId);
        $data['course'] = $course;
        $data['courseId'] = $courseId;
        $data['fileList'] = array();
        $data['contentList'] = array();
        $data['homeworkList'] = array();
            
        $this->load->view('teacher/lecture/form',$data);
    }
    /**
     * Load view to see lecture details for edit
     */
    public function edit(){
        
        $id = $this->input->get('id');
        $courseId = $this->input->get('courseId');
        $this->checker($courseId);
        $row = $this->Lecture->get($id);
        if($row){
            $data = array(
                'id' => $row['id'],
                'name' => $row['name'],
                'date' => $row['date'],
            );
            $content_type = '0';
            $homework_type = '1';
            $solution_type = '2';
//            $data['fileList'] = $this->Lecture->get_lecture_file_list($id);
            $contentList = $this->Lecture->get_file_list_by_lecture_id_and_type($id, $content_type);
            $homeworkList = $this->Lecture->get_file_list_by_lecture_id_and_type($id, $homework_type);
            if($homeworkList){
                for($i = 0; $i < count($homeworkList); $i++){
                    $solutionList = $this->Lecture->get_file_list_by_lecture_id_and_file_id($id, $homeworkList[$i]['id']);
                    $homeworkList[$i]['solutionList'] = $solutionList; 
                }
            }
            $course = $this->Course->get($courseId);
            $data['contentList'] = $contentList;
            $data['course'] = $course;
            $data['homeworkList'] = $homeworkList;
            $data['courseId'] = $courseId;
            $this->load->view('teacher/lecture/form',$data);
        } else {
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Id is not present', "error"));
            redirect('teacher/course/lectures?courseId='.$courseId);
        }
    }
    /**
     * Save or update lecture data
     */
    public function save(){
        
        $courseId = $this->input->get('courseId');
        $lectureId = "";
        if($courseId == '' && $courseId == null){
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course is not present', "error"));
            redirect('teacher/courses');
        }
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('date', 'date', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->alertmessage->printResultMessage('All fields are required', "error"));
            if(isset($_POST['id']) && $_POST['id'] == ""){  
                redirect('teacher/course/lectures/add?courseId='.$courseId);
            } else {
                redirect('teacher/course/lectures/edit?courseId='.$courseId.'&id='.$_POST['id']);
            }
        } else {
            if(isset($_POST['id']) && $_POST['id'] == ""){                                                                     
                $lectureId = $this->Lecture->add();
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Lecture added successfully', "success"));
                
            } else {
                $lectureId = $this->input->post('id');
                $this->Lecture->update();
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Lecture updated successfully', "success"));
//                redirect('teacher/course/lectures?courseId='.$courseId);
            }
        }
        
        redirect('teacher/course/lectures/edit?courseId='.$courseId.'&id='.$lectureId);
    }
    /**
     * Save content files details to database
     */
    public function save_file(){
        
        $lectureId = $this->input->post('id');
        $courseId = $this->input->post('course_id');
        $file_name = $this->input->post('file_name');
        $file_type = $this->input->post('file_type');
        if($file_type){
            $file_type = 1;
        } else {
            $file_type = 0;
        }
        //**file upload     
        if (isset($_FILES['userfile']) && $_FILES['userfile']['size'] > 0) {
            if(! (count($_FILES['userfile']['name']) == 1 && $_FILES['userfile']['name'][0] == "")) {
                $files = $_FILES;                
                $cpt = count($_FILES['userfile']['name']);                
                for($i = 0; $i < $cpt; $i++) {

                    $originalFileName = $files['userfile']['name'][$i];
                    $ext = substr($originalFileName, strrpos($originalFileName, ".") + 1);                                
                    $imageFileId = $this->Lecture->insert_file($originalFileName, $file_name, $_FILES['userfile']['type'][$i], $ext, $courseId, $file_type, $lectureId);
                    $_FILES['userfile']['name'] = $imageFileId.".".$ext;
                    $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                    $_FILES['userfile']['size'] = $files['userfile']['size'][$i];    

                    $this->upload->initialize($this->set_upload_options());
                    if($this->upload->do_upload()){
                        $upload_data  = $this->upload->data();
                        $this->Lecture->updateLocation($imageFileId,$upload_data["full_path"]);
                        $this->session->set_flashdata('msg', 
                            $this->alertmessage->printResultMessage("File uploaded successfully", "success"));
                    }   else {
                        $this->Lecture->delete_file_by_id($imageFileId);
                        //echo $this->upload->display_errors();
                         $this->session->set_flashdata('msg', 
                            $this->alertmessage->printResultMessage($this->upload->display_errors(), "error"));
                    }         
                }
            }
        }
        
        redirect('teacher/course/lectures/edit?courseId='.$courseId.'&id='.$lectureId);
    }
    /**
     * Save solution files details uploaded by teacher to database
     */
    public function save_solution_file(){
        
        $lectureId = $this->input->post('lecture_id');
        $courseId = $this->input->post('course_id');
        $file_name = $this->input->post('file_name');
        $file_id = $this->input->post('file_id');
        $file_type = 2;
        
        //**file upload     
        if (isset($_FILES['userfile']) && $_FILES['userfile']['size'] > 0) {
            if(! (count($_FILES['userfile']['name']) == 1 && $_FILES['userfile']['name'][0] == "")) {
                $files = $_FILES;                
                $cpt = count($_FILES['userfile']['name']);                
                for($i = 0; $i < $cpt; $i++) {

                    $originalFileName = $files['userfile']['name'][$i];
                    $ext = substr($originalFileName, strrpos($originalFileName, ".") + 1);                                
                    $imageFileId = $this->Lecture->insert_solution_file($originalFileName, $file_name, $_FILES['userfile']['type'][$i], $ext, $courseId, $file_type, $lectureId, $file_id);
                    $_FILES['userfile']['name'] = $imageFileId.".".$ext;
                    $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                    $_FILES['userfile']['size'] = $files['userfile']['size'][$i];    

                    $this->upload->initialize($this->set_upload_options());
                    if($this->upload->do_upload()){
                        $upload_data  = $this->upload->data();
                        $this->Lecture->updateLocation($imageFileId,$upload_data["full_path"]);
                        $this->session->set_flashdata('msg', 
                            $this->alertmessage->printResultMessage("File uploaded successfully", "success"));
                    }   else {
                        $this->Lecture->delete_file_by_id($imageFileId);
                        //echo $this->upload->display_errors();
                         $this->session->set_flashdata('msg', 
                            $this->alertmessage->printResultMessage($this->upload->display_errors(), "error"));
                    }         
                }
            }
        }
        
        redirect('teacher/course/lectures/edit?courseId='.$courseId.'&id='.$lectureId);
    }
    /**
     * Set location for files to upload
     */
    public function set_upload_options(){   
        $config = array();
        $config['upload_path'] = './uploads/files/courses';
        $config['allowed_types'] = '*';
        $config['max_size']      = '5000KB';
        $config['overwrite']     = TRUE;
        return $config;
    }
    /**
     * Delete the lecture
     */
    public function delete(){
        
        $id = $this->input->get('id');
        $courseId = $this->input->get('courseId');
        $this->checker($courseId);
        $ajaxResult = array();
        if($id == ""){
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Id is not present', "error"));
        } else {
            $this->Lecture->delete();
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Lecture deleted successfully', "success"));
        }
        redirect('teacher/course/lectures?courseId='.$courseId);
    }
    /**
     * Delete the files 
     */
    public function delete_file(){
        $id = $this->input->get('id');
        $course_id = $this->input->get('course_id');
        $lecture_id = $this->input->get('lecture_id');
        $ajaxResult = array();
        if($id == ""){
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Id is not present', "error"));
        } else {
            $this->Course->delete_file();
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Lecture file deleted successfully', "success"));
        }
        redirect('teacher/course/lectures/edit?id='.$lecture_id.'&courseId='.$course_id);
    }
    
}