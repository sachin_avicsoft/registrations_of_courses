<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudentCourseController extends MY_Controller {
    
    public function __construct() {

        parent::__construct();  
        $this->checkLogin();
        $this->checkRoles(array("STUDENT"));
        $this->load->model('Course');
        $this->load->model('Exam');
        $this->load->model('User');
        $this->load->model('Comment');
        $this->load->model('Lecture');
        $this->load->model('StudentCourse');
        $this->load->model('StudentSolution');
        $this->load->library('upload');
    }
    /**
     * Load Course list for student
     */
    public function index(){
        $userdata = $this->session->userdata('logged_in');
        $userId = $userdata['user_id'];
        $courseList = $this->StudentCourse->get_course_list_with_student_courses($userId);
        if($courseList){
            for($i = 0; $i < count($courseList); $i++){
                $date1=date('Y/m/d');
                $tempArr=explode('/', $courseList[$i]['end_date']);
                $tempArr1 = explode('/', $courseList[$i]['start_date']);
                $date2 = date("Y/m/d", mktime(0, 0, 0, $tempArr[1], $tempArr[0], $tempArr[2]));
                $date3 = date("Y/m/d", mktime(0, 0, 0, $tempArr1[1], $tempArr1[0], $tempArr1[2]));
                if($date2 >= $date1 && $date3 <= $date1){
                    $courseList[$i]['is_expired'] = 'active';
                } else {
                    $courseList[$i]['is_expired'] = 'expired';
                }
                
                $registered = $this->StudentCourse->get_student_course_by_user_id_and_course_id($userId, $courseList[$i]['id']);
                if($registered){
                    $courseList[$i]['registered'] = 1;
                } else {
                    $courseList[$i]['registered'] = NULL;
                }
            }
        }
        $data['courseList'] = $courseList;
        $this->load->view('student/course/index',$data);
    }
    /**
     * Change status of course to registered or deregistered 
     */
    public function change_registration_status(){
        $courseId = $this->input->get('id');
        $course = $this->Course->get($courseId);
        $date1=date('Y/m/d');
        $tempArr=explode('/', $course['end_date']);
        $tempArr1 = explode('/', $course['start_date']);
        $date2 = date("Y/m/d", mktime(0, 0, 0, $tempArr[1], $tempArr[0], $tempArr[2]));
        $date3 = date("Y/m/d", mktime(0, 0, 0, $tempArr1[1], $tempArr1[0], $tempArr1[2]));
        if($date2 >= $date1 && $date3 <= $date1){
            
        } else {
            $this->session->set_flashdata('msg', 
                $this->alertmessage->printResultMessage('Not able to change the status now.', "error"));
            redirect('student/courses');
        }
        $status = $this->input->get('status');
        $userdata = $this->session->userdata('logged_in');
        $userId = $userdata['user_id'];
        if($status == 1){
            $status = 0;
            $data = 'deregistered';
            $student_course = $this->StudentCourse->get_student_course_by_user_id_and_course_id($userId, $courseId);
            if($student_course){
                $id = $student_course['id'];
                $this->StudentCourse->change_registration_status($id, $status);
                $this->session->set_flashdata('msg', 
                        $this->alertmessage->printResultMessage("Student $data successfully", "success"));
            }
        } else {
            $status = 1;
            $data = 'registered';
            $this->StudentCourse->add_student_course($courseId, $status, $userId);
            //send mail to student user after registering for particular course
            $user = $this->User->get($userId);
            $course = $this->Course->get($courseId);
            $courseName = $course['name'];
            $username = $user['first_name'].' '.$user['last_name'];
            $to = $user['email'];
            $subject = "Confirmation | Registration of Courses";
            $body= "\r\n\r\n";
            $body.= "Hi $username";
            $body.= "\r\n\r\n";
            $body.="You have successfully registered for the course $courseName.Thank you $to!!!";
            $body.= "\r\n\r\n";
            $from_name = $this->email->smtp_name;
            $from_email = $this->email->smtp_email;
            $this->email->from($from_email, $from_name);
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($body);

//            try {
//                $result = $this->email->send();    
//                $this->session->set_flashdata('msg', 
//                    $this->alertmessage->printResultMessage('Mail sent successfully', "success"));
//            } catch (Exception $e) {
//                $this->session->set_flashdata('msg', 
//                    $this->alertmessage->printResultMessage($e->getMessage(), "error"));
//            } 
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage("Student $data successfully", "success"));
        }
        redirect('student/courses');
    }
    /**
     * Load particular course details for student
     */
    public function edit(){
        
        $courseId = $this->input->get('id');
        $userdata = $this->session->userdata('logged_in');
        $userId = $userdata['user_id'];
        $student_course = $this->StudentCourse->get_student_course_by_user_id_and_course_id($userId, $courseId);
        if($student_course){
            $row = $this->Course->get($courseId);
            if($row && $row['status'] == 'enable'){
                $data = array(
                    'id' => $row['id'],
                    'name' => $row['name'],
                    'description' => $row['description'],
                    'status' => $row['status'],
                );
                $lectureList = $this->Lecture->get_lecture_list_by_courseId($courseId);
                if($lectureList){
                    for($i = 0; $i < count($lectureList); $i++){
                        $fileList = $this->Lecture->get_lecture_file_list($lectureList[$i]['id']);
                        $contentList = $this->Lecture->get_file_list_by_lecture_id_and_type($lectureList[$i]['id'], '0');
                        $homeworkList = $this->Lecture->get_file_list_by_lecture_id_and_type($lectureList[$i]['id'], '1');
                        if($homeworkList){
                            for($j = 0; $j < count($homeworkList); $j++){
                                $teacherSolutionList = $this->Lecture->get_file_list_by_lecture_id_and_file_id($lectureList[$i]['id'], $homeworkList[$j]['id']);
                                $homeworkList[$j]['teacherSolutionList'] = $teacherSolutionList;
                            }
                        }
                        
                        $lectureList[$i]['fileList'] = $fileList;
                        $lectureList[$i]['contentList'] = $contentList;
                        $lectureList[$i]['homeworkList'] = $homeworkList;
                    }
                    
                    for($i = 0; $i < count($lectureList); $i++){
                        $solutionList = $this->StudentSolution->get_lecture_solution_file_list_by_user_id_and_student_course_id($lectureList[$i]['id'], $userId, $student_course['id']);
                        $lectureList[$i]['solutionList'] = $solutionList;
                    }
                }
                $examList = $this->Exam->get_exam_list_by_courseId($courseId);
                $data['examList'] = $examList;
                $data['lectureList'] = $lectureList;
                $data['fileList'] = $this->Course->get_file_list_by_course_id($courseId);
                $data['homeworkFileList'] = $this->Course->get_file_list_by_course_id_and_type($courseId);
                $data['solutionFileList'] = $this->StudentSolution->get_solution_list_by_course_id_and_user_id($courseId, $userId);
                $data['forumList'] = $this->Comment->get_comment_list($courseId);
                $data['courseId'] = $courseId;
                $this->load->view('student/course/form',$data);
            } else {
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage("Course is not present!", "error"));
                redirect('student/courses');
            }
        } else {
            show_404();
        }
    }
    /**
     * Save student solution file
     */
    public function save_file(){
        $userdata = $this->session->userdata('logged_in');
        $userId = $userdata['user_id'];
        $courseId = $this->input->post('course_id');
        $studentCourse = $this->StudentCourse->get_student_course_by_user_id_and_course_id($userId, $courseId);
        if(!$studentCourse){
            $this->session->set_flashdata('msg', 
                            $this->alertmessage->printResultMessage("Please register for this course then upload solution", "error"));
            redirect('student/courses/edit?id='.$courseId);
        }
        $lectureId = $this->input->post('lecture_id');
        $file_id = $this->input->post('file_id');
        //**file upload     
        if (isset($_FILES['userfile']) && $_FILES['userfile']['size'] > 0) {
            if(! (count($_FILES['userfile']['name']) == 1 && $_FILES['userfile']['name'][0] == "")) {
                $files = $_FILES;                
                $cpt = count($_FILES['userfile']['name']);                
                for($i = 0; $i < $cpt; $i++) {

                    $originalFileName = $files['userfile']['name'][$i];
                    $ext = substr($originalFileName, strrpos($originalFileName, ".") + 1);                                
                    $imageFileId = $this->StudentSolution->insert_file($originalFileName, $file_id, $_FILES['userfile']['type'][$i], $ext, $courseId, $userId, $lectureId, $studentCourse['id']);
                    $_FILES['userfile']['name'] = $imageFileId.".".$ext;
                    $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                    $_FILES['userfile']['size'] = $files['userfile']['size'][$i];    

                    $this->upload->initialize($this->set_upload_options());
                    if($this->upload->do_upload()){
                        $upload_data  = $this->upload->data();
                        $this->StudentSolution->updateLocation($imageFileId,$upload_data["full_path"]);
                        $this->session->set_flashdata('msg', 
                            $this->alertmessage->printResultMessage("File uploaded successfully", "success"));
                    }   else {
                        $this->StudentSolution->delete_file_by_id($imageFileId);
                        //echo $this->upload->display_errors();
                         $this->session->set_flashdata('msg', 
                            $this->alertmessage->printResultMessage($this->upload->display_errors(), "error"));
                    }         
                }
            }
        }
        redirect('student/courses/edit?id='.$courseId);
    }
    /**
     * Set upload location for solution file
     */
    public function set_upload_options(){   
        $config = array();
        $config['upload_path'] = './uploads/files/solutions';
        $config['allowed_types'] = '*';
        $config['max_size']      = '5000KB';
        $config['overwrite']     = TRUE;
        return $config;
    }
    
    public function get_homework_file(){
        $lectureId = $this->input->get('id');
        $data = $this->Lecture->get_lecture_homework_file_list($lectureId);
        echo json_encode($data); 
        exit;
    }
    /**
     * Delete solution file
     */
    public function delete_file(){
        $id = $this->input->get('id');
        $courseId = $this->input->get('course_id');
        $ajaxResult = array();
        if($id == ""){
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Id is not present', "error"));
        } else {
            $this->StudentSolution->delete_file();
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('File deleted successfully', "success"));
        }
        redirect('student/courses/edit?id='.$courseId);
    }
    /**
     * Add and view comments for uploaded solution
     */
    public function solutionComments(){
        $userdata = $this->session->userdata('logged_in');
        $userId = $userdata['user_id'];
        $solutionId = $this->input->get('id');
        $solutin = $this->StudentSolution->get($solutionId);
        if(!$solutin){
            show_404();
        }
        $courseId = $solutin['course_id'];
        $studentCourse = $this->StudentCourse->get_student_course_by_user_id_and_course_id($userId, $courseId);
        if($solutin['student_course_id'] != $studentCourse['id']){
            show_404();
        }
        $course = $this->Course->get($courseId);
        $lecture = $this->Lecture->get($solutin['lecture_id']);
        $forumList = $this->StudentSolution->get_student_solution_comment_by_solution_id($solutionId);
        $data['forumList'] = $forumList;
        $data['course'] = $course;
        $data['courseId'] = $courseId;
        $data['solutionId'] = $solutionId;
        $data['lecture'] = $lecture;
        $this->load->view('student/course/solution-comment', $data);
    }
    /**
     * Save comments for solution
     */
    public function saveStudentSolutionComments(){
        $this->form_validation->set_rules('comment', 'comment', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->alertmessage->printResultMessage('Comment is required', "error"));
            redirect('student/solutions/comments?id='.$_POST['id']);
        } else {
            $userdata = $this->session->userdata('logged_in');
            $userId = $userdata['user_id'];
            $solutionId = $this->input->post('id');
            $commentId = $this->StudentSolution->addCommentforStudentSolutionByStudent($userId, $solutionId);
            $this->session->set_flashdata('msg', 
                $this->alertmessage->printResultMessage('Comment added successfully', "success"));
            redirect('student/solutions/comments?id='.$solutionId);
            
        }
        
    }
    
}