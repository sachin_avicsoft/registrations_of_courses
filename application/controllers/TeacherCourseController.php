<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TeacherCourseController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();   
        $this->checkLogin();
        $this->checkRoles(array("TEACHER"));
        $this->getConfigProperties();
        $this->load->model('Course');
        $this->load->model('Properties');
        $this->load->model('Comment');
        $this->load->library('upload');
     }
    /**
     * Load list of courses
     */
    public function index(){
        
        $courseList = $this->Course->get_course_list();
        if(count($courseList) < $this->getMaxCoursesCount()){
            $data['buttonShow'] = TRUE;
        } else {
            $data['buttonShow'] = FALSE;
        }
        $data['courseList'] = $courseList;
        $this->load->view('teacher/course/index',$data);
    }
    /**
     * Load view to add new course
     */
    public function add(){
        $courseList = $this->Course->get_course_list();
        if(count($courseList) == $this->getMaxCoursesCount() || count($courseList) > $this->getMaxCoursesCount()){
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Max limit for courses reached!!', "error"));
                redirect('teacher/courses');
        }
        $data = array(
            'id' => '',
            'name' => '',
            'description' => '',
            'status' => 'enable',
            'start_date' => '',
            'end_date' => '',
        );
        $data['length'] = 240;
        $data['forumList'] = array();
        $this->load->view('teacher/course/form',$data);
    }
    /**
     * Load course details for edit
     */
    public function edit(){
        
        $id = $this->input->get('id');
        $row = $this->Course->get($id);
        if($row){
            $data = array(
                'id' => $row['id'],
                'name' => $row['name'],
                'description' => $row['description'],
                'status' => $row['status'],
                'start_date' => $row['start_date'],
                'end_date' => $row['end_date'],
            );
            $length = strlen($row['description']); 
            $data['length'] = 240 - $length;
            $data['fileList'] = $this->Course->get_file_list_by_course_id($id);
            $content_type = 0;
            $homework_type = 1;
            $content_type = 2;
            $data['contentList'] = $this->Course->get_file_list_by_course_id_and_type($id, $content_type);
            $data['homeworkList'] = $this->Course->get_file_list_by_course_id_and_type($id, $homework_type);
            $data['forumList'] = $this->Comment->get_comment_list($id);
            $this->load->view('teacher/course/form',$data);
        } else {
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Id is not present', "error"));
            redirect('teacher/courses');
        }
        
    }
    /**
     * Save or update the course data
     */
    public function save(){
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->alertmessage->printResultMessage('All fields are required', "error"));
            if(isset($_POST['id']) && $_POST['id'] == ""){  
                redirect('teacher/courses/add');
            } else {
                redirect('teacher/courses/edit?id='.$_POST['id']);
            }
        } else {         
            if(isset($_POST['id']) && $_POST['id'] == ""){   
                $courseList = $this->Course->get_course_list();
                if(count($courseList) == $this->getMaxCoursesCount() || count($courseList) > $this->getMaxCoursesCount()){
                    $this->session->set_flashdata('msg', 
                            $this->alertmessage->printResultMessage('Max limit for courses reached!!', "error"));
                        redirect('teacher/courses');
                }
                $courseId = $this->Course->add();
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course added successfully', "success"));
                redirect('teacher/courses/edit?id='.$courseId);
            } else {
                $courseId = $this->input->post('id');
                $this->Course->update();
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course updated successfully', "success"));
                redirect('teacher/courses');
            }
            
        }
    }
    /**
     * Change status of course to enable or disable
     */
    public function change_status(){
        $id = $this->input->get('id');
        $status = $this->input->get('status');
        if($status == 1){
            $status = 'disable';
        } else {
            $status = 'enable';
        }
        if($id == ""){
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Id is not present', "error"));
        } else {
            $this->Course->change_status($id, $status);
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course status change successfully', "success"));
        }
        redirect('teacher/courses');
    }
    /**
     * Delete the course 
     */
    public function delete(){
        $id = $this->input->get('id');
        $ajaxResult = array();
        if($id == ""){
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Id is not present', "error"));
        } else {
            $this->Course->delete();
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course deleted successfully', "success"));
        }
        redirect('teacher/courses');
    }
    
}