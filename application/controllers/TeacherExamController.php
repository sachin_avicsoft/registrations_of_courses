<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TeacherExamController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();      
        $this->checkLogin();
        $this->checkRoles(array("TEACHER"));
        $this->getConfigProperties();
        $this->load->model('Course');
        $this->load->model('Exam');
        $this->load->library('upload');
     }
     /**
      * Check for particular exam is related to given course
      */
    public function checker($courseId){
        
        if($courseId == '' && $courseId == null){
            
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course is not present', "error"));
            redirect('teacher/courses');
        } else {
            $course = $this->Course->get($courseId);
            if(!$course){
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course is not present', "error"));
                redirect('teacher/courses');
            } else if($course['status'] != 'enable'){
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course is not enable', "error"));
                redirect('teacher/courses');
            }
        }
    } 
    /**
     * Load list of exams
     */
    public function index(){
        
        $courseId = $this->input->get('courseId');
        $this->checker($courseId);
        $course = $this->Course->get($courseId);
        $examList = $this->Exam->get_exam_list_by_courseId($courseId);
        if(count($examList) < $this->getMaxExamsCount()){
            $data['buttonShow'] = TRUE;
        } else {
            $data['buttonShow'] = FALSE;
        }
        $data['course'] = $course;
        $data['examList'] = $examList;
        $data['courseId'] = $courseId;
        
        $this->load->view('teacher/exam/index',$data);
    }
    /**
     * Load view to add new exam
     */
    public function add(){
        $courseId = $this->input->get('courseId');
        $this->checker($courseId);
        
        $examList = $this->Exam->get_exam_list_by_courseId($courseId);
        if(count($examList) == $this->getMaxExamsCount() || count($examList) > $this->getMaxExamsCount()){
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Max limit for exams reached!!', "error"));
                redirect('teacher/course/exams?courseId='.$courseId);
        }
        $data = array(
            'id' => '',
            'name' => '',
            'start_date' => '',
            'end_date' => '',
            'time_limit' => '',
            'status' => 'enable',
        );
        $course = $this->Course->get($courseId);
        $data['course'] = $course;
        $data['courseId'] = $courseId;
        $data['course'] = $course;
        $this->load->view('teacher/exam/form',$data);
    }
    /**
     * Load view to show particular exam details for edit
     */
    public function edit(){
        
        $id = $this->input->get('id');
        $courseId = $this->input->get('courseId');
        $this->checker($courseId);
        $row = $this->Exam->get($id);
        if($row){
            $data = array(
                'id' => $row['id'],
                'name' => $row['name'],
                'start_date' => $row['start_date'],
                'end_date' => $row['end_date'],
                'time_limit' => $row['time_limit'],
                'status' => $row['status'],
            );
            $course = $this->Course->get($courseId);
            $data['course'] = $course;
            $data['courseId'] = $courseId;
            $data['course'] = $course;
            $this->load->view('teacher/exam/form',$data);
        } else {
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Id is not present', "error"));
            redirect('teacher/course/exams?courseId='.$courseId);
        }
        
    }
    /**
     * Save or update exam data
     */
    public function save(){
        $courseId = $this->input->get('courseId');
        if($courseId == '' && $courseId == null){
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Course is not present', "error"));
            redirect('teacher/courses');
        }
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('start_date', 'start_date', 'required');
        $this->form_validation->set_rules('end_date', 'end_date', 'required');
        $this->form_validation->set_rules('time_limit', 'time_limit', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->alertmessage->printResultMessage('All fields are required', "error"));
            if(isset($_POST['id']) && $_POST['id'] == ""){  
                redirect('teacher/course/exams/add?courseId='.$courseId);
            } else {
                redirect('teacher/course/exams/edit?courseId='.$courseId.'&id='.$_POST['id']);
            }
        } else {
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');
            
            if(isset($_POST['id']) && $_POST['id'] == ""){
                
                $examList = $this->Exam->get_exam_list_by_courseId($courseId);
                if(count($examList) == $this->getMaxExamsCount() || count($examList) > $this->getMaxExamsCount()){
                    $this->session->set_flashdata('msg', 
                            $this->alertmessage->printResultMessage('Max limit for exams reached!!', "error"));
                        redirect('teacher/course/exams?courseId='.$courseId);
                }
                
                $examId = $this->Exam->add();
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Exam added successfully', "success"));
                redirect('teacher/course/exams/edit?courseId='.$courseId.'&id='.$examId);
            } else {
                $examId = $this->input->post('id');
                $this->Exam->update();
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Exam updated successfully', "success"));
                redirect('teacher/course/exams?courseId='.$courseId);
            }
        }
    }
    /**
     * Delete the exam
     */
    public function delete(){
        $id = $this->input->get('id');
        $courseId = $this->input->get('courseId');
        $this->checker($courseId);
        $ajaxResult = array();
        if($id == ""){
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Id is not present', "error"));
        } else {
            $this->Exam->delete();
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Exam deleted successfully', "success"));
        }
        redirect('teacher/course/exams?courseId='.$courseId);
    }
    /**
     * Change exam status to enable or disable
     */
    public function change_status(){
        
        $id = $this->input->get('id');
        $courseId = $this->input->get('courseId');
        $status = $this->input->get('status');
        if($status == 1){
            $status = 'disable';
        } else {
            $status = 'enable';
        }
        if($id == ""){
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Id is not present', "error"));
        } else {
            $this->Exam->change_status($id, $status);
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Exam status change successfully', "success"));
        }
        redirect('teacher/course/exams?courseId='.$courseId);
    }
    
}