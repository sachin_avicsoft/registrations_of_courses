<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CommentController extends MY_Controller {
    
    public function __construct() {

       parent::__construct();   
       $this->checkLogin();
       $this->checkRoles(array("TEACHER","ADMIN","STUDENT"));
       $this->load->model('Course');
       $this->load->model('Comment');
       $this->load->model('StudentCourse');
       $this->load->library('upload');
    }
    /**
     * Load view for course comments
     */
    public function comments(){
        $courseId = $this->input->get('id');
        $course = $this->Course->get($courseId);
        $userdata = $this->session->userdata('logged_in');
        $userId = $userdata['user_id'];
        $student_course = $this->StudentCourse->get_student_course_by_user_id_and_course_id($userId, $courseId);
        if(!$student_course){
            show_404();
        }
        $data['forumList'] = $this->Comment->get_comment_list($courseId);
        $data['course'] = $course;
        $data['courseId'] = $courseId;
        $this->load->view('student/course/comment',$data);
    }
    /**
     * Save comment to database from student user
     */
    public function save_student_comment(){
        $this->form_validation->set_rules('comment', 'comment', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->alertmessage->printResultMessage('Comment is required', "error"));
            if(isset($_POST['id']) && $_POST['id'] == ""){  
                redirect('student/course/comments?id='.$_POST['id']);
            } else {
                redirect('student/course/comments?id='.$_POST['id']);
            }
        } else {         
            
            $userdata = $this->session->userdata('logged_in');
            $userId = $userdata['user_id'];
            
            if(isset($_POST['id']) && $_POST['id'] == ""){                                                                     
                $commentId = $this->Comment->add($userId);
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Comment added successfully', "success"));
                
            } else {
                $commentId = $this->Comment->add($userId);
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Comment added successfully', "success"));
            }
            redirect('student/course/comments?id='.$_POST['id']);
        }
    }
    /**
     * save comment to database from teacher user
     */
    public function save_teacher_comment(){
        $this->form_validation->set_rules('comment', 'comment', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->alertmessage->printResultMessage('Comment is required', "error"));
            if(isset($_POST['id']) && $_POST['id'] == ""){  
                redirect('teacher/courses/edit?id='.$_POST['id']);
            } else {
                redirect('teacher/courses/edit?id='.$_POST['id']);
            }
        } else {         
            
            $userdata = $this->session->userdata('logged_in');
            $userId = $userdata['user_id'];
            
            if(isset($_POST['id']) && $_POST['id'] == ""){                                                                     
                $commentId = $this->Comment->add($userId);
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Comment added successfully', "success"));
                
            } else {
                $commentId = $this->Comment->add($userId);
                $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Comment added successfully', "success"));
            }
            redirect('teacher/courses/edit?id='.$_POST['id']);
        }
    }
}