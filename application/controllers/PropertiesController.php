<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PropertiesController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();   
        $this->checkLogin();
        $this->checkRoles(array("ADMIN","TEACHER"));
        $this->load->model('Properties');
     }
    /**
     * Load form to set configurations for project by admin user
     */
    public function index(){
        $row = $this->Properties->get();
        if($row){
            $data = array(
                'id' => $row['id'],
                'no_of_courses' => $row['no_of_courses'],
                'no_of_exams_per_course' => $row['no_of_exams_per_course'],
                'no_of_questions_per_exam' => $row['no_of_questions_per_exam'],
            );
        } else {
            $data = array(
                'id' => '',
                'no_of_courses' => '',
                'no_of_exams_per_course' => '',
                'no_of_questions_per_exam' => '',
            );
        }
        $this->load->view('admin/properties',$data);
    }
    /**
     * Load form to set configurations for project by teacher user
     */
    public function master(){
        $row = $this->Properties->get();
        if($row){
            $data = array(
                'id' => $row['id'],
                'no_of_courses' => $row['no_of_courses'],
                'no_of_exams_per_course' => $row['no_of_exams_per_course'],
                'no_of_questions_per_exam' => $row['no_of_questions_per_exam'],
            );
        } else {
            $data = array(
                'id' => '',
                'no_of_courses' => '',
                'no_of_exams_per_course' => '',
                'no_of_questions_per_exam' => '',
            );
        }
        $this->load->view('teacher/master/properties',$data);
    }
    /**
     * Save config data to database
     */
    public function save(){
        $this->form_validation->set_rules('no_of_courses', 'no_of_courses', 'required');
        $this->form_validation->set_rules('no_of_exams_per_course', 'no_of_exams_per_course', 'required');
        $this->form_validation->set_rules('no_of_questions_per_exam', 'no_of_questions_per_exam', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->alertmessage->printResultMessage('All fields are required', "error"));
            redirect('properties');
        } else {  
            if(isset($_POST['id']) && $_POST['id'] != ''){
                $this->Properties->update();
            } else {
                $this->Properties->add();
            }
            
            $this->session->set_flashdata('msg', 
                $this->alertmessage->printResultMessage('Properties added successfully', "success"));
            redirect('properties');
        }
    }
    /**
     * Save config data to database by teacher user
     */
    public function savemaster(){
        $this->form_validation->set_rules('no_of_courses', 'no_of_courses', 'required');
        $this->form_validation->set_rules('no_of_exams_per_course', 'no_of_exams_per_course', 'required');
        $this->form_validation->set_rules('no_of_questions_per_exam', 'no_of_questions_per_exam', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->alertmessage->printResultMessage('All fields are required', "error"));
            redirect('properties');
        } else {  
            if(isset($_POST['id']) && $_POST['id'] != ''){
                $this->Properties->update();
            } else {
                $this->Properties->add();
            }
            
            $this->session->set_flashdata('msg', 
                $this->alertmessage->printResultMessage('Properties added successfully', "success"));
            redirect('teacher/properties');
        }
    }
    
}