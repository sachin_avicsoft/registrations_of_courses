<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConfigController extends CI_Controller {
    
    public function __construct() {

       parent::__construct();   

//            $this->load->model('User');

    }
    
    /**
     * Load view for create database file for new installation
     */
    public function index(){
        $path = 'config.php';
        $file = file_exists($path);
        if($file == 1){
            $this->load->view('config_file');
        } else {
            redirect();
        }
        
    }
    
    /**
     * Check given credentials are correct or not
     */
    public function checkConnection(){
        
        $host = 'localhost';
        $user = $this->input->get('username');
        $password = $this->input->get('password');
        $database = $this->input->get('database');
        $dbdriver = $this->input->get('dbdriver');
       if($dbdriver == 'postgre') {
           $db = pg_connect("host=$host  dbname=$database user=$user password=$password") or die("0");            
       } else {
           $db = mysqli_connect($host, $user, $password, $database);
       }
        if ($db){
            echo "1";
            exit;
        } else {
            echo "0";
            exit;
        }
    }
    /**
     * Update database.php file from config folder for new project installation
     */
    public function updateDatabaseFile(){
        $path = 'config.php';
        $file = file_exists($path);
        if($file != 1){
            redirect();
        }
        
//        $data['username'] = $this->input->post('username');
//        $data['password'] = $this->input->post('password');
//        $data['database'] = $this->input->post('database');
//        $data['dbdriver'] = $this->input->post('dbdriver');
//        $DBConfig = $this->load->view('database1', $data, TRUE);
        $this->load->helper('file');
        
  /*      $filename = APPPATH.'config/database.php';
        if (is_writable($filename)) {
            if (!$handle = fopen($filename, 'w+')) {
                 echo "Cannot open file ($filename)";
                 exit;
            }
            
            if (fwrite($handle, $DBConfig) === FALSE) {
                echo "Cannot write to file ($filename)";
                exit;
            }

            fclose($handle); */
            $this->load->database();
            //run sql file
            $hostname = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $database = $this->db->database;
            $dbdriver = $this->db->dbdriver;
            if($dbdriver == 'postgre'){
                $conn = pg_connect("host=$hostname  dbname=$database user=$username password=$password");
            } else {
                $conn =new mysqli($hostname, $username, $password , $database);
            }
            
            $query = '';
            if($dbdriver == 'postgre'){
                $sqlScript = file('postgresqldata.sql');
            } else {
                $sqlScript = file('mysqldata.sql');
            }
            
            foreach ($sqlScript as $line)    {

                    $startWith = substr(trim($line), 0 ,2);
                    $endWith = substr(trim($line), -1 ,1);

                    if (empty($line) || $startWith == '--' || $startWith == '/*' || $startWith == '//') {
                            continue;
                    }

                    $query = $query . $line;
                    if ($endWith == ';') {
                            if($dbdriver == 'postgre'){
                                pg_exec($conn, $query) or die('<div class="error-response sql-import-response">Problem in executing the SQL query <b>' . $query. '</b></div>');
                            } else {
                                mysqli_query($conn,$query) or die('<div class="error-response sql-import-response">Problem in executing the SQL query <b>' . $query. '</b></div>');
                            }
                            $query= '';        
                    }
            }
            
            //CREATE ADMIN LOGIN
            redirect('create-admin');
            
//        } else {
//            echo "The file $filename is not writable";
//        }
    }
    
    /**
     * Load view to create admin for new installation
     */
    public function createAdmin(){
        $path = 'config.php';
        $file = file_exists($path);
        if($file == 1){
            $this->load->view('admin-form');
        } else {
            redirect();
        }
        
    }
    /**
     * Add admin details to database 
     */
    public function create_admin_user(){
        $this->load->model('User');
        $this->form_validation->set_rules('first_name', 'first_name', 'required');
        $this->form_validation->set_rules('last_name', 'last_name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('contact_number', 'contact_number', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->alertmessage->printResultMessage('All fields are required', "error"));
            redirect('create-admin');
        } else {         
            $userId = $this->User->add();
            
            $path = 'config.php';
            if (!unlink($path))
            {
                echo ("Error deleting $path");
                exit;
            }
            
            $this->session->set_flashdata('msg', 
                $this->alertmessage->printResultMessage('Admin information saved successfully', "success"));
            redirect();
        }
    }
    
    /**
     * Update admin information 
     */
    public function updateAdminUser(){
        $username = $this->input->post('username');
        $user = $this->User->get_user_by_email($username);
        if($user){
            $this->User->update_user_to_admin($user['id']);
        } else {
            $id = 1;
            $this->User->update_user_to_admin($id);
        }
        
        $path = 'config.php';
        if (!unlink($path))
        {
        echo ("Error deleting $path");
        exit;
        }
        redirect();
    }
    
}
