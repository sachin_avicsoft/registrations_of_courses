<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CommonController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();   
     }
    
     /**
      * After admin login load Admin Dashboard view
      */
    public function admin_dashboard(){
        $this->checkLogin();
        $this->checkRoles(array("ADMIN"));
        
        $this->load->view('admin/dashboard/index');
    }
    /**
     * After teacher login load Teacher Dashboard view
     */
    public function teacher_dashboard(){
        $this->checkLogin();
        $this->checkRoles(array("TEACHER"));
        
        $this->load->view('teacher/dashboard/index');
    }
    /**
     * After student login load Student Dashboard view
     */
    public function student_dashboard(){
        $this->checkLogin();
        $this->checkRoles(array("STUDENT"));
        
        $this->load->view('student/dashboard/index');
    }
}