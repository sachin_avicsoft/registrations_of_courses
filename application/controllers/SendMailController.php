<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SendMailController extends MY_Controller {
    
    public function __construct() {
        
        parent::__construct();   
        $this->checkLogin();
        $this->checkRoles(array("TEACHER"));
        $this->load->model('StudentCourse');
        $this->load->model('User');
        $this->load->library('upload');
    }
    /**
     * Send email to all students who are registered to particular course
     */
    public function index(){
        $courseId = $this->input->post('course_id');
        $massage = $this->input->post('message');
        $studentList = $this->StudentCourse->get_student_list_by_course($courseId);
        if($studentList){
            foreach ($studentList as $key => $value) {
                $to = $value['email'];
                $subject = "Registration of Courses";
                $body= "\r\n\r\n";
                $body.= $massage;
                $body.= "\r\n\r\n";
                $from_name = $this->email->smtp_name;
                $from_email = $this->email->smtp_email;
                $this->email->from($from_email, $from_name);
                $this->email->to($to);
                $this->email->subject($subject);
                $this->email->message($body);

                try {
                    $result = $this->email->send();    
                    $this->session->set_flashdata('msg', 
                        $this->alertmessage->printResultMessage('Mail sent successfully', "success"));
                } catch (Exception $e) {
                    $this->session->set_flashdata('msg', 
                        $this->alertmessage->printResultMessage($e->getMessage(), "error"));
                }  
            }
        } else {
            $this->session->set_flashdata('msg', 
                    $this->alertmessage->printResultMessage('Mail not sent,No students registered!!', "success"));
        }
//        $this->session->set_flashdata('msg', 
//                    $this->alertmessage->printResultMessage('Mail sent successfully', "success"));
        redirect('teacher/courses');

    }
    
}