<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {
    
    public function __construct() {
        parent::__construct();      
    }

    /**
     * Load login page and after login redirect to particular users dashboard
     */
    public function index()
    {
        $path = 'config.php';
        $file = file_exists($path);
        if($file == 1){
            redirect('configurations');
        }
        $this->load->model('User');
        $this->load->database();
        if(isset($_POST['action']) && $_POST['action'] == "login"){

            $email = $this->input->post("email");
            $password = $this->input->post("password");

            $this->form_validation->set_rules("email", "email", "trim|required");
            $this->form_validation->set_rules("password", "password", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                 $this->load->view('login');
            } else {

                $usr_result = $this->User->get_entry($email, $password);			
                if($usr_result) {
                    $username =  $usr_result['first_name'].' '.$usr_result['last_name']; 				 
                    $sessiondata = array(
                        'user_id' => $usr_result['id'],
                        'email' => $email,
                        'username' => $username,
                        'role' => $usr_result['role'],
                        'loginuser' => TRUE
                    );
                    $this->session->set_userdata('logged_in', $sessiondata);

                    if($usr_result['role'] == "ADMIN"){
                           redirect('admin/dashboard');
                    } else if($usr_result['role'] == "TEACHER"){
                            redirect("teacher/dashboard");
                    } else if($usr_result['role'] == "STUDENT") {
                           redirect("student/dashboard");
                    } else {
                        redirect();
                    }
                } else {
                    $this->session->set_flashdata('msg', 
                        $this->alertmessage->printResultMessage('Invalid Credentials!', "error"));
                    redirect();				 
                }			
            }
        } else {		
            $this->load->view('login');
        }
    }
    /**
     * Sign out loged in user
     */
    public function logout(){
        $this->session->unset_userdata('logged_in');       
        redirect('', 'refresh');		
    }   
}
