<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FileController extends MY_Controller {
    
    public function __construct() {

        parent::__construct();  
        $this->checkLogin();
        $this->checkRoles(array("STUDENT","ADMIN","TEACHER"));
        $this->load->model('Course');
        $this->load->model('File');
        $this->load->model('Exam');
        $this->load->model('Comment');
        $this->load->model('Lecture');
        $this->load->model('StudentCourse');
        $this->load->model('StudentSolution');
        $this->load->library('upload');
    }
    /**
     * Download uploaded files from teacher and student
     */
    public function file(){
        
        $fileId = $this->input->get('id');
        $type = $this->input->get('type');
        $userdata = $this->session->userdata('logged_in');
        $userId = $userdata['user_id'];
        $role = $userdata['role'];
        
        if($role == 'STUDENT'){
            if($type == "solution"){
                $file = $this->File->get_student_solution_file($fileId, $userId);
                if($file){
                    
                    $homeworkFile = $this->File->get_course_file($file['file_id']);
//                    $filename = $homeworkFile['file_name'];
//                    $ext = $file['ext'];
                    $file_name = $homeworkFile['file_name'].'.'.$file['ext'];
                    $path = $file['location'];
                    
                    if(is_file($path)) {
                       // required for IE
                       if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

                       // get the file mime type using the file extension
                       $this->load->helper('file');

                       $mime = get_mime_by_extension($path);

                       // Build the headers to push out the file properly.
                       header('Pragma: public');     // required
                       header('Expires: 0');         // no cache
                       header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                       header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
                       header('Cache-Control: private',false);
                       header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
                       header('Content-Disposition: attachment; filename="'.basename($file_name).'"');  // Add the file name
                       header('Content-Transfer-Encoding: binary');
                       header('Content-Length: '.filesize($path)); // provide file size
                       header('Connection: close');
                       readfile($path); // push it out
                       exit();
                    
                    } else {
                        show_404();
//                        echo 'No path present';
                    }
                } else {
                    show_404();
                }
            } else if($type == "course"){ 
                $file = $this->File->get_course_file($fileId);
                if($file){
                    $courseId = $file['course_id'];
                    $studentCourse = $this->StudentCourse->get_student_course_by_user_id_and_course_id($userId, $courseId);
                    if($studentCourse){
                        $file_name = $file['file_name'].'.'.$file['ext'];
                        $path = $file['location'];
                        
                        if(is_file($path)) {
                           // required for IE
                           if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

                           // get the file mime type using the file extension
                           $this->load->helper('file');

                           $mime = get_mime_by_extension($path);

                           // Build the headers to push out the file properly.
                           header('Pragma: public');     // required
                           header('Expires: 0');         // no cache
                           header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                           header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
                           header('Cache-Control: private',false);
                           header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
                           header('Content-Disposition: attachment; filename="'.basename($file_name).'"');  // Add the file name
                           header('Content-Transfer-Encoding: binary');
                           header('Content-Length: '.filesize($path)); // provide file size
                           header('Connection: close');
                           readfile($path); // push it out
                           exit();

                        } else {
                            show_404();
//                            echo 'No path present';
                        }
                    } else {
                        show_404();
//                        echo 'No course present';
                    }
                } else {
                    show_404();
//                    echo 'No file present';
                }
            }
        } else if($role == 'TEACHER' || $role == 'ADMIN'){
            if($type == "solution"){
                $file = $this->File->get_student_solution_file_by_id($fileId);
                if($file){
                    $file_name = $file['file_name'].'.'.$file['ext'];
                    $path = $file['location'];
                    if(is_file($path)) {
                           // required for IE
                           if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

                           // get the file mime type using the file extension
                           $this->load->helper('file');

                           $mime = get_mime_by_extension($path);

                           // Build the headers to push out the file properly.
                           header('Pragma: public');     // required
                           header('Expires: 0');         // no cache
                           header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                           header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
                           header('Cache-Control: private',false);
                           header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
                           header('Content-Disposition: attachment; filename="'.basename($file_name).'"');  // Add the file name
                           header('Content-Transfer-Encoding: binary');
                           header('Content-Length: '.filesize($path)); // provide file size
                           header('Connection: close');
                           readfile($path); // push it out
                           exit();

                        } else {
                            show_404();
//                            echo 'No path 1 present';
                        }
                } else {
                    show_404();
//                    echo 'No file 1 present';
                }
            } else if($type == "course"){
                $file = $this->File->get_course_file($fileId);
                if($file){
                    $file_name = $file['file_name'].'.'.$file['ext'];
                    $path = $file['location'];
                    if(is_file($path)) {
                        // required for IE
                        if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

                        // get the file mime type using the file extension
                        $this->load->helper('file');

                        $mime = get_mime_by_extension($path);

                        // Build the headers to push out the file properly.
                        header('Pragma: public');     // required
                        header('Expires: 0');         // no cache
                        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                        header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
                        header('Cache-Control: private',false);
                        header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
                        header('Content-Disposition: attachment; filename="'.basename($file_name).'"');  // Add the file name
                        header('Content-Transfer-Encoding: binary');
                        header('Content-Length: '.filesize($path)); // provide file size
                        header('Connection: close');
                        readfile($path); // push it out
                        exit();

                    } else {
                        show_404();
//                        echo 'Path is not present';
                    }
                } else {
                    show_404();
//                    echo 'File is not present';
                }
            } else {
                show_404();
//                echo 'Type is not present';
            }
        } else {
            show_404();
//            echo 'Role is not present';
        }      
    }
}