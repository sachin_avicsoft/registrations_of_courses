<?php if( !defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

function resource_url(){
   return base_url().'resources/';
}

function print_resource_url($path){
   echo base_url().'resources/'.$path;
}

function printResultMessage($that){
    
     if($that->session->flashdata('msg')) {
         $msg =  $that->session->flashdata('msg'); 
         
         var_dump($msg);
     }
}


function toSlug($title) {
    $slug =  mb_strtolower(url_title(($title)));
    return $slug;
}

/*
|--------------------------------------------------------------------------
| Static Resource Library
|--------------------------------------------------------------------------
|
| Some simple code to help manage/serve static
| files efficiently.
| Refer : https://github.com/erwaller/static-resource-helper-for-codeigniter
*/

?>