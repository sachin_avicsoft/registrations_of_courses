<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'LoginController';
$route['login'] = 'LoginController';
$route['logout'] = 'LoginController/logout';

$route['configurations'] = 'ConfigController';
$route['connection-check'] = 'ConfigController/checkConnection';
$route['update_database'] = 'ConfigController/updateDatabaseFile';
$route['create-admin/save'] = 'ConfigController/create_admin_user';//updateAdminUser';
$route['create-admin'] = 'ConfigController/createAdmin';

//admin
$route['admin/dashboard'] = 'CommonController/admin_dashboard';
$route['properties'] = 'PropertiesController';
$route['properties/save'] = 'PropertiesController/save';
//teacher
$route['teacher/dashboard'] = 'CommonController/teacher_dashboard';
$route['teacher/course/send-mail'] = 'SendMailController';
//master
$route['teacher/properties'] = 'PropertiesController/master';
$route['teacher/properties/save'] = 'PropertiesController/savemaster';
//courses
$route['teacher/courses'] = 'TeacherCourseController';
$route['teacher/courses/add'] = 'TeacherCourseController/add';
$route['teacher/courses/edit'] = 'TeacherCourseController/edit';
$route['teacher/courses/save'] = 'TeacherCourseController/save';
$route['teacher/courses/delete'] = 'TeacherCourseController/delete';
$route['teacher/courses/change-status'] = 'TeacherCourseController/change_status';
$route['teacher/courses/save/file'] = 'TeacherCourseController/save_file';
$route['teacher/courses/delete/file'] = 'TeacherCourseController/delete_file';

//lectures
$route['teacher/course/lectures'] = 'TeacherLectureController';
$route['teacher/course/lectures/add'] = 'TeacherLectureController/add';
$route['teacher/course/lectures/edit'] = 'TeacherLectureController/edit';
$route['teacher/course/lectures/delete'] = 'TeacherLectureController/delete';
$route['teacher/course/lectures/save'] = 'TeacherLectureController/save';
$route['teacher/course/lectures/delete/file'] = 'TeacherLectureController/delete_file';
$route['teacher/course/lectures/save/file'] = 'TeacherLectureController/save_file';
$route['teacher/course/lectures/save/solution-file'] = 'TeacherLectureController/save_solution_file';

//exams
$route['teacher/course/exams'] = 'TeacherExamController';
$route['teacher/course/exams/add'] = 'TeacherExamController/add';
$route['teacher/course/exams/edit'] = 'TeacherExamController/edit';
$route['teacher/course/exams/delete'] = 'TeacherExamController/delete';
$route['teacher/course/exams/save'] = 'TeacherExamController/save';
$route['teacher/course/exams/change-status'] = 'TeacherExamController/change_status';

//questions
$route['teacher/course/exam/questions'] = 'TeacherQuestionController';
$route['teacher/course/exam/questions/add'] = 'TeacherQuestionController/add';
$route['teacher/course/exam/questions/edit'] = 'TeacherQuestionController/edit';
$route['teacher/course/exam/questions/delete'] = 'TeacherQuestionController/delete';
$route['teacher/course/exam/questions/save'] = 'TeacherQuestionController/save';
$route['teacher/course/exam/questions/change-status'] = 'TeacherQuestionController/change_status';

$route['teacher/course/comments/save'] = 'CommentController/save_teacher_comment';

//teacher students
$route['result/save'] = 'TeacherStudentController/saveResult';
$route['teacher/students'] = 'TeacherStudentController';
$route['teacher/students/marks'] = 'TeacherStudentController/studentAnswerMarks';

$route['teacher/students/exams'] = 'TeacherStudentController/exams';
$route['teacher/students/exams/answers'] = 'TeacherStudentController/getStudentExamAnswers';

$route['teacher/students/solutions'] = 'TeacherStudentController/solutions';
$route['teacher/students/solutions/comments'] = 'TeacherStudentController/getCommentOnSolution';
$route['teacher/students/solutions/comments/save'] = 'TeacherStudentController/addCommentOnSolution';
$route['teacher/courses/students/solutions'] = 'TeacherStudentController/allstudentsSolutionsSummary';
        
$route['teacher/courses/students/deregistered'] = 'TeacherStudentController/deregistered_students';

//student
$route['student/dashboard'] = 'CommonController/student_dashboard';

//exam
$route['student/course/exams'] = 'StudentExamController';
$route['student/course/exam-list'] = 'StudentExamController/examList';
$route['student/course/exams/start'] = 'StudentExamController/start';
$route['student/course/exams/answer'] = 'StudentExamController/submit_answer';
$route['student/course/exams/submit'] = 'StudentExamController/submit';

//courses
$route['student/courses'] = 'StudentCourseController';
$route['student/courses/edit'] = 'StudentCourseController/edit';
$route['student/courses/change-registration-status'] = 'StudentCourseController/change_registration_status';
$route['student/course/lectures/save/file'] = 'StudentCourseController/save_file';
$route['student/course/lectures/get-file'] = 'StudentCourseController/get_homework_file';
$route['student/course/lectures/delete/file'] = 'StudentCourseController/delete_file';

$route['student/solutions/comments'] = 'StudentCourseController/solutionComments';
$route['student/solutions/comments/save'] = 'StudentCourseController/saveStudentSolutionComments';

//comment
$route['student/course/comments'] = 'CommentController/comments';
$route['student/course/comments/save'] = 'CommentController/save_student_comment';

//file
$route['get/files'] = "FileController/file";

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
