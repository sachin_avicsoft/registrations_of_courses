
Project Registrations :

We are using the codeigniter framework for this project you can find the details at https://codeigniter.com/    


Following are the configuration file you may need to change as per your set up

Under directory, /application/config 

1. config.php file : General configuration

Path for file : registrations/application/config/config.php

Base Url of the project, change base_url to required url e.g. 'https://www.tu-chemnitz.de'

$config['base_url'] = 'http://localhost/registrations';


2. database.php file : Database configuration

Path for file : registrations/application/config/database.php

You can define the database username, password, database name and the database driver to be use. Change the values as per your db setup.

'username' => 'username', => database username
'password' => 'password', => user password
'database' => 'database_name', =>database name

'dbdriver' => 'postgre' => for postgresql database
 
OR 

'dbdriver' => 'mysqli' => for mysql database,


3. email.php file : Email set up

Path for file : registrations/application/config/email.php

You can define the smtp mail server setting for sending the mail.


$config['smtp_host'] = 'smtp host name'; e.g.'ssl://in-v3.mailjet.com'
$config['smtp_user'] = 'smtp user name';
$config['smtp_pass'] = 'smtp password';
$config['smtp_email'] = '';set the smtp_email for email from email e.g. 'test@test.com', this email must be registered with smtp server
$config['smtp_name'] = ''; set the smtp_name for email from name e.g. 'TU CHEMINTZ'
