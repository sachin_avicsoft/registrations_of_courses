CREATE TABLE IF NOT EXISTS users (
  id bigint NOT NULL ,
  first_name character varying(100) DEFAULT NULL,
  last_name character varying(100) DEFAULT NULL,
  email character varying(100) NOT NULL,
--   password character varying(255) DEFAULT NULL,
--   contact_number character varying(45) DEFAULT NULL,
  username character varying(255) DEFAULT NULL,
  org_unit_number character varying(255) DEFAULT NULL,
  role character varying(45) NOT NULL,
  active bigint DEFAULT '1',
  "createdAt" timestamp NULL DEFAULT NULL,
  "updatedAt" timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id),
--   CONSTRAINT username UNIQUE (username)
);

CREATE SEQUENCE registrations_users_id START 1;
ALTER TABLE users ALTER id SET default nextval('registrations_users_id');

CREATE TABLE IF NOT EXISTS courses (
  id bigint NOT NULL ,
  user_id bigint DEFAULT NULL ,
  name character varying(255) DEFAULT NULL,
  description text NOT NULL,
  status character varying(45) DEFAULT NULL,
  start_date character varying(45) DEFAULT NULL,
  end_date character varying(45) DEFAULT NULL,
  active bigint DEFAULT '1',
  "createdAt" timestamp NULL DEFAULT NULL,
  "updatedAt" timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)  
);

CREATE SEQUENCE registrations_courses_id START 1;
ALTER TABLE courses ALTER id SET default nextval('registrations_courses_id');

CREATE TABLE IF NOT EXISTS course_files (
  id bigint NOT NULL ,
  course_id bigint DEFAULT NULL ,
  lecture_id bigint DEFAULT NULL ,
  file_name character varying(255) DEFAULT NULL,
  file_type character varying(45) DEFAULT NULL,
  "fileName" character varying(255) DEFAULT NULL,
  "contentType" character varying(255) DEFAULT NULL,
  ext character varying(45) DEFAULT NULL,
  location character varying(255) DEFAULT NULL,
  file_id bigint DEFAULT NULL,
  "createdAt" timestamp NULL DEFAULT NULL,
  "updatedAt" timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)  
);

CREATE SEQUENCE registrations_course_files_id START 1;
ALTER TABLE course_files ALTER id SET default nextval('registrations_course_files_id');

CREATE TABLE IF NOT EXISTS exams (
  id bigint NOT NULL ,
  course_id bigint DEFAULT NULL ,
  name character varying(255) DEFAULT NULL,
  start_date character varying(45) DEFAULT NULL,
  end_date character varying(45) DEFAULT NULL,
  time_limit character varying(45) DEFAULT NULL,
  status character varying(45) DEFAULT NULL,
  active bigint DEFAULT '1',
  "createdAt" timestamp NULL DEFAULT NULL,
  "updatedAt" timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)  
);

CREATE SEQUENCE registrations_exams_id START 1;
ALTER TABLE exams ALTER id SET default nextval('registrations_exams_id');

CREATE TABLE IF NOT EXISTS questions (
  id bigint NOT NULL ,
  course_id bigint DEFAULT NULL ,
  exam_id bigint DEFAULT NULL ,
  question text DEFAULT NULL,
  question_type character varying(255) DEFAULT NULL,
  status character varying(45) DEFAULT NULL,
  marks character varying(45) DEFAULT NULL,
  active bigint DEFAULT '1',
  "createdAt" timestamp NULL DEFAULT NULL,
  "updatedAt" timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)  
);

CREATE SEQUENCE registrations_questions_id START 1;
ALTER TABLE questions ALTER id SET default nextval('registrations_questions_id');

CREATE TABLE IF NOT EXISTS question_options (
  id bigint NOT NULL ,
  question_id bigint DEFAULT NULL ,
  option text DEFAULT NULL,
  status character varying(45) DEFAULT NULL,
  active bigint DEFAULT '1',
  "createdAt" timestamp NULL DEFAULT NULL,
  "updatedAt" timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)  
);

CREATE SEQUENCE registrations_question_options_id START 1;
ALTER TABLE question_options ALTER id SET default nextval('registrations_question_options_id');

CREATE TABLE IF NOT EXISTS lectures (
  id bigint NOT NULL ,
  course_id bigint DEFAULT NULL ,
  name character varying(255) DEFAULT NULL,
  date character varying(45) DEFAULT NULL,
  active bigint DEFAULT '1',
  "createdAt" timestamp NULL DEFAULT NULL,
  "updatedAt" timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)  
);

CREATE SEQUENCE registrations_lectures_id START 1;
ALTER TABLE lectures ALTER id SET default nextval('registrations_lectures_id');

CREATE TABLE IF NOT EXISTS comments (
  id bigint NOT NULL ,
  course_id bigint DEFAULT NULL ,
  user_id bigint DEFAULT NULL ,
  comment text DEFAULT NULL,
  active bigint DEFAULT '1',
  "createdAt" timestamp NULL DEFAULT NULL,
  "updatedAt" timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)  
);

CREATE SEQUENCE registrations_comments_id START 1;
ALTER TABLE comments ALTER id SET default nextval('registrations_comments_id');

CREATE TABLE IF NOT EXISTS properties (
  id bigint NOT NULL ,
  no_of_courses bigint DEFAULT NULL ,
  no_of_exams_per_course bigint DEFAULT NULL ,
  no_of_questions_per_exam bigint DEFAULT NULL,
  "createdAt" timestamp NULL DEFAULT NULL,
  "updatedAt" timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)  
);

CREATE SEQUENCE registrations_properties_id START 1;
ALTER TABLE properties ALTER id SET default nextval('registrations_properties_id');

CREATE TABLE IF NOT EXISTS student_courses (
  id bigint NOT NULL ,
  course_id bigint DEFAULT NULL ,
  user_id bigint DEFAULT NULL ,
  registered bigint DEFAULT '1',
  "createdAt" timestamp NULL DEFAULT NULL,
  "updatedAt" timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)  
);

CREATE SEQUENCE registrations_student_courses_id START 1;
ALTER TABLE student_courses ALTER id SET default nextval('registrations_student_courses_id');

CREATE TABLE IF NOT EXISTS student_solutions (
  id bigint NOT NULL ,
  user_id bigint DEFAULT NULL ,
  file_id bigint DEFAULT NULL ,
  course_id bigint DEFAULT NULL,
  lecture_id bigint DEFAULT NULL,
  student_course_id bigint DEFAULT NULL ,
  "fileName" character varying(255) DEFAULT NULL,
  "contentType" character varying(255) DEFAULT NULL,
  ext character varying(45) DEFAULT NULL,
  location character varying(255) DEFAULT NULL,
  "createdAt" timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)  
);

CREATE SEQUENCE registrations_student_solutions_id START 1;
ALTER TABLE student_solutions ALTER id SET default nextval('registrations_student_solutions_id');

CREATE TABLE IF NOT EXISTS student_solution_comments (
  id bigint NOT NULL ,
  student_solution_id bigint DEFAULT NULL ,
  user_id bigint DEFAULT NULL ,
  comment text DEFAULT NULL,
  "createdAt" timestamp NULL DEFAULT NULL,
  "updatedAt" timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)  
);

CREATE SEQUENCE registrations_student_solution_comments_id START 1;
ALTER TABLE student_solution_comments ALTER id SET default nextval('registrations_student_solution_comments_id');

CREATE TABLE IF NOT EXISTS student_exams (
  id bigint NOT NULL ,
  exam_id bigint DEFAULT NULL ,
  user_id bigint DEFAULT NULL ,
  student_course_id bigint DEFAULT NULL ,
  start_time timestamp NULL DEFAULT NULL ,
  submitted bigint DEFAULT '0',
  status character varying(45) DEFAULT NULL,
  grade character varying(45) DEFAULT NULL,
  "createdAt" timestamp NULL DEFAULT NULL,
  "updatedAt" timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)  
);

CREATE SEQUENCE registrations_student_exams_id START 1;
ALTER TABLE student_exams ALTER id SET default nextval('registrations_student_exams_id');

CREATE TABLE IF NOT EXISTS student_answers (
  id bigint NOT NULL ,
  user_id bigint DEFAULT NULL ,
  question_id bigint DEFAULT NULL ,
  student_exam_id bigint DEFAULT NULL ,
  student_course_id bigint DEFAULT NULL ,
  solution text DEFAULT NULL,
  marks_obtained character varying(45) DEFAULT NULL,
  "createdAt" timestamp NULL DEFAULT NULL,
  "updatedAt" timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)  
);

CREATE SEQUENCE registrations_student_answers_id START 1;
ALTER TABLE student_answers ALTER id SET default nextval('registrations_student_answers_id');

INSERT INTO courses (id, user_id, name, description, status, start_date, end_date, active, "createdAt", "updatedAt") VALUES
(1, NULL, 'Datenbanken Grundlagen', 'Datenbanken Grundlagen is the name of current course.', 'enable', '05/10/2018', '05/10/2019', 1, '2018-09-03 13:02:58', NULL),
(2, NULL, 'Datenbanken und Webtechniken', 'Datenbanken und Webtechniken is the name of current course.', 'enable', '05/10/2018', '05/10/2019', 1, '2018-09-03 13:03:44', NULL);

ALTER SEQUENCE registrations_courses_id RESTART WITH 3;

INSERT INTO exams (id, name, course_id, start_date, end_date, time_limit, active, "createdAt", "updatedAt", status) VALUES
(1, 'Exam 1', 1, '03/09/2018', '31/08/2019', '15', 1, '2018-09-03 13:05:01', '2018-09-03 13:05:01', 'enable'),
(2, 'Exam 1', 2, '03/09/2018', '31/08/2019', '15', 1, '2018-09-03 14:41:22', '2018-09-03 14:41:22', 'enable');

ALTER SEQUENCE registrations_exams_id RESTART WITH 3;

INSERT INTO questions (id, question, exam_id, course_id, status, question_type, active, "createdAt", "updatedAt", marks) VALUES
(1, 'What are the different types of Normalization?', 1, 1, 'enable', 'shortdescription', 1, '2018-09-03 13:05:23', '2018-09-03 13:05:23', '2'),
(2, 'Which of the following gives a logical structure of the database graphically ?', 1, 1, 'enable', 'multichoice', 1, '2018-09-03 13:07:29', '2018-09-03 13:07:29', '2'),
(3, 'If you were collecting and storing information about your music collection, an album would be considered a(n) _____', 1, 1, 'enable', 'fillintheblank', 1, '2018-09-03 13:11:36', '2018-09-03 14:38:13', '2'),
(4, 'A database is called ''self-describing'' because it contains a description of itself.', 1, 1, 'enable', 'truefalse', 1, '2018-09-03 14:38:57', '2018-09-03 14:39:24', '2'),
(5, 'Words used in PL/SQL block are called.', 1, 1, 'enable', 'fillintheblank', 1, '2018-09-03 14:40:39', '2018-09-03 14:40:39', '2'),
(6, 'What is a Trigger?', 2, 2, 'enable', 'shortdescription', 1, '2018-09-03 14:42:23', '2018-09-03 14:42:23', '2'),
(7, 'In data model schemas, constraints that are can not be expressed directly are classified as', 2, 2, 'enable', 'multichoice', 1, '2018-09-03 14:43:28', '2018-09-03 14:43:28', '2'),
(8, 'In relational database schemas, state constraints are also known as', 2, 2, 'enable', 'multichoice', 1, '2018-09-03 14:44:41', '2018-09-03 14:44:41', '2'),
(9, 'SQL does not allow you to use the same table in a subquery as is being used in the outer query (the query that includes the subquery)', 2, 2, 'enable', 'truefalse', 1, '2018-09-03 14:45:20', '2018-09-03 14:45:20', '2'),
(10, 'The process that stores copies of a table and stores it in several different locations at the same time is called  ______ ?', 2, 2, 'enable', 'fillintheblank', 1, '2018-09-03 14:46:18', '2018-09-03 14:46:18', '2'),
(11, 'The type of form that displays data from two tables that have a one to many relationship is called a __________', 2, 2, 'enable', 'fillintheblank', 1, '2018-09-03 14:47:14', '2018-09-03 14:47:14', '2'),
(12, 'Words used in PL/SQL block are called.', 2, 2, 'enable', 'multichoice', 1, '2018-09-03 14:48:17', '2018-09-03 14:48:17', '2'),
(13, 'A simple class diagram for a university database has only three classes  :Teachers, Students and Courses.How many binary associations will be sufficient to establish whether Teacher X is teaching Course Y to student Z?', 2, 2, 'enable', 'multichoice', 1, '2018-09-03 14:50:12', '2018-09-03 14:50:12', '2'),
(14, 'The Unified Modeling Language(UML)is :', 2, 2, 'enable', 'multichoice', 1, '2018-09-03 14:51:27', '2018-09-03 14:51:27', '2');

ALTER SEQUENCE registrations_questions_id RESTART WITH 15;

INSERT INTO question_options (id, question_id, option, status, active, "createdAt", "updatedAt") VALUES
(6, 2, 'Entity-relationship diagram', NULL, 1, '2018-09-03 13:07:29', '2018-09-03 13:07:29'),
(7, 2, 'Entity diagram', NULL, 1, '2018-09-03 13:07:29', '2018-09-03 13:07:29'),
(8, 2, 'Database diagram', NULL, 1, '2018-09-03 13:07:29', '2018-09-03 13:07:29'),
(9, 2, 'Architectural representation', NULL, 1, '2018-09-03 13:07:29', '2018-09-03 13:07:29'),
(10, 3, 'Relation', NULL, 1, '2018-09-03 14:38:13', '2018-09-03 14:38:13'),
(11, 3, 'Entity', NULL, 1, '2018-09-03 14:38:13', '2018-09-03 14:38:13'),
(12, 3, 'Instance', NULL, 1, '2018-09-03 14:38:13', '2018-09-03 14:38:13'),
(13, 3, 'Attribute', NULL, 1, '2018-09-03 14:38:13', '2018-09-03 14:38:13'),
(14, 5, 'Grammar', NULL, 1, '2018-09-03 14:40:39', '2018-09-03 14:40:39'),
(15, 5, 'Lexical Units', NULL, 1, '2018-09-03 14:40:39', '2018-09-03 14:40:39'),
(16, 5, 'Block Units', NULL, 1, '2018-09-03 14:40:39', '2018-09-03 14:40:39'),
(17, 5, 'Lexical', NULL, 1, '2018-09-03 14:40:39', '2018-09-03 14:40:39'),
(18, 7, 'application based constraints', NULL, 1, '2018-09-03 14:43:28', '2018-09-03 14:43:28'),
(19, 7, 'semantic constraints', NULL, 1, '2018-09-03 14:43:28', '2018-09-03 14:43:28'),
(20, 7, 'business rules', NULL, 1, '2018-09-03 14:43:28', '2018-09-03 14:43:28'),
(21, 7, 'all of above', NULL, 1, '2018-09-03 14:43:28', '2018-09-03 14:43:28'),
(22, 8, 'static constraints', NULL, 1, '2018-09-03 14:44:41', '2018-09-03 14:44:41'),
(23, 8, 'dynamic constraints', NULL, 1, '2018-09-03 14:44:41', '2018-09-03 14:44:41'),
(24, 8, 'implicit constraints', NULL, 1, '2018-09-03 14:44:41', '2018-09-03 14:44:41'),
(25, 8, 'explicit constraints', NULL, 1, '2018-09-03 14:44:41', '2018-09-03 14:44:41'),
(26, 10, 'Partitioning', NULL, 1, '2018-09-03 14:46:18', '2018-09-03 14:46:18'),
(27, 10, 'Segmentation', NULL, 1, '2018-09-03 14:46:18', '2018-09-03 14:46:18'),
(28, 10, 'Data mirroring', NULL, 1, '2018-09-03 14:46:18', '2018-09-03 14:46:18'),
(29, 10, 'Replication', NULL, 1, '2018-09-03 14:46:18', '2018-09-03 14:46:18'),
(30, 11, 'Relational form', NULL, 1, '2018-09-03 14:47:14', '2018-09-03 14:47:14'),
(31, 11, 'Crosstab form', NULL, 1, '2018-09-03 14:47:14', '2018-09-03 14:47:14'),
(32, 11, 'Subform', NULL, 1, '2018-09-03 14:47:14', '2018-09-03 14:47:14'),
(33, 11, 'Referential form', NULL, 1, '2018-09-03 14:47:14', '2018-09-03 14:47:14'),
(34, 12, 'Grammar', NULL, 1, '2018-09-03 14:48:17', '2018-09-03 14:48:17'),
(35, 12, 'Lexical Units', NULL, 1, '2018-09-03 14:48:17', '2018-09-03 14:48:17'),
(36, 12, 'Block Units', NULL, 1, '2018-09-03 14:48:17', '2018-09-03 14:48:17'),
(37, 12, 'Lexical', NULL, 1, '2018-09-03 14:48:17', '2018-09-03 14:48:17'),
(38, 13, '3', NULL, 1, '2018-09-03 14:50:12', '2018-09-03 14:50:12'),
(39, 13, '1', NULL, 1, '2018-09-03 14:50:12', '2018-09-03 14:50:12'),
(40, 13, '2', NULL, 1, '2018-09-03 14:50:12', '2018-09-03 14:50:12'),
(41, 13, 'None of the above', NULL, 1, '2018-09-03 14:50:12', '2018-09-03 14:50:12'),
(42, 14, 'An international standard language used to write database application programs.', NULL, 1, '2018-09-03 14:51:27', '2018-09-03 14:51:27'),
(43, 14, 'An important markup language used for developing web sites.', NULL, 1, '2018-09-03 14:51:27', '2018-09-03 14:51:27'),
(44, 14, 'A general purpose set of standards to visually model various aspects of software systems and business processes.', NULL, 1, '2018-09-03 14:51:27', '2018-09-03 14:51:27'),
(45, 14, 'None of the above', NULL, 1, '2018-09-03 14:51:27', '2018-09-03 14:51:27');

ALTER SEQUENCE registrations_question_options_id RESTART WITH 46;

INSERT INTO users (first_name, last_name, email, password, contact_number, role, active, "createdAt", "updatedAt") VALUES
('Teacher', 'User', 'teacher@test.com', '25f9e794323b453885f5181f1b624d0b', '9999999999', 'TEACHER', 1, '2018-08-22 15:10:59', '2018-08-22 15:10:59'),
('Student', 'User', 'student@test.com', '25f9e794323b453885f5181f1b624d0b', '9999999999', 'STUDENT', 1, '2018-08-22 15:10:59', '2018-08-22 15:10:59'),
('Student1', 'User', 'student1@test.com', '25f9e794323b453885f5181f1b624d0b', '9999999999', 'STUDENT', 1, '2018-08-22 15:10:59', '2018-08-22 15:10:59');

ALTER SEQUENCE registrations_users_id RESTART WITH 5;

INSERT INTO properties (id, no_of_courses, no_of_exams_per_course, no_of_questions_per_exam, "createdAt", "updatedAt") VALUES
(1, '3', '3', '3', '2018-09-03 14:51:27', '2018-09-03 14:51:27');

ALTER SEQUENCE registrations_properties_id RESTART WITH 2;