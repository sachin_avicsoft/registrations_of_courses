<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class CI_AlertMessage {
        
    public $SUCCESS = 'success';
    public $ERROR = 'error';     
        
    public $message;    
    public $messageType;
   
     public function __construct(){
        //parent::__construct();
    }

    function initialize($message, $messageType){        
        $this->message = $message;
        $this->messageType = $messageType;
    }
    
    function printResultMessage($message, $messageType){
        
        $this->initialize($message, $messageType);
        
        if(null != $this->message){    
            if($this->messageType == $this->SUCCESS){
                return '<div class="alert alert-success alert-dismissible" role="alert">
                    <span type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></span>
                    <i class="fa fa-check-circle"></i>&nbsp;'.$this->message.'</div>';
            } else {
                return '<div class="alert alert-danger alert-dismissible" role="alert">
  <span type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></span>
  &nbsp;'.$this->message.'
</div>';
            }
        }        
    }

}

?>